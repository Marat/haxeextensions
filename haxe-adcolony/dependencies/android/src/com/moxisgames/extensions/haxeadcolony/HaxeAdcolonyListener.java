package com.moxisgames.extensions.haxeadcolony;

import android.content.Intent;
import android.util.Log;
import com.jirbo.adcolony.*;

import org.haxe.lime.HaxeObject;

public class HaxeAdcolonyListener implements AdColonyAdListener, AdColonyV4VCListener, AdColonyAdAvailabilityListener
{

    private HaxeObject _callback;
    public boolean isVideoActive = false;

    public HaxeAdcolonyListener(HaxeObject callback)
    {
        _callback = callback;
    }

    // Reward Callback
    public void onAdColonyV4VCReward(AdColonyV4VCReward reward)
    {
        _callback.call("onStatus", new Object[]{"reward", reward.success() ? 1 : 0 });
    }

    // Ad Started Callback - called only when an ad successfully starts playing
    public void onAdColonyAdStarted(AdColonyAd ad)
    {
        isVideoActive = true;
        _callback.call("onStatus", new Object[]{"started", ""});
    }

    //Ad Attempt Finished Callback - called at the end of any ad attempt - successful or not.
    public void onAdColonyAdAttemptFinished(AdColonyAd ad)
    {
        // You can ping the AdColonyAd object here for more information:
        // ad.shown() - returns true if the ad was successfully shown.
        // ad.notShown() - returns true if the ad was not shown at all (i.e. if onAdColonyAdStarted was never triggered)
        // ad.skipped() - returns true if the ad was skipped due to an interval play setting
        // ad.canceled() - returns true if the ad was cancelled (either programmatically or by the user)
        // ad.noFill() - returns true if the ad was not shown due to no ad fill.
		isVideoActive = false;
        _callback.call("onStatus", new Object[]{"finished", ad.shown() ? 1 : 0});

        //setResultsText();
    }

    // Ad Availability Change Callback - update button text
    public void onAdColonyAdAvailabilityChange(boolean available, String zone_id)
    {
        _callback.call("onStatus", new Object[]{ "available", available ? 1 : 0 });

        //if (available) button_text_handler.post(button_text_runnable);
    }
}