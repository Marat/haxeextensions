package com.moxisgames.extensions.haxeadcolony;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.util.Log;

import org.haxe.extension.Extension;
import org.haxe.lime.HaxeObject;

import com.jirbo.adcolony.*;

import java.lang.String;

/* 
	You can use the Android Extension class in order to hook
	into the Android activity lifecycle. This is not required
	for standard Java code, this is designed for when you need
	deeper integration.
	
	You can access additional references from the Extension class,
	depending on your needs:
	
	- Extension.assetManager (android.content.res.AssetManager)
	- Extension.callbackHandler (android.os.Handler)
	- Extension.mainActivity (android.app.Activity)
	- Extension.mainContext (android.content.Context)
	- Extension.mainView (android.view.View)
	
	You can also make references to static or instance methods
	and properties on Java classes. These classes can be included 
	as single files using <java path="to/File.java" /> within your
	project, or use the full Android Library Project format (such
	as this example) in order to include your own AndroidManifest
	data, additional dependencies, etc.
	
	These are also optional, though this example shows a static
	function for performing a single task, like returning a value
	back to Haxe from Java.
*/
public class HaxeAdcolony extends Extension {

	private static String _appID;
	private static String _zoneID;
	private static HaxeObject _callback;

	private static HaxeAdcolonyListener _adColonyListener;

	private static AdColonyV4VCAd v4vc_ad;

	public static void init(String appID, String zoneID, HaxeObject callback) {
		_appID = appID;
		_zoneID = zoneID;
		_callback = callback;

		_adColonyListener = new HaxeAdcolonyListener(_callback);

		AdColony.configure(Extension.mainActivity, "version:1.0,store:google", _appID, _zoneID);
		AdColony.addAdAvailabilityListener(_adColonyListener);
		AdColony.addV4VCListener(_adColonyListener);

		//if ( !AdColony.isTablet() )
		//	Extension.mainActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public static void showVideo() {
		v4vc_ad = new AdColonyV4VCAd( _zoneID ).withListener(_adColonyListener);
		v4vc_ad.show();
	}

	/**
	 * Called as part of the activity lifecycle when an activity is going into
	 * the background, but has not (yet) been killed.
	 */
	public void onPause () {
		if (_adColonyListener != null && _adColonyListener.isVideoActive)
			AdColony.pause();
	}

	/**
	 * Called after {@link #onRestart}, or {@link #onPause}, for your activity
	 * to start interacting with the user.
	 */
	public void onResume () {
		if (_adColonyListener != null && _adColonyListener.isVideoActive)
			AdColony.resume(Extension.mainActivity);
	}

	/**
	 * Called when the activity is starting.
	 */
	public void onCreate (Bundle savedInstanceState) {
	}
	
	
	/**
	 * Perform any final cleanup before an activity is destroyed.
	 */
	public void onDestroy () {
	}

	/**
	 * Called after {@link #onStop} when the current activity is being 
	 * re-displayed to the user (the user has navigated back to it).
	 */
	public void onRestart () {
	}

	
	/**
	 * Called after {@link #onCreate} &mdash; or after {@link #onRestart} when  
	 * the activity had been stopped, but is now again being displayed to the 
	 * user.
	 */
	public void onStart () {
	}
	
	
	/**
	 * Called when the activity is no longer visible to the user, because 
	 * another activity has been resumed and is covering this one. 
	 */
	public void onStop () {
	}
	
	
}