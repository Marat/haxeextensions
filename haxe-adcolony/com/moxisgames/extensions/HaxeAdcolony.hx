package com.moxisgames.extensions;

import openfl.Lib;

class HaxeAdcolony {
	
	static private var _initFunc:String->String->Dynamic->Void;
	static private var _showFunc:Void->Void;
	
	static private inline var ANDROID_CLASS_PATH:String = "com.moxisgames.extensions.haxeadcolony.HaxeAdcolony";
	
	static public function init(appID:String, zoneID:String, listener:Dynamic) 
	{
		#if android
		
		if (_initFunc == null)
			_initFunc = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "init", "(Ljava/lang/String;Ljava/lang/String;Lorg/haxe/lime/HaxeObject;)V");
		
		Lib.postUICallback(
			function() {
				_initFunc(appID, zoneID, listener);
			}
		);
		
		#elseif ios
		
		if (_initFunc == null)
			_initFunc = cpp.Lib.load("haxe_adcolony", "init", 3);
		
		_initFunc(appID, zoneID, listener);
		#end
	}
	
	static public function showVideo()
	{
		#if android
		
		if (_showFunc == null)
			_showFunc = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "showVideo", "()V");
		
		Lib.postUICallback(
			function() {
				_showFunc();
			}
		);
		
		#elseif ios
		
		if (_showFunc == null)
			_showFunc = cpp.Lib.load("haxe_adcolony", "show", 0);
		
		_showFunc();
		#end
	}
	
}