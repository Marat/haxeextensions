package com.moxisgames.extensions;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.widget.Toast;

import org.haxe.extension.Extension;
import org.haxe.lime.HaxeObject;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.RewardedVideoCallbacks;

import java.lang.Override;
import java.lang.String;

/* 
	You can use the Android Extension class in order to hook
	into the Android activity lifecycle. This is not required
	for standard Java code, this is designed for when you need
	deeper integration.
	
	You can access additional references from the Extension class,
	depending on your needs:
	
	- Extension.assetManager (android.content.res.AssetManager)
	- Extension.callbackHandler (android.os.Handler)
	- Extension.mainActivity (android.app.Activity)
	- Extension.mainContext (android.content.Context)
	- Extension.mainView (android.view.View)
	
	You can also make references to static or instance methods
	and properties on Java classes. These classes can be included 
	as single files using <java path="to/File.java" /> within your
	project, or use the full Android Library Project format (such
	as this example) in order to include your own AndroidManifest
	data, additional dependencies, etc.
	
	These are also optional, though this example shows a static
	function for performing a single task, like returning a value
	back to Haxe from Java.
*/
public class HaxeAppodeal extends Extension
{

	private static HaxeObject _videoStatusCallback;
	private static HaxeObject _videoShowCallback;

	private static boolean _bannerShow = false;


	static private RewardedVideoCallbacks videoListener = new RewardedVideoCallbacks()
	{
		//private Toast mToast;

		@Override
		public void onRewardedVideoLoaded()
		{
			//showToast("onRewardedVideoLoaded");
			//Log.i("HaxeAppodeal", "HaxeAppodeal onVideoLoaded");

			if (_videoStatusCallback != null)
				_videoStatusCallback.call("onStatus", new Object[]{1});
		}

		@Override
		public void onRewardedVideoFailedToLoad()
		{
			//showToast("onRewardedVideoLoaded");
			//Log.i("HaxeAppodeal", "HaxeAppodeal onVideoFailedToLoad");

			if (_videoStatusCallback != null)
				_videoStatusCallback.call("onStatus", new Object[]{0});
		}

		@Override
		public void onRewardedVideoShown()
		{
			//showToast("onRewardedVideoShown");
		}

		@Override
		public void onRewardedVideoFinished(int amount, String name)
		{
			//showToast("onRewardedVideoFinished. Reward: " + amount + " " + name);

			if (_videoShowCallback != null)
			{
				_videoShowCallback.call("onResult", new Object[]{1});
				_videoShowCallback = null;
			}
		}

		@Override
		public void onRewardedVideoClosed()
		{
			//showToast("onRewardedVideoClosed");

			if (_videoShowCallback != null)
			{
				_videoShowCallback.call("onResult", new Object[]{0});
				_videoShowCallback = null;
			}
		}

		void showToast(final String text)
		{
			//if (mToast == null)
			//{
			Toast mToast = Toast.makeText(Extension.mainActivity, text, Toast.LENGTH_SHORT);
			//}
			mToast.setText(text);
			mToast.setDuration(Toast.LENGTH_SHORT);
			mToast.show();
		}
	};

	//static private AppodealTask task;

	static class AppodealTask extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... params)
		{
			Appodeal.setTesting(_test);
			Appodeal.disableLocationPermissionCheck();

			Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, false);
			Appodeal.setAutoCache(Appodeal.BANNER, _canShowBanners);

			Appodeal.setRewardedVideoCallbacks(videoListener);

			int typesMask = Appodeal.REWARDED_VIDEO;

			if (_canShowBanners)
				typesMask = typesMask | Appodeal.BANNER;

			Appodeal.initialize(Extension.mainActivity, _appKey, typesMask);

			return null;
		}
	}

	static class ShowBannerTask extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... params)
		{
			Appodeal.show(Extension.mainActivity, Appodeal.BANNER_BOTTOM);
			return null;
		}
	}

	static private String _appKey;
	static private boolean _test = true;
	static private boolean _canShowBanners = true;

	public static void init(String appKey, boolean initBanners, boolean test)
	{
		_appKey = appKey;
		_test = test;
		_canShowBanners = initBanners;

		AppodealTask task = new AppodealTask();
		task.execute();
	}

	public static void showBanner()
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal showBanner");

		_bannerShow = true;

		if (_canShowBanners)
		{
			ShowBannerTask task = new ShowBannerTask();
			task.execute();
		}
	}

	public static void hideBanner()
	{
		if (_canShowBanners)
		{
			//Log.i("HaxeAppodeal", "HaxeAppodeal hideBanner");
			Appodeal.hide(Extension.mainActivity, Appodeal.BANNER);

			_bannerShow = false;
		}
	}

	public static void setVideoStatusCallback(HaxeObject callback)
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal setVideoStatusCallback " + callback);
		_videoStatusCallback = callback;
	}

	public static void showVideo(HaxeObject callback)
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal showVideo");

		_videoShowCallback = callback;

		ShowVideoTask task = new ShowVideoTask();
		task.execute();

		//Appodeal.show(Extension.mainActivity, Appodeal.REWARDED_VIDEO);
	}

	public static boolean hasVideo()
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal video isLoaded " + Appodeal.isLoaded(Appodeal.VIDEO));
		return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
	}

	public static void cacheVideo()
	{
		if (!hasVideo())
		{
			CacheVideoTask task = new CacheVideoTask();
			task.execute();

			//Appodeal.cache(Extension.mainActivity, Appodeal.REWARDED_VIDEO);
		}
	}

	static class CacheVideoTask extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... params)
		{
			//Log.i("HaxeAppodeal", "HaxeAppodeal CacheVideoTask execute");
			Appodeal.cache(Extension.mainActivity, Appodeal.REWARDED_VIDEO);
			return null;
		}
	}

	static class ShowVideoTask extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... params)
		{
			//Log.i("HaxeAppodeal", "HaxeAppodeal ShowVideoTask execute");
			Appodeal.show(Extension.mainActivity, Appodeal.REWARDED_VIDEO);
			return null;
		}
	}

	/**
	 * Called as part of the activity lifecycle when an activity is going into
	 * the background, but has not (yet) been killed.
	 */
	public void onPause()
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal onPause");
	}

	/**
	 * Called after {@link #onRestart}, or {@link #onPause}, for your activity
	 * to start interacting with the user.
	 */
	public void onResume()
	{
		super.onResume();

		//Log.i("HaxeAppodeal", "HaxeAppodeal onResume _bannerShow " + _bannerShow);

		if (_canShowBanners)
		{
			if (_bannerShow)
				Appodeal.onResume(Extension.mainActivity, Appodeal.BANNER);
			else
				Appodeal.hide(Extension.mainActivity, Appodeal.BANNER);
		}
	}

	/**
	 * Called when the activity is starting.
	 */
	public void onCreate(Bundle savedInstanceState)
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal onCreate");
	}

	/**
	 * Perform any final cleanup before an activity is destroyed.
	 */
	public void onDestroy()
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal onDestroy");
	}

	/**
	 * Called after {@link #onStop} when the current activity is being
	 * re-displayed to the user (the user has navigated back to it).
	 */
	public void onRestart()
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal onRestart");
	}

	/**
	 * Called after {@link #onCreate} &mdash; or after {@link #onRestart} when
	 * the activity had been stopped, but is now again being displayed to the
	 * user.
	 */
	public void onStart()
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal onStart");
	}

	/**
	 * Called when the activity is no longer visible to the user, because
	 * another activity has been resumed and is covering this one.
	 */
	public void onStop()
	{
		//Log.i("HaxeAppodeal", "HaxeAppodeal onStop");
	}

}