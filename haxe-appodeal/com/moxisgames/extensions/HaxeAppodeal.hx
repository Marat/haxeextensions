package com.moxisgames.extensions;

import openfl.Lib;

class HaxeAppodeal {
	
	static private var _inited:Bool = false;
	
	static private var _initFunc:String->Bool->Bool->Void;
	
	static private var _hasVideo:Void->Bool;
	static private var _cacheVideo:Void->Void;
	static private var _showVideo:Dynamic->Void;
	
	static private var _showBanner:Void->Void;
	static private var _hideBanner:Void->Void;
	
	static private inline var ANDROID_CLASS_PATH:String = "com.moxisgames.extensions.HaxeAppodeal";
	
	static public function init(appKey:String, initBanners:Bool, test:Bool = false) 
	{
		#if android
		if (_initFunc == null)
			_initFunc = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "init", "(Ljava/lang/String;ZZ)V");
		
		#if (legacy || hybrid) openfl.Lib #else openfl.utils.JNI #end
		.postUICallback(
			function() {
				_initFunc(appKey, initBanners, test);
			}
		);
		
		#elseif ios
		
		if (_initFunc == null)
			_initFunc = cpp.Lib.load("haxe_appodeal", "init", 3);
		
		_initFunc(appKey, initBanners, test);
		#end
		
		_inited = true;
	}
	
	static public function isInited():Bool
	{
		return _inited;
	}
	
	static public function showBanner()
	{
		#if android
		
		if (_showBanner == null)
			_showBanner = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "showBanner", "()V");
		
		#if (legacy || hybrid) openfl.Lib #else openfl.utils.JNI #end
		.postUICallback(
			function() {
				_showBanner();
			}
		);
		
		#elseif ios
		
		if (_showBanner == null)
			_showBanner = cpp.Lib.load("haxe_appodeal", "showBanner", 0);
		
		_showBanner();
		#end
	}
	
	static public function hideBanner()
	{
		#if android
		
		if (_hideBanner == null)
			_hideBanner = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "hideBanner", "()V");
		
		_hideBanner();
		
		#elseif ios
		
		if (_hideBanner == null)
			_hideBanner = cpp.Lib.load("haxe_appodeal", "hideBanner", 0);
		
		_hideBanner();
		#end
	}
	
	static public function setVideoStatusCallback(listener:Dynamic)
	{
		#if android
		var setVideoCallback:Dynamic->Void = 
			openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "setVideoStatusCallback", "(Lorg/haxe/lime/HaxeObject;)V");
		setVideoCallback(listener);
		#elseif ios
		//setVideoCallback = cpp.Lib.load("haxe_appodeal", "showVideo", 1);
		//setVideoCallback(listener);
		#end
	}
	
	static public function hasVideo():Bool
	{
		#if android
		if (_hasVideo == null)
			_hasVideo = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasVideo", "()Z");
			
		return _hasVideo();
		#end
		
		return false;
	}
	
	static public function cacheVideo():Void
	{
		#if android
		if (_cacheVideo == null)
			_cacheVideo = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "cacheVideo", "()V");
			
		_cacheVideo();
		#end
	}
	
	static public function showVideo(listener:Dynamic)
	{
		#if android
		if (_showVideo == null)
			_showVideo = openfl.utils.JNI.createStaticMethod(ANDROID_CLASS_PATH, "showVideo", "(Lorg/haxe/lime/HaxeObject;)V");
		
		#if (legacy || hybrid) openfl.Lib #else openfl.utils.JNI #end
		.postUICallback(
			function() {
				_showVideo(listener);
			}
		);
		
		#elseif ios
		
		if (_showVideo == null)
			_showVideo = cpp.Lib.load("haxe_appodeal", "showVideo", 1);
		
		_showVideo(listener);
		#end
	}
	
}