package com.moxisgames.framework.listeners;

import haxe.Constraints.Function;
import flash.events.IEventDispatcher;

class ActiveListener
{
    public var dispatcher : IEventDispatcher;
    public var event : String;
    public var listener : Function;
    public var useCapture : Bool;
    
    public function new(dispatcher : IEventDispatcher, event : String, listener : Function, useCapture : Bool = false)
    {
        this.dispatcher = dispatcher;
        this.event = event;
        this.listener = listener;
        this.useCapture = useCapture;
        
        dispatcher.addEventListener(event, listener, useCapture);
    }
    
    public function compare(disp : IEventDispatcher, evt : String, list : Function, useCapt : Bool) : Bool
    {
        if (disp != dispatcher)
        {
            return false;
        }
        
        if (evt != event)
        {
            return false;
        }
        
        if (list != listener)
        {
            return false;
        }
        
        if (useCapt != useCapture)
        {
            return false;
        }
        
        return true;
    }
    
    public function remove() : Void
    {
        dispatcher.removeEventListener(event, listener, useCapture);
        
        dispatcher = null;
        listener = null;
    }
}
