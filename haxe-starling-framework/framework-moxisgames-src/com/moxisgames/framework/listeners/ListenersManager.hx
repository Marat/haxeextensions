package com.moxisgames.framework.listeners;

import haxe.Constraints.Function;
import flash.events.IEventDispatcher;

class ListenersManager
{
    private var _hash : Array<ActiveListener>;
    
    public function new()
    {
        _hash = [];
    }
    
    public function addListener(dispatcher : IEventDispatcher, eventType : String, listener : Function, useCapture : Bool = false) : Void
    {
        if (dispatcher == null)
        {
            return;
        }
        
        if (getListener(dispatcher, eventType, listener, useCapture) == null)
        {
            _hash.push(new ActiveListener(dispatcher, eventType, listener, useCapture));
        }
    }
    
    private function getListener(dispatcher : IEventDispatcher, eventType : String, listener : Function, useCapture : Bool) : ActiveListener
    {
        for (list in _hash)
        {
            if (list.compare(dispatcher, eventType, listener, useCapture))
            {
                return list;
            }
        }
        
        return null;
    }
    
    public function removeListener(dispatcher : IEventDispatcher, eventType : String, listener : Function, useCapture : Bool = false) : Void
    {
        if (dispatcher == null)
        {
            return;
        }
        
        var list : ActiveListener = getListener(dispatcher, eventType, listener, useCapture);
        
        if (list != null)
        {
            list.remove();
            _hash.splice(Lambda.indexOf(_hash, list), 1);
        }
    }
    
    public function clear() : Void
    {
        var i : Int = Std.int(_hash.length - 1);
        
        while (i >= 0)
        {
            var list : ActiveListener = _hash[i];
            list.remove();
            _hash.splice(i, 1);
            
            i--;
        }
    }
    
    public function destroy() : Void
    {
        clear();
        
        _hash = null;
    }
}
