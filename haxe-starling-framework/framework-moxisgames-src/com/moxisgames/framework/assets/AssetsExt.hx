package com.moxisgames.framework.assets;

import openfl.errors.Error;
import com.moxisgames.framework.core.CoreApp;
import com.moxisgames.framework.logs.Logger;
import flash.display.Stage;
import starling.events.Event;
import starling.utils.MathUtil;
import starling.utils.StringUtil;

/**
 * ...
 * @author mismagilov
 */
class AssetsExt {
    @:isVar public static var instance(get, never):AssetsExt;

    private var _managers:Map<String, AssetManagerExt>;

    private static var _allowInstantiate:Bool = false;
    private static var _instance:AssetsExt;

    public static var assetsPackScale:Float; // assets scale
    public static var texturesScale:Float; // from pack scale

    private static var _scaledImagesDir:String = "img/x{0}/";

    public function new() {
        if (!_allowInstantiate) {
            throw new Error("Use singltone");
        }

        _managers = new Map<String, AssetManagerExt>();
    }

    public function init(assetsScales:Array<Float>, scaledImagesDir:String = "img/x{0}/"):Void {
        var assetsScale:Float = MathUtil.min([CoreApp.stageWidth / CoreApp.defaultGameWidth, CoreApp.stageHeight / CoreApp.defaultGameHeight]);

        function getNearScale(value:Float, supportScales:Array<Float>):Float {
            supportScales.sort(
                function(a:Float, b:Float):Int {
                    if (a > b) {
                        return 1;
                    }
                    else {
                        if (a < b) {
                            return -1;
                        }
                    }

                    return 0;
                }
            );

            var currentIndex:Int = 0;

            for (i in 0...supportScales.length) {
                if (Math.abs(supportScales[i] - value) < Math.abs(supportScales[currentIndex] - value)) {
                    currentIndex = i;
                }
            }

            return supportScales[currentIndex];
        }

        assetsPackScale = getNearScale(assetsScale, assetsScales); //assetsPackScale = 1.5;
        texturesScale = assetsPackScale / CoreApp.gameAreaSizeFactor;

        _scaledImagesDir = StringUtil.formatString(scaledImagesDir, [assetsPackScale]);

        Logger.log("[AssetsExt] use images pack x" + assetsPackScale);
        Logger.log("[AssetsExt] scale factor x" + texturesScale);
    }

    public static function getManager(id:String):AssetManagerExt {
        if (!_instance._managers.exists(id)) {
            var manager:AssetManagerExt = new AssetManagerExt(texturesScale, _scaledImagesDir);
            manager.addEventListener(Event.IO_ERROR, onAssetLoadingError);
            _instance._managers.set(id, manager);
        }

        return _instance._managers.get(id);
    }

    public static function clearManager(id:String):Void {
        if (_instance._managers.exists(id)) {
            _instance._managers.get(id).purge();
        }
    }

    public static function destroyManager(id:String):Void {
        clearManager(id);

        if (_instance._managers.exists(id)) {
            _instance._managers.get(id).removeEventListener(Event.IO_ERROR, onAssetLoadingError);
            _instance._managers.remove(id);
        }
    }

    private static function get_instance():AssetsExt {
        if (_instance == null) {
            _allowInstantiate = true;
            _instance = new AssetsExt();
            _allowInstantiate = false;
        }

        return _instance;
    }

    private static function onAssetLoadingError(event:Event):Void {
        Logger.warning("Asset not found: " + Std.string(event));
    }
}
