package com.moxisgames.framework.assets;

import String;
import flash.media.Sound;
import lime.app.Future;
import openfl.utils.AssetType;
import openfl.utils.Assets;
import haxe.Constraints.Function;
import com.moxisgames.framework.logs.Logger;
import com.moxisgames.framework.utils.Platform;
import flash.system.Capabilities;
import starling.events.Event;
import starling.textures.TextureOptions;
import starling.utils.AssetManager;

class AssetManagerExt extends AssetManager {
    private var _scaledImagesDir:String;

    public function new(scaleFactor:Float, scaledImagesDir:String) {
        super(scaleFactor, false);

        _scaledImagesDir = scaledImagesDir;

        numConnections = 3;

        // INFO output level AssetsManagerExt
        verbose = true;
    }

    public function loadAssets(progressFunction:Float->Void, paths:Array<Dynamic>):Void {
        var i:Int = 0;
        var l:Int = paths.length;
        while (i < l) {
            enqueue(paths[i]);
            i++;
        }

        loadQueue(progressFunction);
    }

    public function hasTextureAtlas(atlasName:String):Bool {
        return getTextureAtlas(atlasName) != null;
    }

    public function addTextureAtlases(atlasNames:Array<String>):Void {
        enqueue(addScaleDir(splitAtlasNames(atlasNames)));
    }

    public function loadTextureAtlases(progressFunction:Float->Void, atlasNames:Array<String>):Void {
        loadAssets(progressFunction, addScaleDir(splitAtlasNames(atlasNames)));
    }

    public function addScaledDir(paths:Array<String>, appDir:Bool = true)
    {
        enqueue(addScaleDir(paths));
    }

    public function unloadTextureAtlases(atlasNames:Array<String>):Void {
        for (atlasName in atlasNames) {
            removeTextureAtlas(atlasName, true);
        }
    }

    private function addScaleDir(paths:Array<String>):Array<String> {
        var i:Int = 0;
        var l:Int = paths.length;
        while (i < l) {
            paths[i] = _scaledImagesDir + paths[i];
            i++;
        }

        return paths;
    }

    private static function splitAtlasNames(atlasNames:Array<String>):Array<String> {
        var newPaths:Array<String> = [];

        for (atlasName in atlasNames) {
            if (atlasName.lastIndexOf("/") == atlasName.length - 1) {
                newPaths.push(atlasName);
            }
            else {
                newPaths.push(atlasName + ".png");
                newPaths.push(atlasName + ".xml");
            }
        }

        return newPaths;
    }

    // SOUNDS

    public function loadSoundsDir(progressFunction:Float->Void, dirPath:String):Void {
        // var appDir : File = File.applicationDirectory;
        // enqueue(appDir.resolvePath(dirPath));

        enqueue([dirPath]);


        loadQueue(progressFunction);
    }

//    override public function enqueue(rawAssets:Array<Dynamic>):Void {
//        for (i in 0...rawAssets.length) {
//            var arg:Dynamic = rawAssets[i];
//
//            if (Std.is(arg, Array)) {
//                for (j in 0...arg.length) {
//                    var argValue:Dynamic = Reflect.field(arg, Std.string(j));
//
//                    if (Std.is(argValue, String)) {
//                        Reflect.setField(arg, Std.string(j), argValue);
//                    }
//                }
//            }
//            else {
//                if (Std.is(arg, String)) {
//                    rawAssets[i] = Std.string(arg);
//                }
//            }
//        }
//
//        super.enqueue(rawAssets);
//    }
}
