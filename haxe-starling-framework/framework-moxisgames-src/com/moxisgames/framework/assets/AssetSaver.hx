package com.moxisgames.framework.assets;

import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.utils.ByteArray;

/**
	 * Created by mismagilov on 07.12.2016.
	 */
class AssetSaver
{
    public function new()
    {
    }
    
    public static function saveFileSync(path : String, bytes : ByteArray) : File
    {
        if (path.indexOf("/") == 0)
        {
            path = path.substring(1);
        }
        
        var stream : FileStream = new FileStream();
        var toFile : File = File.applicationStorageDirectory.resolvePath(path);
        toFile.preventBackup = true;
        
        //			Logger.log("AssetSaver nativePath = " + toFile.nativePath);
        //			Logger.log("AssetSaver url = " + toFile.url);
        //			Logger.log("AssetSaver exists = " + toFile.exists);
        
        stream.open(toFile, FileMode.WRITE);
        stream.writeBytes(bytes, 0, bytes.length);
        stream.close();
        
        return toFile;
    }
    
    public static function deleteFile(path : String) : Void
    {
        var file : File = File.applicationStorageDirectory.resolvePath(path);
        
        if (file.exists)
        {
            file.deleteFile();
        }
    }
}

