package com.moxisgames.framework.sound;

import openfl.utils.Assets;
import starling.utils.Max;
import motion.Actuate;
import com.moxisgames.framework.logs.Logger;
import flash.errors.IOError;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;

/**
 * Created by mismagilov on 07.11.2016.
 */
class MusicSound {
    public var path(default, null):String;

    private var _channel:SoundChannel;
    private var _sound:Sound;

    private var _path:String;
    private var _playing:Bool = false;
    private var _loading:Bool = false;
    private var _position:Float = 0;

    public static var soundTransform:SoundTransform = new SoundTransform();

    @:allow(com.moxisgames.framework.sound)
    private function new(path:String) {
        this.path = path;
    }

    public function play():Void {
        if (_loading) return;

        if (_sound == null) {
            // check in app directory
            var delay:Float = 0.5;

            _loading = true;

            Assets.loadSound(path)
                .onComplete(function(sound) {
                    _sound = sound;
                    _loading = false;
                    _playing = true;
                    _channel = _sound.play(0, Max.INT_MAX_VALUE, new SoundTransform(0));

                    Actuate.transform(_channel, delay).sound(soundTransform.volume);
                })
                .onError(function(error) {
                    Logger.error("Music sound error " + error);
                });
        }
        else {
            if (!_playing) {
                _playing = true;

                _channel = _sound.play(_position, Max.INT_MAX_VALUE, soundTransform);
            }
        }
    }

    public function pause():Void {
        if (_channel != null && _playing) {
            _position = _channel.position;
            _channel.stop();

            Actuate.stop(_channel);

            _playing = false;
        }
    }

    public function fadeAndDestroy():Void {
        if (_channel != null) {
            Actuate.transform(_channel, 1)
                .sound(0)
                .onComplete(destroy);
        }
        else {
            destroy();
        }
    }

    private function destroy():Void {
        if (_channel != null) {
            Actuate.stop(_channel);

            _channel.stop();
            _channel = null;
        }

        if (_sound != null) {
            try {
                _sound.close();
            }
            catch (e:IOError) {
            }

            _sound = null;
        }
    }
}

