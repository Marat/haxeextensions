package com.moxisgames.framework.sound;

import starling.utils.Max;
import com.moxisgames.framework.animation.Animation;
import flash.media.Sound;
import flash.media.SoundTransform;

/**
 * Created by Marat on 27.10.2016.
 */
class FxPlayer {
    @:isVar public var volume(default, set):Int;

    public static var enabled:Bool;

    private var _sounds:Map<String, FxSound>;

    private var _transform:SoundTransform;

    private var _volume:Int = 50;

    private static var _instance:FxPlayer;

    public function new() {
        _sounds = new Map<String, FxSound>();

        _transform = new SoundTransform();

        volume = _volume;
    }

    public static function init():Void {
        _instance = new FxPlayer();
    }

    private function addSound(soundID:String, sound:Sound):Void {
        _sounds.set(soundID, new FxSound(sound, _transform));
    }

    public function playSound(soundID:String):Void {
        if (_sounds.exists(soundID)) {
            _sounds.get(soundID).play();
        }
    }

    public function playSoundLoop(soundID:String, count:Int = -1):Void {
        if (!enabled) {
            return;
        }

        if (count == -1) count = Max.INT_MAX_VALUE;

        if (_sounds.exists(soundID)) {
            _sounds.get(soundID).playLoop(count);
        }
    }

    public function stopSoundLoop(soundID:String):Void {
        if (_sounds.exists(soundID)) {
            _sounds.get(soundID).stop();
        }
    }

    private function set_volume(value:Int):Int {
        if (value < 0) {
            value = 0;
        }
        if (value > 100) {
            value = 100;
        }

        _volume = value;

        _transform.volume = _volume / 100;
        return value;
    }

    public static function setVolume(value:Int):Int {
        _instance.volume = value;
        return value;
    }

    public static function add(soundID:String, sound:Sound):Void {
        _instance.addSound(soundID, sound);
    }

    public static function play(soundID:String):Void {
        if (enabled) {
            _instance.playSound(soundID);
        }
    }

    public static function playDelayed(soundID:String, delay:Float, loop:Int = 0):Void {
        if (enabled) {
            if (loop == 0) {
                Animation.delay(delay, _instance.playSound, [soundID]);
            }
            else {
                Animation.delay(delay, _instance.playSoundLoop, [soundID, loop]);
            }
        }
    }

    public static function stopSounds():Void {
    }

    public static function getInstance():FxPlayer {
        return _instance;
    }
}

