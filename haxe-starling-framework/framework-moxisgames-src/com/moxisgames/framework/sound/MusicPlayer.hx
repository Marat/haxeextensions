package com.moxisgames.framework.sound;

import com.moxisgames.framework.logs.Logger;

/**
 * Created by mismagilov on 31.10.2016.
 */
class MusicPlayer {
    @:isVar public static var volume(never, set):Int = 50;
    public static var enabled(get, set):Bool;

    private static var _currentSound:MusicSound;

    private static var _enabled:Bool = false;

    public function new() {
    }

    public static function setCurrentTrack(soundPath:String):Void {
        if (_currentSound != null && _currentSound.path == soundPath) {
            return;
        }

        if (_currentSound != null) {
            _currentSound.fadeAndDestroy();
        }

        _currentSound = new MusicSound(soundPath);

        if (_enabled) {
            _currentSound.play();
        }
    }

    private static function set_volume(value:Int):Int {
        if (value < 0) {
            value = 0;
        }
        if (value > 100) {
            value = 100;
        }

        MusicSound.soundTransform.volume = value / 100;
        return value;
    }

    private static function get_enabled():Bool {
        return _enabled;
    }

    private static function set_enabled(value:Bool):Bool {
        _enabled = value;

        if (_currentSound != null) {
            if (_enabled) {
                _currentSound.play();
            }
            else {
                _currentSound.pause();
            }
        }
        return value;
    }

    public static function pause():Void {
        if (_currentSound != null) {
            _currentSound.pause();
        }
    }

    public static function play():Void {
        if (_enabled && _currentSound != null) {
            _currentSound.play();
        }
    }
}

