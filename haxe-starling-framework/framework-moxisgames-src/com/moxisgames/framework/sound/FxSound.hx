package com.moxisgames.framework.sound;

import starling.utils.Max;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;

/**
 * Created by Marat on 03.11.2016.
 */
class FxSound {
    private var _channel:SoundChannel;
    private var _sound:Sound;
    private var _transform:SoundTransform;

    @:allow(com.moxisgames.framework.sound)
    private function new(sound:Sound, transform:SoundTransform) {
        _sound = sound;
        _transform = transform;
    }

    public function play():Void {
        _channel = _sound.play(0, 0, _transform);
    }

    public function playLoop(count:Int = -1):Void {
        if (count == -1) count = Max.INT_MAX_VALUE;

        _channel = _sound.play(0, count, _transform);
    }

    public function stop():Void {
        if (_channel != null) {
            _channel.stop();
        }
    }
}

