package com.moxisgames.framework.cookie;

import flash.net.SharedObject;

class CookiesManager
{
    private var _cookies : Dynamic;
    
    private static var _instance : CookiesManager;
    
    private static var _allowInstantiate : Bool = false;
    
    public function new()
    {
        if (!_allowInstantiate)
        {
            throw "Use getInstance() for access to singletone instance";
        }
        
        _cookies = { };
    }
    
    public function getCookie(soName : String, secured : Bool = false) : Cookie
    {
        if (_cookies.exists(soName))
        {
            return try cast(Reflect.field(_cookies, soName), Cookie) catch(e:Dynamic) null;
        }
        
        var soData : SharedObject = SharedObject.getLocal(soName, "/");
        var cookie : Cookie = new Cookie(soData, soName, secured);
        Reflect.setField(_cookies, soName, cookie);
        return cookie;
    }
    
    public function unloadCookie(soName : String) : Void
    {
        if (_cookies.exists(soName))
        {
            (try cast(Reflect.field(_cookies, soName), Cookie) catch(e:Dynamic) null).destroy();
            Reflect.deleteField(_cookies, soName);
        }
    }
    
    public function flushAll() : Void
    {
        for (cookie/* AS3HX WARNING could not determine type for var: cookie exp: EIdent(_cookies) type: Dynamic */ in _cookies)
        {
            cookie.flush();
        }
    }
    
    public static function getInstance() : CookiesManager
    {
        if (_instance == null)
        {
            _allowInstantiate = true;
            _instance = new CookiesManager();
            _allowInstantiate = false;
        }
        
        return _instance;
    }
}
