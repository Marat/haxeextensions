package com.moxisgames.framework.cookie;

import flash.errors.Error;
import com.adobe.crypto.MD5;
import com.moxisgames.framework.logs.Logger;
import flash.net.SharedObject;
import flash.system.Capabilities;

class Cookie
{
    
    private var _soData : SharedObject;
    
    private var _busy : Bool = false;
    private var _changed : Bool = false;
    
    // secure params
    private var _secured : Bool = true;
    private var _s : String;
    private var _sortedParams : Array<Dynamic>;
    
    private static inline var P_KEY : String = "k";
    
    public function new(soData : SharedObject, name : String = "defaultCookieName", secured : Bool = false)
    {
        _soData = soData;
        _secured = secured;
        _s = "M" + name.charAt(0) + name.charAt(name.length - 1) + "mg_com";
        
        if (_secured)
        {
            _sortedParams = getFields();
        }
        
        // validate data, clear loaded data if not valid
        if (_secured && _sortedParams.length > 0 && getData(P_KEY) != gk())
        {
            if (Capabilities.isDebugger)
            {
                Logger.error("[Cookie] data is invalid! Clear cookies data!");
                Logger.log("[Cookie] = " + toString());
                Logger.warn("Stored key = " + getData(P_KEY));
                Logger.warn("Generated key = " + gk());
                
                throw new Error("Wrong secured SO!");
            }
            
            clear();
        }
    }
    
    private function getFields() : Array<Dynamic>
    {
        var fields : Array<Dynamic> = [];
        
        for (key in Reflect.fields(_soData.data))
        {
            if (key != P_KEY)
            {
                fields.push(key);
            }
        }
        
        fields.sort();
        
        return fields;
    }
    
    public function setData(varName : String, value : Dynamic, flushData : Bool = false) : Void
    {
        if (getData(varName) != value)
        {
            _soData.data[varName] = value;
            
            if (_secured)
            {
                if (Lambda.indexOf(_sortedParams, varName) == -1)
                {
                    _sortedParams.push(varName);
                    _sortedParams.sort();
                }
                
                updateProtectKey();
            }
            
            _changed = true;
        }
        
        if (flushData)
        {
            flush();
        }
    }
    
    public function flush() : Bool
    {
        if (!_changed || _busy)
        {
            //Logger.warning("[Cookie] can't flush _hasChanges=" + _changed + ", _busy=" + _busy);
            return false;
        }
        
        try
        {
            if (_secured)
            {
                updateProtectKey();
            }
            
            _soData.flush();
            _changed = false;
            return true;
        }
        catch (msg : String)
        {
            trace("[Cookie] flush error = " + msg);
            return false;
        }
        
        return false;
    }
    
    public function removeData(varName : String) : Void
    {
        if (hasData(varName))
        {
            if (_secured)
            {
                if (Lambda.indexOf(_sortedParams, varName) != -1)
                {
                    _sortedParams.splice(Lambda.indexOf(_sortedParams, varName), 1);
                }
                
                updateProtectKey();
            }
            
            _changed = true;
        }
    }
    
    private function updateProtectKey() : Void
    {
        _soData.data[P_KEY] = gk();
    }
    
    private function gk() : String
    {
        if (_sortedParams.length == 0)
        {
            return "";
        }
        
        var s : String = _s;
        
        var i : Int = 0;
        var l : Int = _sortedParams.length;
        while (i < l)
        {
            s += _sortedParams[i] + _soData.data[_sortedParams[i]];
            i++;
        }
        
        return MD5.hash(s);
    }
    
    public function toString() : String
    {
        var s : String = "[Cookie";
        
        var keys : Array<Dynamic> = getFields();
        
        for (key in keys)
        {
            s += " " + key + "=" + _soData.data[key];
        }
        
        return s + "]";
    }
    
    public function getData(varName : String, defaultValue : Dynamic = null, createIfNotExists : Bool = false) : Dynamic
    {
        if (!hasData(varName))
        {
            if (createIfNotExists)
            {
                setData(varName, defaultValue);
            }
            
            return defaultValue;
        }
        
        return _soData.data[varName];
    }
    
    public function getBool(varName : String, defaultValue : Bool = false, createIfNotExists : Bool = false) : Bool
    {
        if (!hasData(varName))
        {
            if (createIfNotExists)
            {
                setData(varName, defaultValue);
            }
            
            return defaultValue;
        }
        
        return _soData.data[varName];
    }
    
    public function getNumber(varName : String, defaultValue : Float = 0, createIfNotExists : Bool = false) : Float
    {
        return as3hx.Compat.parseFloat(getData(varName, Std.string(defaultValue), createIfNotExists));
    }
    
    public function getInt(varName : String, defaultValue : Int = 0) : Int
    {
        return Std.int(getData(varName, Std.string(defaultValue)));
    }
    
    public function getIntInBounds(varName : String, defaultValue : Int = 0, min : Int = 0, max : Int = 1) : Int
    {
        var value : Int = getInt(varName, defaultValue);
        
        if (value < min)
        {
            value = min;
        }
        else
        {
            if (value > max)
            {
                value = max;
            }
        }
        
        return value;
    }
    
    public function hasData(varName : String) : Bool
    {
        return _soData.data.exists(varName);
    }
    
    public function clear() : Void
    {
        _soData.clear();
        flush();
    }
    
    public function getStorage() : Dynamic
    {
        return _soData.data;
    }
    
    public function copyFrom(cookieFrom : Cookie) : Void
    {
        var storage : Dynamic = cookieFrom.getStorage();
        
        for (key in Reflect.fields(storage))
        {
            if (key != P_KEY)
            {
                setData(key, Reflect.field(storage, key));
            }
        }
    }
    
    public function destroy() : Void
    {
        _soData = null;
        
        _sortedParams = null;
    }
}
