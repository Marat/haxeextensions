package com.moxisgames.framework.data;

import flash.errors.Error;
import com.adobe.crypto.MD5;
import com.moxisgames.framework.utils.Random;

class SecureInt
{
    private var _rand : Int;
    private var _int : Int;
    
    private var _hash : String;
    
    private var S1 : String = Random.getUID(5);
    private static inline var S2 : String = "1b964Il81";
    
    public function new(val : Int = 0)
    {
        setValue(val);
    }
    
    public function getValue() : Int
    {
        if (getCurrentHash() != _hash)
        {
            throw new Error("Error: SecureInt validate failed");
            
            return -1;
        }
        
        setValue(_int - _rand);
        
        return Std.int(_int - _rand);
    }
    
    public function setValue(val : Int) : Void
    {
        generateSeed();
        
        _int = Std.int(val + _rand);
        
        _hash = getCurrentHash();
    }
    
    // SYNTAX SUGAR
    
    public function addValue(val : Int) : Void
    {
        setValue(getValue() + val);
    }
    
    public function removeValue(val : Int) : Void
    {
        setValue(getValue() - val);
    }
    
    // PRIVATE UTILS
    
    private function generateSeed() : Void
    {
        _rand = Random.intRanged(1, 999);
    }
    
    private function getCurrentHash() : String
    {
        return MD5.hash(S1 + _int + S2 + _rand);
    }
}
