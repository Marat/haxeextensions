package com.moxisgames.framework.locale;

import haxe.Constraints.Function;
import flash.events.Event;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.system.System;

class Locale
{
    public static var currLang(get, never) : String;

    
    private var _supportedLanguages : Array<Dynamic>;
    private var _langCode : String = "";
    private var _hash : Dynamic;
    private var _callback : Function;
    private var _busy : Bool = false;
    
    private static var _allowInstantiate : Bool = false;
    private static var _instance : Locale;
    
    public function new()
    {
        if (!_allowInstantiate)
        {
            throw "Use singleton!";
        }
        
        _hash = { };
    }
    
    public function init(availLanguages : Array<Dynamic>, currentLang : String, callback : Function = null) : String
    {
        _supportedLanguages = availLanguages;
        
        var langFound : Bool = false;
        
        for (i in 0...availLanguages.length)
        {
            if (currentLang.toLowerCase().indexOf(availLanguages[i].toLowerCase()) != -1)
            {
                currentLang = availLanguages[i];
                langFound = true;
                break;
            }
        }
        
        if (!langFound)
        {
            currentLang = availLanguages[0];
        }
        
        loadLang(currentLang, callback);
        
        return currentLang;
    }
    
    public function getCurrentLang() : String
    {
        return _langCode;
    }
    
    public function getString(stringID : String) : String
    {
        if (_hash.exists(stringID))
        {
            return Reflect.field(_hash, stringID);
        }
        
        return stringID;
    }
    
    public function loadLang(langCode : String, callback : Function = null) : Void
    {
        if (_langCode == langCode || _busy)
        {
            callback(false);
            return;
        }
        
        _busy = true;
        
        _langCode = langCode;
        
        _callback = callback;
        
        var loader : URLLoader = new URLLoader();
        loader.addEventListener(Event.COMPLETE, onXMLLoaded);
        loader.load(new URLRequest("./values/" + getCurrentLandAssetName() + ".xml"));
    }
    
    private function onXMLLoaded(event : Event) : Void
    {
        (try cast(event.currentTarget, URLLoader) catch(e:Dynamic) null).removeEventListener(Event.COMPLETE, onXMLLoaded);
        
        var xml : FastXML = new FastXML((try cast(event.currentTarget, URLLoader) catch(e:Dynamic) null).data);
        var lib : FastXMLList = xml.node.elements.innerData();
        
        for (node in lib)
        {
            var src : String = Std.string(node);
            
            if (src.indexOf("\\n") != -1)
            {
                Reflect.setField(_hash, Std.string(node.node.attribute.innerData("name")), src.split("\\n").join("\n"));
            }
            else
            {
                Reflect.setField(_hash, Std.string(node.node.attribute.innerData("name")), src);
            }
        }
        
        callback(true);
        
        _busy = false;
        
        System.disposeXML(xml);
    }
    
    private function getCurrentLandAssetName() : String
    {
        return "strings_" + _langCode;
    }
    
    private function callback(value : Bool) : Void
    {
        if (_callback != null)
        {
            _callback(value);
            _callback = null;
        }
    }
    
    public static function _(stringID : String) : String
    {
        return _instance.getString(stringID);
    }
    
    private static function get_currLang() : String
    {
        return _instance.getCurrentLang();
    }
    
    public static function getInstance() : Locale
    {
        if (_instance == null)
        {
            _allowInstantiate = true;
            _instance = new Locale();
            _allowInstantiate = false;
        }
        
        return _instance;
    }
}
