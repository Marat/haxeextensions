package com.moxisgames.framework.core;

import msignal.Signal.Signal1;
import haxe.Constraints.Function;
import com.moxisgames.framework.utils.Platform;
import com.moxisgames.framework.utils.StringUtils;
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.UncaughtErrorEvent;
import starling.utils.MathUtil;

class CoreApp extends Sprite {
    @:isVar public static var appVersion(default, null):String;
    @:isVar public static var appId(default, null):String;
    @:isVar public static var appIsActive(default, null):Bool = true;
    @:isVar public static var onActiveChanged(default, never):Signal1<Bool> = new Signal1<Bool>();

    // скейлы внутригровых размеров
    @:isVar public static var gameAreaSizeFactor(default, null):Float;
    // скейл стейджа старлинга
    @:isVar public static var stageScale(default, null):Float;

    // текущие размеры внутриигровой области
    @:isVar public static var gameWidth(default, null):Int = 0;
    @:isVar public static var gameHeight(default, null):Int = 0;
    // текущие размеры
    @:isVar public static var stageWidth(default, null):Int = 0;
    @:isVar public static var stageHeight(default, null):Int = 0;
    // изначально заданные размеры игровой области 100%
    @:isVar public static var defaultGameWidth(default, null):Int = 0;
    @:isVar public static var defaultGameHeight(default, null):Int = 0;

    private var _needScaleStage:Bool;

    private var _orientation:Int = ORIENTATION_AUTO;

    public static inline var ORIENTATION_AUTO:Int = 0;
    public static inline var ORIENTATION_LANDSCAPE:Int = 1;
    public static inline var ORIENTATION_PORTRAIT:Int = 2;

    public function new(defaultGameWidth:Int, defaultGameHeight:Int, orientation:Int, needScaleStage:Bool = true) {
        super();

        appVersion = "0.1";
        appId = "com.moxisgames.ladyfortune";

        _orientation = orientation;
        _needScaleStage = needScaleStage;

        CoreApp.defaultGameWidth = defaultGameWidth;
        CoreApp.defaultGameHeight = defaultGameHeight;

        addEventListener(Event.ADDED_TO_STAGE, addedToStage);
    }

    /*public static function getAppInfo():String {
        if (Platform.isAndroid()) {
            _appDirPath = "/mnt/sdcard/Android/data/" + _appID + "/";
        }
        else {
            if (Platform.isIOS()) {
                _appDirPath = File.userDirectory.nativePath + "/Documents/";
            }
            else {
                _appDirPath = File.userDirectory.nativePath + "/Documents/";
            }
        }

        return "appID=" + _appID + ", appVersion=" + _appVersion + ", appDirPath=" + _appDirPath;
    }*/

    // zoppo
    //[LadyFortune] 0.660: stage 1794x1005
    //[LadyFortune] 0.662: fullScreen 1794x1080

    // Asus 5
    //[LadyFortune] 3.200: stage 1280x670
    //[LadyFortune] 3.208: fullScreen 1280x720

    // planshet
    //[LadyFortune] 0.684: stage 1920x1054
    //[LadyFortune] 0.686: fullScreen 1920x1104 +

    // HTC incredible
    //[LadyFortune] 1.837: stage 800x442
    //[LadyFortune] 1.840: fullScreen 800x480

    private function defineStageSizes():Void {
        //setScreenSizes(stage.fullScreenWidth, stage.fullScreenHeight);
        setScreenSizes(stage.stageWidth, stage.stageHeight);
    }

    private function setScreenSizes(w:Int, h:Int):Void {
        if (_orientation == ORIENTATION_AUTO) {
            stageWidth = w;
            stageHeight = h;
        }
        else {
            if (_orientation == ORIENTATION_LANDSCAPE) {
                stageWidth = (w > h) ? w : h;
                stageHeight = (w < h) ? w : h;
            }
            else {
                if (_orientation == ORIENTATION_PORTRAIT) {
                    stageWidth = (w < h) ? w : h;
                    stageHeight = (w > h) ? w : h;
                }
            }
        }
    }

    private function calculateSizes(scaleStage:Bool = false):Void {
        var minScale:Float = MathUtil.min([stageWidth / defaultGameWidth, stageHeight / defaultGameHeight]);

        // растягиваем стейдж
        if (scaleStage) {
            gameAreaSizeFactor = 1;

            stageScale = minScale;

            gameWidth = Std.int(defaultGameWidth + (stageWidth / minScale - defaultGameWidth));
            gameHeight = Std.int(defaultGameHeight + (stageHeight / minScale - defaultGameHeight));
        }
        else {
            // растягиваем все размеры в игре
            {
                gameAreaSizeFactor = minScale;

                stageScale = 1;

                gameWidth = stageWidth;
                gameHeight = stageHeight;
            }
        }
    }

    public static function scaleNum(value:Float):Float {
        return value * gameAreaSizeFactor;
    }

    public static function scaleInt(value:Int):Int {
        return Math.round(value * gameAreaSizeFactor);
    }

    // HANDLERS

    private function addedToStage(event:Event):Void {
        removeEventListener(Event.ADDED_TO_STAGE, addedToStage);

        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;

        defineStageSizes();

        calculateSizes(_needScaleStage);

        stage.addEventListener(Event.ACTIVATE, activateHandler);
        stage.addEventListener(Event.DEACTIVATE, deactivateHandler);
        // stage.addEventListener(Event.RESIZE, resizeHandler);

        complete();
    }

    private function activateHandler(event:Event):Void {
        if (!appIsActive) {
            appIsActive = true;

            appActivate();

            onActiveChanged.dispatch(true);
        }
    }

    /*private function resizeHandler(event:Event):Void {
        trace("[Core] resizeHandler");
        trace("[Core] stage.fullStageSize " + stage.fullScreenWidth + "x" + stage.fullScreenHeight);
    }*/

    private function deactivateHandler(event:Event):Void {
        if (appIsActive) {
            appIsActive = false;

            appDeactivate();

            onActiveChanged.dispatch(false);
        }
    }

    // OVERRIDE

    private function complete():Void {
    }

    private function appActivate():Void {
    }

    private function appDeactivate():Void {
    }

    private function uncaughtError(message:String):Void {
        trace("Error uncaughtError. Message" + message);
    }

    public static function getAppBuildTime():Date {
        return Date.now();
    }

    public static function getAppBuildTimeAsTimestamp():String {
        var date:Date = getAppBuildTime();
        var lz:Function = StringUtils.leadZero;

        return lz(date.getDate()) + "." + lz(date.getMonth() + 1) + "." + date.getFullYear() + " " +
               lz(date.getHours()) + ":" + lz(date.getMinutes()) + ":" + lz(date.getSeconds());
    }
}
