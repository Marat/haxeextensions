package com.moxisgames.framework.utils;

import starling.display.Canvas;

/**
	 * Created by Marat on 16.02.2017.
	 */
class StarlingGuiUtils
{
    public function new()
    {
    }
    
    public static function createRoundRect(width : Int, height : Int, radius : Int, color : Int) : Canvas
    {
        if (radius * 2 > height)
        {
            radius = Std.int(height * 0.5) ^ 0;
        }
        
        if (radius * 2 > width)
        {
            radius = Std.int(width * 0.5) ^ 0;
        }
        
        var canvas : Canvas = new Canvas();
        canvas.beginFill(color);
        
        canvas.drawRectangle(0, radius, width, height - 2 * radius);
        canvas.drawRectangle(radius, 0, width - 2 * radius, height);
        canvas.drawCircle(radius, radius, radius);
        canvas.drawCircle(radius, height - radius, radius);
        canvas.drawCircle(width - radius, radius, radius);
        canvas.drawCircle(width - radius, height - radius, radius);
        
        canvas.endFill();
        return canvas;
    }
}

