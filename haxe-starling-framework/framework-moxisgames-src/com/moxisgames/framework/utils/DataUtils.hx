package com.moxisgames.framework.utils;


class DataUtils
{
    public function new()
    {
    }
    
    public static function objectToArray(obj : Dynamic) : Array<Dynamic>
    {
        var arr : Array<Dynamic> = [];
        
        for (item/* AS3HX WARNING could not determine type for var: item exp: EIdent(obj) type: Dynamic */ in obj)
        {
            arr.push(item);
        }
        
        return arr;
    }
    
    public static function vectorToArray(vector : Array<Dynamic>) : Array<Dynamic>
    {
        var arr : Array<Dynamic> = [];
        
        var i : Int = 0;
        var l : Int = vector.length;
        while (i < l)
        {
            arr[i] = vector[i];
            i++;
        }
        
        return arr;
    }
    
    public static function copyArrayToVectorInt(arr : Array<Dynamic>) : Array<Int>
    {
        var vector : Array<Int> = new Array<Int>();
        
        var i : Int = 0;
        var l : Int = arr.length;
        while (i < l)
        {
            vector[i] = arr[i];
            i++;
        }
        
        return vector;
    }
    
    public static function copyArrayToVectorString(arr : Array<Dynamic>) : Array<String>
    {
        var vector : Array<String> = new Array<String>();
        
        var i : Int = 0;
        var l : Int = arr.length;
        while (i < l)
        {
            vector[i] = arr[i];
            i++;
        }
        
        return vector;
    }
    
    public static function clearArray(arr : Array<Dynamic>) : Void
    {
        if (arr == null)
        {
            return;
        }
        
        as3hx.Compat.setArrayLength(arr, 0);
    }
    
    public static function clearObject(obj : Dynamic) : Void
    {
        if (obj == null)
        {
            return;
        }
        
        var keys : Array<Dynamic> = [];
        
        for (key in Reflect.fields(obj))
        {
            keys.push(key);
        }
        
        var i : Int = 0;
        var l : Int = keys.length;
        while (i < l)
        {
            Reflect.deleteField(obj, keys[i]);
            i++;
        }
    }
    
    public static function removeFromArray(arr : Array<Dynamic>, item : Dynamic) : Void
    {
        if (arr == null)
        {
            return;
        }
        
        var index : Int = Lambda.indexOf(arr, item);
        
        if (index != -1)
        {
            arr.splice(index, 1);
        }
    }
    
    public static function mergeObjects(objects : Array<Dynamic> = null) : Dynamic
    {
        var result : Dynamic = { };
        
        for (i in 0...objects.length)
        {
            var object : Dynamic = objects[i];
            
            for (key in Reflect.fields(object))
            {
                Reflect.setField(result, key, Reflect.field(object, key));
            }
        }
        
        return result;
    }
    
    public static function keys(object : Dynamic) : Array<Dynamic>
    {
        var arr : Array<Dynamic> = [];
        
        for (key in Reflect.fields(object))
        {
            arr.push(key);
        }
        
        return arr;
    }
}
