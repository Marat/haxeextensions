package com.moxisgames.framework.utils;

import com.moxisgames.framework.logs.Logger;

/**
	 * ...
	 * @author mismagilov
	 */
class PerformanceUtils {
    private static var _timesHash:Map<String, Int> = new Map<String, Int>();

    public function new() {
    }

    public static function time(id:String = "default"):Void {
        Logger.log(">> MEASURE " + id + " started");

        _timesHash.set(id, Math.round(haxe.Timer.stamp() * 1000));
    }

    public static function timeEnd(id:String = "default"):Int {
        if (_timesHash.exists(id)) {
            var time:Int = Std.int(Math.round(haxe.Timer.stamp() * 1000) - _timesHash.get(id));

            Logger.log(">> MEASURE " + id + " finished in " + time + " ms.");

            _timesHash.remove(id);

            return time;
        }

        return 0;
    }
}
