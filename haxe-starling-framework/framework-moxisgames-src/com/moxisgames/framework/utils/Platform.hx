package com.moxisgames.framework.utils;

import flash.system.Capabilities;

class Platform
{
    
    private static var _osID : Int = -1;
    
    private static inline var OS_ANDROID : String = "android";
    private static inline var OS_IOS : String = "ios";
    private static inline var OS_WINDOWS : String = "windows";
    
    private static inline var WIN_ID : Int = 0;
    private static inline var ANDR_ID : Int = 1;
    private static inline var IOS_ID : Int = 2;
    
    public function new()
    {
    }
    
    public static function init() : Void
    {
        var os : String = Capabilities.manufacturer.toLowerCase();
        
        if (os.indexOf(OS_IOS) > -1)
        {
            _osID = IOS_ID;
        }
        else
        {
            if (os.indexOf(OS_ANDROID) > -1)
            {
                _osID = ANDR_ID;
            }
            else
            {
                if (os.indexOf(OS_WINDOWS) > -1)
                {
                    _osID = WIN_ID;
                }
            }
        }
    }
    
    public static function isWindows() : Bool
    {
        return _osID == WIN_ID;
    }
    
    public static function isAndroid() : Bool
    {
        return _osID == ANDR_ID;
    }
    
    public static function isIOS() : Bool
    {
        return _osID == IOS_ID;
    }
    
    public static function isMobile() : Bool
    {
        return isIOS() || isAndroid();
    }
    
    public static function getPlatformID() : String
    {
        if (_osID == WIN_ID)
        {
            return "windows";
        }
        else
        {
            if (_osID == ANDR_ID)
            {
                return "android";
            }
        }
        
        return "ios";
    }
}

