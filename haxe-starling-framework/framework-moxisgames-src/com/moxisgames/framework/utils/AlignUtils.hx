package com.moxisgames.framework.utils;

import starling.display.DisplayObject;

class AlignUtils
{
    public static function alignBy(target : DisplayObject, alignBy : DisplayObject, byX : Bool = true, byY : Bool = true) : Void
    {
        if (byX)
        {
            target.x = Std.int(alignBy.x + (alignBy.width - target.width) * 0.5);
        }
        
        if (byY)
        {
            target.y = Std.int(alignBy.y + (alignBy.height - target.height) * 0.5);
        }
    }

    public function new()
    {
    }
}
