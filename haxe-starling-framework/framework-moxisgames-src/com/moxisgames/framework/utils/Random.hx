package com.moxisgames.framework.utils;


class Random
{
    public static function random() : Float
    {
        return Math.random();
    }
    
    public static function boolean(value : Float = 0.5) : Bool
    {
        return random() < value;
    }
    
    public static function chance(value : Int) : Bool
    {
        return boolean(value / 100);
    }
    
    /**
		 * Returns either a 1 or -1. 
		 * 
		 * @param	Chance	The chance of receiving a positive value. Should be given as a number between 0 and 100 (effectively 0% to 100%)
		 * @return	1 or -1
		 */
    public static function sign() : Int
    {
        return (boolean()) ? 1 : -1;
    }
    
    /** Return a random integer between 'from' and 'to', inclusive. */
    public static function intRanged(from : Int, to : Int) : Int
    {
        return Std.int(from + ((random() * (to - from + 1)) ^ 0));
    }
    
    public static function numRanged(from : Float, to : Float) : Float
    {
        return from + (to - from) * random();
    }
    
    public static function shuffleArray(array : Array<Dynamic>) : Array<Dynamic>
    {
        var temp : Dynamic;
        var index : Int;
        
        var i : Int = 0;
        var l : Int = array.length;
        while (i < l)
        {
            index = Std.int(i + (random() * (array.length - i) ^ 0));
            temp = array[i];
            array[i] = array[index];
            array[index] = temp;
            i++;
        }
        
        return array;
    }
    
    public static function getArrayRandomElement(array : Array<Dynamic>, removeFromArray : Bool = false) : Dynamic
    {
        var index : Int = Std.int(Math.random() * array.length) ^ 0;
        
        var value : Dynamic = array[index];
        
        if (removeFromArray)
        {
            array.splice(index, 1);
        }
        
        return value;
    }
    
    public static function getUID(numChars : Int = 10) : String
    {
        var CHARS : String = "abcdefghijklnrstxyzABDEFGHIJKLMNOPQRSTXYZ1234567890";
        var CHARS_LEN : Int = CHARS.length;
        var retval : String = "";
        
        for (i in 0...numChars)
        {
            retval += CHARS.charAt(intRanged(0, CHARS_LEN - 1));
        }
        
        return retval;
    }

    public function new()
    {
    }
}
