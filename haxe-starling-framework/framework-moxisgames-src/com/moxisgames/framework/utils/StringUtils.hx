package com.moxisgames.framework.utils;

import flash.utils.Dictionary;

class StringUtils {
    public static inline var SEC_IN_DAY:Int = 86400;
    public static inline var SEC_IN_HOUR:Int = 3600;
    public static inline var SEC_IN_MIN:Int = 60;

    public function new() {
    }

    /**
		 * @param	timeInSeconds
		 * @return time D HH:MM:SS
		 */
    public static function getTimeString(timeInSeconds:Int):String {
        if (timeInSeconds < 0) {
            timeInSeconds = 0;
        }

        var str:String = "";

        var days:Int = Std.int(timeInSeconds / SEC_IN_DAY) ^ 0;

        if (days > 0) {
            timeInSeconds -= (days * SEC_IN_DAY);
            str += days + " ";
        }

        var hours:Int = Std.int(timeInSeconds / SEC_IN_HOUR) ^ 0;

        if (hours != 0 || days != 0) {
            timeInSeconds -= hours * SEC_IN_HOUR;
            str += leadZero(hours) + ":";
        }

        var mins:Int = Std.int(timeInSeconds / SEC_IN_MIN) ^ 0;

        if (mins != 0 || hours != 0 || days != 0) {
            timeInSeconds -= mins * SEC_IN_MIN;
            str += leadZero(mins) + ":";
        }

        var seconds:Int = Std.int(timeInSeconds % SEC_IN_MIN);

        str += leadZero(seconds);

        return str;
    }

    /**
		 * @param	timeInSeconds
		 * @return [D,H,M,S]
		 */
    public static function splitTimeInParams(timeInSeconds:Int):Array<Dynamic> {
        if (timeInSeconds < 0) {
            timeInSeconds = 0;
        }

        var days:Int = Std.int(timeInSeconds / SEC_IN_DAY) ^ 0;

        if (days > 0) {
            timeInSeconds -= (days * SEC_IN_DAY);
        }

        var hours:Int = Std.int(timeInSeconds / SEC_IN_HOUR) ^ 0;

        if (hours != 0 || days != 0) {
            timeInSeconds -= hours * SEC_IN_HOUR;
        }

        var mins:Int = Std.int(timeInSeconds / SEC_IN_MIN) ^ 0;

        if (mins != 0 || hours != 0 || days != 0) {
            timeInSeconds -= mins * SEC_IN_MIN;
        }

        var seconds:Int = Std.int(timeInSeconds % SEC_IN_MIN);

        return [days, hours, mins, seconds];
    }

    public static function removeWhitespacesInString(str:String):String {
        //удаление пробелов в начале строки
        while ((str.charAt(0) == " ") && str.length > 0) {
            str = str.substring(1, str.length);
        }

        //удаление пробелов в конце строки
        while ((str.charAt(str.length - 1) == " ") && str.length > 0) {
            str = str.substring(0, str.length - 1);
        }

        return str;
    }

    public static function replaceParams(str:String, params:Array<Dynamic>):String {
        if (str == "" || str == null) {
            return "";
        }

        var i:Int = 0;
        var l:Int = params.length;
        while (i < l) {
            str = insertParam(str, i, Std.string(params[i]));
            i++;
        }

        return str;
    }

    private static function insertParam(str:String, paramIndex:Int, paramValue:String):String {
        var searchStr:String = "{" + paramIndex + "}";
        var i:Int;

        i = str.lastIndexOf(searchStr);
        if (i == -1) {
            return str;
        }

        while (i != -1) {
            str = str.substring(0, i) + paramValue + str.substring(i + searchStr.length, str.length);
            i = str.indexOf(searchStr);
        }

        return str;
    }

    /*public static function translit(str:String):String
		{
			var arrRUSSIAN:Array = ["А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Э","Ю","Я","а","б","в","г","д","е","ё","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","ь","ы","ъ","э","ю","я"," "];
			var arrTRANSLATE:Array = ["A","B","V","G","D","E","E","Zh","Z","I","I","K","L","M","N","O","P","R","S","T","U","F","H","C","Ch","Sh","Sh'","E","Yu","Ya","a","b","v","g","d","e","e","zh","z","i","i","k","l","m","n","o","p","r","s","t","u","f","h","c","ch","sh","sh'","'","i","'","e","yu","ya","_"];
			var i:int = str.length;
			while (i < arrRUSSIAN.length)
			{
				str = (str.split(arrRUSSIAN[i])).join(arrTRANSLATE[i]);
				i++;
			}
			return str;
		}*/
/*
    private static var _replaceAlphabet:Dictionary<String, String>;

    public static function translit(__DOLLAR__string:String):String {
        var i:Int = 0;

        if (_replaceAlphabet == null) {
            _replaceAlphabet = new Dictionary<String, String>(true);

            // Сначала заменяем "односимвольные" фонемы.
            var cyr:String = "абвгдеёзийклмнопрстуфхъыэ";
            var lat:String = "abvgdeeziyklmnoprstufh'iei";

            var cyrPhonems:Array<String> = cyr.split("");
            var latPhonems:Array<String> = lat.split("");

            for (i in 0...cyrPhonems.length) {
                _replaceAlphabet[cyrPhonems[i]] = latPhonems[i];
            }

            // Сначала заменяем "односимвольные" фонемы.
            cyr = "АБВГДЕЁЗИЙКЛМНОПРСТУФХЪЫЭ";
            lat = "ABVGDEEZIYKLMNOPRSTUFH'IEI";

            cyrPhonems = cyr.split("");
            latPhonems = lat.split("");

            for (i in 0...cyrPhonems.length) {
                _replaceAlphabet[cyrPhonems[i]] = latPhonems[i];
            }

            _replaceAlphabet["ж"] = "zh";
            _replaceAlphabet["ц"] = "ts";
            _replaceAlphabet["ч"] = "ch";
            _replaceAlphabet["ш"] = "sh";
            _replaceAlphabet["щ"] = "shch";
            _replaceAlphabet["ь"] = "";
            _replaceAlphabet["ю"] = "yu";
            _replaceAlphabet["я"] = "ya";

            _replaceAlphabet["Ж"] = "ZH";
            _replaceAlphabet["Ц"] = "TS";
            _replaceAlphabet["Ч"] = "CH";
            _replaceAlphabet["Ш"] = "SH";
            _replaceAlphabet["Щ"] = "SHCH";
            _replaceAlphabet["Ь"] = "";
            _replaceAlphabet["Ю"] = "YU";
            _replaceAlphabet["Я"] = "YA";

            _replaceAlphabet["ї"] = "i";
            _replaceAlphabet["Ї"] = "Yi";
            _replaceAlphabet["є"] = "ie";
            _replaceAlphabet["Є"] = "Ye";
        }

        //replace only existing chars
        var newString:String = "";
        var oldStrArray:Array<Dynamic> = __DOLLAR__string.split("");
        for (i in 0...oldStrArray.length) {
            (newString != null += (_replaceAlphabet[oldStrArray[i]] != null)) ? _replaceAlphabet[oldStrArray[i]] : oldStrArray[i];
        }

        return newString;
    }
*/

    public static function cutString(string:String, length:Int):String {
        if (string.length > length) {
            string = string.substr(0, length) + "...";
        }

        return string;
    }

    public static function formatPrice(num:Float, separator:String = " "):String {
        if (num < 1000) {
            return Std.string(num);
        }

        num = Std.int(num);
        var len:Int = Std.string(num).length;

        var pos:Int = Std.int(len - Std.int((len - 1) / 3) * 3);
        var str:String = Std.string(num).substr(0, pos);

        while (pos < len) {
            str += separator + Std.string(num).substr(pos, 3);
            pos += 3;
        }

        return str;
    }

    /**
		 * Метод для правильной работы с числительными
		 * 
		 * @param	count - число
		 * @param	form1 - форма обозначения числа в кол-ве 1
		 * @param	form2 - форма обозначения числа в кол-ве 2
		 * @param	form5 - форма обозначения числа в кол-ве 5
		 * @return - возращает строку в правильной форме 
		 */
    public static function pluralForm(count:Int, form1:String, form2:String, form5:String, addCountToString:Bool = true):String {
        var defaultCount:Int = count;
        count = Std.int(Math.abs(count) % 100);

        var n1:Int = Std.int(count % 10);

        var form:String = "";

        if (count > 10 && count < 20) {
            form = form5;
        }
        else {
            if (n1 > 1 && n1 < 5) {
                form = form2;
            }
            else {
                if (n1 == 1) {
                    form = form1;
                }
                else {
                    form = form5;
                }
            }
        }

        if (addCountToString) {
            return defaultCount + " " + form;
        }

        return form;
    }

    public static function leadZero(value:Int):String {
        if (value < 10) {
            return "0" + value;
        }
        return Std.string(value);
    }

    public static function leadZeros(value:Int, length:Int = 4):String {
        var strValue:String = Std.string(value);

        while (strValue.length < length) {
            strValue = "0" + strValue;
        }

        return strValue;
    }
}
