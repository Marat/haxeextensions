package com.moxisgames.framework.logs;

import com.moxisgames.framework.core.CoreApp;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;
import starling.text.TextFormat;
import starling.utils.Color;

/**
	 * ...
	 * @author mismagilov
	 */
class LogOutputPanel extends Sprite
{
    public var textField(get, never) : TextField;

    private var _bg : Quad;
    private var _txt : TextField;
    
    private var _open : Bool = false;
    
    @:meta(Embed(source="../../../../../framework-moxisgames-assets/fonts/stan0755.ttf",embedAsCFF="false",fontFamily="stan"))

    private static var StandartFont : Class<Dynamic>;
    
    public function new(w : Int, h : Int)
    {
        super();
        
        _bg = new Quad(w, h);
        _bg.alpha = 0.9;
        _bg.touchable = false;
        
        _txt = new TextField(w, h, "10", new TextFormat("stan", CoreApp.scaleInt(14), 0x000000, "left", "top"));
        //			_txt.y = 10;
        _txt.border = true;
        _txt.touchable = false;
        
        var btnQuad : Quad = new Quad(CoreApp.scaleInt(40), CoreApp.scaleInt(40), Color.NAVY);
        btnQuad.addEventListener(TouchEvent.TOUCH, onTouch);
        addChild(btnQuad);
    }
    
    private function onTouch(e : TouchEvent) : Void
    {
        if (e.touches[0].phase == TouchPhase.BEGAN)
        {
            if (!_open)
            {
                addChildAt(_txt, 0);
                addChildAt(_bg, 0);
            }
            else
            {
                removeChild(_txt);
                removeChild(_bg);
            }
            
            _open = !_open;
        }
    }
    
    private function get_textField() : TextField
    {
        return _txt;
    }
}
