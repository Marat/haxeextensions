package com.moxisgames.framework.logs;

import openfl.Lib;

class Logger {
    #if html5
    static private var _startTime:Float;
    #end

    private static var _appPrefix:String = "";

    public static var debug:Bool = false;

    public function new() {
    }

    public static function setAppPrefix(appPrefix:String):Void {
        #if html5
        _startTime = Lib.getTimer();
        #end

        _appPrefix = "[" + appPrefix + "] ";
    }

    public static function log(str:String):Void {
        if (debug) {
            str = _appPrefix + getTime() + ": " + str;

            trace(str);
        }
    }

    public static function warning(str:String):Void {
        if (debug) {
            str = "Warning! " + _appPrefix + getTime() + ": " + str;

            Lib.trace(str);
        }
    }

    public static function error(str:String):Void {
        if (debug) {
            str = "ERROR! " + _appPrefix + getTime() + ": " + str;

            Lib.trace(str);
        }
    }

    private static function getTime():Float {
        #if html5
        return Math.floor((Lib.getTimer() - _startTime) / 10) / 100;
        #else
        return Math.floor(Lib.getTimer() / 10) / 100;
        #end
    }
}
