package com.moxisgames.framework.profiling;

import com.moxisgames.framework.core.CoreApp;
import openfl.Assets;
import starling.utils.HAlign;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.system.System;
import starling.core.Starling;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.*;
//import starling.rendering.Painter;
import starling.text.BitmapFont;
import starling.text.TextField;
import starling.textures.Texture;
//import starling.utils.Align;

/**
	 * ...
	 * @author mismagilov
	 */
class FPSCounter extends Sprite {
    private var _w:Int;
    private var _h:Int;
    private var _hMem:Int;

    private var _currentFPS:Int;
    private var _maxFPS:Int;

    private var _prevTime:Float;
    private var _now:Float;
    private var _delta:Float;
    private var _avgTime:Float;

    private var _currMemory:Float;
    private var _maxMemory:Float = 0;
    private var _memUpdateCounter:Int = 0;

    private var _cacheCount:Int;
    private var _times:Array<Float>;

    // private var _painter : Painter;

    private var _tmpValue:Dynamic;
    private var _perfValues:Array<Dynamic>;
    private var _pointer:Int;
    private var _timeOffset:Int;
    private var _currentCount:Int;

    // ui

    private var _bData:BitmapData;
    private var _image:Image;

    private var _txtFps:TextField;
    private var _txtMem:TextField;
    private var _txtDrw:TextField;

    static private inline var UPDATE_INTERVAL:Int = 30;
    static private inline var TIME_SCALE:Int = 3;

    static private inline var COLOR_BG:Int = 0x000033;
    static private inline var COLOR_FPS:Int = 0xffff00;
    static private inline var COLOR_MS:Int = 0x00ff00;
    static private inline var COLOR_MEM:Int = 0x00ffff;
    static private inline var COLOR_DRW:Int = 0xff0070;

    static private inline var TXT_FPS:String = "FPS: ";
    static private inline var TXT_MEM:String = "MEM: ";
    static private inline var TXT_DRW:String = "DRW: ";

    private var _drawArea:Rectangle;

    public function new(w:Int = 120, h:Int = 60, maxFPS:Int = 60) {
        super();

        _w = w;
        _h = h;
        _hMem = Std.int(_h / 2);
        _maxFPS = maxFPS;
        _avgTime = Math.ceil((1 / _maxFPS) * 1000) / 1000;

        _currentFPS = 0;
        _cacheCount = 0;
        _times = [];

        //alpha = 0.5;

        init();
    }

    private function init():Void {
//        _painter = Starling.current.painter;

        var spacer:Float = 4;
        var fontSize:Int = 10;
        var fontFamily:String = "standard 07_55";

        //var fontBitmap : Bitmap = Type.createInstance(StandardAtlas, []);
        var fontTexture:Texture = Texture.fromBitmapData(Assets.getBitmapData("font.png"));
        var fontXML:Xml = Xml.parse(Assets.getText("font.fnt"));
        TextField.registerBitmapFont(new BitmapFont(fontTexture, fontXML), fontFamily);

        _drawArea = new Rectangle(0, 0, _w, _h);

        _bData = new BitmapData(_w, _h, false, COLOR_BG);
        _image = new Image(Texture.fromBitmapData(_bData));

        _txtFps = new TextField(_w, 14, TXT_FPS);
        _txtFps.touchable = false;
        _txtFps.fontName = fontFamily;
        _txtFps.fontSize = fontSize;
        _txtFps.color = COLOR_FPS;
        _txtFps.hAlign = HAlign.LEFT;
        _txtFps.x = spacer;
        _txtFps.y = spacer;

        _txtMem = new TextField(_w, 14, TXT_MEM);
        _txtMem.touchable = false;
        _txtMem.fontName = fontFamily;
        _txtMem.fontSize = fontSize;
        _txtMem.color = COLOR_MEM;
        _txtMem.hAlign = HAlign.LEFT;
        _txtMem.x = spacer;
        _txtMem.y = _txtFps.y + _txtFps.height - spacer;

        _txtDrw = new TextField(_w, 14, TXT_DRW);
        _txtDrw.touchable = false;
        _txtDrw.fontName = fontFamily;
        _txtDrw.fontSize = fontSize;
        _txtDrw.color = COLOR_DRW;
        _txtDrw.hAlign = HAlign.LEFT;
        _txtDrw.x = spacer;
        _txtDrw.y = _txtMem.y + _txtMem.height - spacer;

        _perfValues = new Array<Dynamic>();

        for (i in 0..._w) {
            _perfValues[i] = {
                fps : 0,
                time : 0,
                mem : 0
            };
        }

        addEventListener(Event.ADDED_TO_STAGE, onAdded);
    }

    private function onAdded(event:Event):Void {
        removeEventListener(Event.ADDED_TO_STAGE, onAdded);

        addChild(_image);
        addChild(_txtFps);
        addChild(_txtMem);
        addChild(_txtDrw);

        CoreApp.onActiveChanged.add(onActiveChanged);

        addEventListener(TouchEvent.TOUCH, click);
        addEventListener(Event.ENTER_FRAME, update);
    }

    private function onActiveChanged(isActive:Bool):Void {
        if (isActive) {
            _prevTime = Math.round(haxe.Timer.stamp() * 1000) / 1000;

            addEventListener(Event.ENTER_FRAME, update);
        }
        else {
            removeEventListener(Event.ENTER_FRAME, update);
        }
    }

    private function click(event:TouchEvent):Void {
        var touch:Touch = event.getTouch(this);

        if (touch != null) {
            if (touch.phase == TouchPhase.BEGAN) {
                _memUpdateCounter = UPDATE_INTERVAL;

                _txtMem.text = "Clean...";

                System.gc();
                System.gc();
            }
        }
    }

    private function update(event:EnterFrameEvent):Void {
        // FPS
        _now = Math.round(haxe.Timer.stamp() * 1000) / 1000;
        _delta = Math.ceil((_now - _prevTime) * 1000) / 1000;
        _prevTime = _now;

        _times.push(_now);

        while (_times[0] < _now - 1) {
            _times.shift();
        }

        _currentCount = _times.length;
        _currentFPS = Math.round((_currentCount + _cacheCount) / 2);

        if (_currentFPS > _maxFPS) {
            _currentFPS = _maxFPS;
        }

        _cacheCount = _currentCount;

        _txtFps.text = TXT_FPS + _currentFPS;

        // memory
        if (_memUpdateCounter == 0) {
            _memUpdateCounter = UPDATE_INTERVAL;

            _currMemory = Std.int((System.totalMemory * 0.000000954) * 1000) / 1000;

            if (_currMemory > _maxMemory) {
                _maxMemory = _currMemory;
            }

            _txtMem.text = TXT_MEM + _currMemory + "/" + _maxMemory;
        }
        else {
            _memUpdateCounter--;
        }

        // drw
        _txtDrw.text = TXT_DRW + 0/*Std.string(_painter.drawCount - numChildren)*/;

        // updateData
        _tmpValue = _perfValues.pop();
        _tmpValue.fps = _currentFPS;
        _tmpValue.time = (_delta > _avgTime) ? _delta - _avgTime : 0;
        _tmpValue.mem = _currMemory;
        _perfValues.unshift(_tmpValue);

        // draw graphs
        _bData.lock();

        _bData.fillRect(_drawArea, COLOR_BG);

        _pointer = Std.int(_perfValues.length - 1);

        while (_pointer >= 0) {
            _tmpValue = _perfValues[_pointer];

            // TIME
            _timeOffset = Std.int(Math.ceil(_tmpValue.time * 100) * TIME_SCALE);

            _timeOffset = Std.int(_h - _timeOffset);

            if (_timeOffset < 0) {
                _timeOffset = 0;
            }

            while (_timeOffset <= _h) {
                _bData.setPixel(_w - _pointer, _timeOffset, COLOR_MS);
                _timeOffset++;
            }

            // FPS
            _bData.setPixel(_w - _pointer, Std.int((1 - (_tmpValue.fps / _maxFPS)) * _h), COLOR_FPS);

            // MEMORY
            _bData.setPixel(_w - _pointer, Std.int(_hMem + (1 - _tmpValue.mem / _maxMemory) * _hMem), COLOR_MEM);

            _pointer--;
        }

        _bData.unlock();
        //_image.texture.dispose();
        //_image.texture.root.uploadBitmapData(_bData);

        _image.texture.dispose();
        _image.texture = Texture.fromBitmapData(_bData);
    }

    override private function get_width():Float {
        return _w * scale;
    }

    override private function get_height():Float {
        return _h * scale;
    }
}
