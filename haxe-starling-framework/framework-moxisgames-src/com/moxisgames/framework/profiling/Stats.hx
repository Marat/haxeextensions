package com.moxisgames.framework.profiling;

import flash.desktop.NativeApplication;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Stage;
import flash.filesystem.File;
import flash.geom.Rectangle;
import flash.system.System;
import starling.core.Starling;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.rendering.Painter;
import starling.text.BitmapFont;
import starling.text.TextField;
import starling.textures.Texture;
import starling.utils.Align;
import starling.utils.MathUtil;

class Stats extends Sprite
{
    
    private static inline var version : String = "V0.3";
    
    private var WIDTH : Int = 70;
    private var HEIGHT : Int = 100;
    
    private var fps : Int;
    private var ms : Int;
    private var mem : Float;
    private var memMax : Float;
    
    private var frameTime : Float = 0;
    private var frameCount : Int;
    
    private var colors : Colors = new Colors();
    
    private var fpsText : TextField;
    private var msText : TextField;
    private var memText : TextField;
    private var drwText : TextField;
    
    private var graphHeight : Float;
    private var graphWidth : Float;
    private var graphTexture : Texture;
    private var graphImage : Image;
    
    private var GRAPH_Y : Float = 50;
    
    private var graphBuffer : BitmapData;
    private var rectangle : Rectangle;
    
    private var fpsGraph : Int;
    private var memGraph : Int;
    private var memMaxGraph : Int;
    
    private var nativeStage : flash.display.Stage;
    
    private var fontSize : Float = 10;
    private var fontFamily : String = "standard 07_55";
    
    private var _painter : Painter;
    
    @:meta(Embed(source="../../../../../framework-moxisgames-assets/fonts/fps_font.png"))

    private static var StandardAtlas : Class<Dynamic>;
    
    @:meta(Embed(source="../../../../../framework-moxisgames-assets/fonts/fps_font.fnt",mimeType="application/octet-stream"))

    private static var StandardXML : Class<Dynamic>;
    
    private static inline var UPDATE_INTERVAL : Float = 0.1;
    
    public function new(w : Int = 120, h : Int = 120)
    {
        super();
        WIDTH = w;
        HEIGHT = h;
        
        alpha = 0.5;
        
        init();
    }
    
    private function init() : Void
    {
        nativeStage = Starling.current.nativeStage;
        _painter = Starling.current.painter;
        
        memMax = 0;
        
        var spacer : Float = 4;
        
        // bitmap font
        var fontBitmap : Bitmap = Type.createInstance(StandardAtlas, []);
        var fontTexture : Texture = Texture.fromBitmap(fontBitmap);
        var fontXML : FastXML = FastXML.parse(Type.createInstance(StandardXML, []));
        TextField.registerBitmapFont(new BitmapFont(fontTexture, fontXML));
        
        fpsText = new TextField(WIDTH, 14, "FPS: ?");
        fpsText.format.setTo(fontFamily, fontSize, colors.fps, Align.LEFT);
        fpsText.x = spacer;
        fpsText.y = spacer;
        
        msText = new TextField(WIDTH, 14, "MS: ?");
        msText.format.setTo(fontFamily, fontSize, colors.ms, Align.LEFT);
        msText.x = spacer;
        msText.y = fpsText.y + fpsText.height - spacer;
        
        memText = new TextField(WIDTH, 14, "MEM: ?");
        memText.format.setTo(fontFamily, fontSize, colors.mem, Align.LEFT);
        memText.x = spacer;
        memText.y = msText.y + msText.height - spacer;
        
        drwText = new TextField(WIDTH, 14, "DRW: ?");
        drwText.format.setTo(fontFamily, fontSize, colors.memmax, Align.LEFT);
        drwText.x = spacer;
        drwText.y = memText.y + memText.height - spacer;
        
        rectangle = new Rectangle(WIDTH - 1, GRAPH_Y, 1, HEIGHT - GRAPH_Y);
        graphHeight = HEIGHT - GRAPH_Y;
        graphWidth = WIDTH - 1;
        
        addEventListener(Event.ADDED_TO_STAGE, onAdded);
    }
    
    private function onAdded(event : Event) : Void
    {
        addChild(fpsText);
        addChild(msText);
        addChild(memText);
        addChild(drwText);
        
        graphBuffer = new BitmapData(WIDTH, HEIGHT, false, colors.bg);
        graphTexture = Texture.fromBitmapData(graphBuffer);
        graphImage = new Image(graphTexture);
        addChildAt(graphImage, 0);
        
        removeEventListener(Event.ADDED_TO_STAGE, onAdded);
        
        addEventListener(TouchEvent.TOUCH, onClick);
        addEventListener(Event.ENTER_FRAME, update);
        addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
    }
    
    private function onRemoved(event : Event) : Void
    {
        destroy();
    }
    
    private function onClick(event : TouchEvent) : Void
    {
        var touch : Touch = event.getTouch(this);
        
        if (touch != null)
        {
            if (touch.phase == TouchPhase.BEGAN)
            {
                fpsText.text = "Clean...";
                frameTime = frameCount = 0;
                System.gc();
            }
            else
            {
                if (touch.phase == TouchPhase.HOVER)
                {
                    alpha = 1;
                }
                else
                {
                    if (touch.phase == TouchPhase.MOVED)
                    {
                        alpha = 0.5;
                    }
                }
            }
        }
    }
    
    private function update(event : EnterFrameEvent) : Void
    {
        frameCount++;
        frameTime += event.passedTime;
        
        ms = Std.int(event.passedTime * 1000);
        msText.text = "MS: " + ms;
        
        if (frameTime > UPDATE_INTERVAL)
        {
            fps = Std.int(frameCount / frameTime);
            
            fpsText.text = "FPS: " + fps + "/" + nativeStage.frameRate;
            
            mem = as3hx.Compat.parseFloat((System.totalMemory * 0.000000954).toFixed(3));
            memMax = (memMax > mem) ? memMax : mem;
            
            memText.text = "MEM: " + Std.string(mem) + "/" + Std.string(memMax);
            drwText.text = "DRW: " + Std.string(_painter.drawCount - numChildren);
            
            fpsGraph = MathUtil.min(graphHeight, (fps / nativeStage.frameRate) * graphHeight);
            memGraph = Std.int(MathUtil.min(graphHeight, Math.sqrt(Math.sqrt(mem * 5000))) - 2);
            memMaxGraph = Std.int(MathUtil.min(graphHeight, Math.sqrt(Math.sqrt(memMax * 5000))) - 2);
            
            graphBuffer.scroll(-1, 0);
            
            graphBuffer.fillRect(rectangle, colors.bg);
            graphBuffer.setPixel(graphWidth, graphHeight - fpsGraph + GRAPH_Y, colors.fps);
            graphBuffer.setPixel(graphWidth, graphHeight - (ms >> 1) + GRAPH_Y, colors.ms);
            graphBuffer.setPixel(graphWidth, graphHeight - memGraph + GRAPH_Y, colors.mem);
            graphBuffer.setPixel(graphWidth, graphHeight - memMaxGraph + GRAPH_Y, colors.memmax);
            
            graphImage.texture.dispose();
            graphImage.texture = Texture.fromBitmapData(graphBuffer);
            
            frameTime = frameCount = 0;
        }
    }
    
    override private function get_width() : Float
    {
        return WIDTH * scaleX;
    }
    
    override private function get_height() : Float
    {
        return HEIGHT * scaleY;
    }
    
    public function destroy() : Void
    {
        removeEventListener(Event.ENTER_FRAME, update);
        removeEventListener(TouchEvent.TOUCH, onClick);
        removeEventListener(Event.ADDED_TO_STAGE, onAdded);
        removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
        
        removeChildren();
        graphBuffer.dispose();
        graphImage.dispose();
        
        nativeStage = null;
        _painter = null;
    }
}



class Colors
{
    
    public var bg : Int = 0x000033;
    public var fps : Int = 0xffff00;
    public var ms : Int = 0x00ff00;
    public var mem : Int = 0x00ffff;
    public var memmax : Int = 0xff0070;

    public function new()
    {
    }
}