package com.moxisgames.framework.profiling;

import com.moxisgames.framework.core.CoreApp;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.system.System;
import flash.text.TextField;
import flash.text.TextFormat;
import starling.core.Starling;
import starling.rendering.Painter;

/**
	 * ...
	 * @author mismagilov
	 */
class FPSCounterNative extends Sprite
{
    private var _w : Int;
    private var _h : Int;
    private var _hMem : Int;
    
    private var _currentFPS : Int;
    private var _maxFPS : Int;
    
    private var _prevTime : Float;
    private var _now : Float;
    private var _delta : Float;
    private var _avgTime : Float;
    
    private var _currMemory : Float;
    private var _maxMemory : Float = 0;
    private var _memUpdateCounter : Int = 0;
    
    private var _cacheCount : Int;
    private var _times : Array<Float>;
    
    private var _painter : Painter;
    
    private var _tmpValue : Dynamic;
    private var _perfValues : Array<Dynamic>;
    private var _pointer : Int;
    private var _timeOffset : Int;
    private var _currentCount : Int;
    
    // ui
    
    private var _bData : BitmapData;
    private var _image : Bitmap;
    
    private var _txtFps : TextField;
    private var _txtMem : TextField;
    private var _txtDrw : TextField;
    
    @:meta(Embed(source="../../../../../framework-moxisgames-assets/fonts/stan0755.ttf",fontName="standard 07_55",mimeType="application/x-font",embedAsCFF="false"))

    private var FontClass : Class<Dynamic>;
    
    //[Embed(source="fps_font.fnt", mimeType="application/octet-stream")]
    //protected static const StandardXML:Class;
    
    private inline var UPDATE_INTERVAL : Int = 30;
    private inline var TIME_SCALE : Int = 3;
    
    private inline var COLOR_BG : Int = 0x000033;
    private inline var COLOR_FPS : Int = 0xffff00;
    private inline var COLOR_MS : Int = 0x00ff00;
    private inline var COLOR_MEM : Int = 0x00ffff;
    private inline var COLOR_DRW : Int = 0xff0070;
    
    private inline var TXT_FPS : String = "FPS: ";
    private inline var TXT_MEM : String = "MEM: ";
    private inline var TXT_DRW : String = "DRW: ";
    
    private var _drawArea : Rectangle;
    
    public function new(w : Int = 120, h : Int = 60, maxFPS : Int = 60)
    {
        super();
        
        _w = w;
        _h = h;
        _hMem = Std.int(_h / 2);
        _maxFPS = maxFPS;
        _avgTime = Math.ceil((1 / _maxFPS) * 1000) / 1000;
        
        _currentFPS = 0;
        _cacheCount = 0;
        _times = [];
        
        //alpha = 0.5;
        
        init();
        
        mouseChildren = false;
    }
    
    private function init() : Void
    {
        _painter = Starling.current.painter;
        
        _drawArea = new Rectangle(0, 0, _w, _h);
        
        _bData = new BitmapData(_w, _h, false, COLOR_BG);
        _image = new Bitmap(_bData);
        
        var spacer : Float = 4;
        
        _txtFps = createTextField(COLOR_FPS);
        _txtFps.x = spacer;
        _txtFps.y = spacer;
        
        _txtMem = createTextField(COLOR_MEM);
        _txtMem.x = spacer;
        _txtMem.y = _txtFps.y + _txtFps.height - spacer;
        
        _txtDrw = createTextField(COLOR_DRW);
        _txtDrw.x = spacer;
        _txtDrw.y = _txtMem.y + _txtMem.height - spacer;
        
        _perfValues = [];
        
        for (i in 0..._w)
        {
            _perfValues[i] = {
                        fps : 0,
                        time : 0,
                        mem : 0
                    };
        }
        
        addEventListener(Event.ADDED_TO_STAGE, onAdded);
    }
    
    private function createTextField(color : Int) : TextField
    {
        var txt : TextField = new TextField();
        txt.embedFonts = true;
        txt.autoSize = "left";
        txt.defaultTextFormat = new TextFormat("standard 07_55", 8, color);
        txt.text = "0";
        txt.antiAliasType = "normal";
        return txt;
    }
    
    private function onAdded(event : Event) : Void
    {
        removeEventListener(Event.ADDED_TO_STAGE, onAdded);
        
        addChild(_image);
        addChild(_txtFps);
        addChild(_txtMem);
        addChild(_txtDrw);
        
        CoreApp.onActiveChanged.add(onActiveChanged);
        
        addEventListener(MouseEvent.CLICK, click);
        addEventListener(Event.ENTER_FRAME, update);
    }
    
    private function onActiveChanged(isActive : Bool) : Void
    {
        if (isActive)
        {
            addEventListener(Event.ENTER_FRAME, update);
        }
        else
        {
            removeEventListener(Event.ENTER_FRAME, update);
        }
    }
    
    private function click(event : MouseEvent) : Void
    {
        _memUpdateCounter = UPDATE_INTERVAL;
        
        _txtMem.text = "Clean...";
        
        System.gc();
        System.gc();
    }
    
    private function update(event : Event) : Void
    {
        // FPS
        _now = Math.round(haxe.Timer.stamp() * 1000) / 1000;
        _delta = Math.ceil((_now - _prevTime) * 1000) / 1000;
        _prevTime = _now;
        
        _times.push(_now);
        
        while (_times[0] < _now - 1)
        {
            _times.shift();
        }
        
        _currentCount = _times.length;
        _currentFPS = Math.round((_currentCount + _cacheCount) / 2);
        
        if (_currentFPS > _maxFPS)
        {
            _currentFPS = _maxFPS;
        }
        
        _cacheCount = _currentCount;
        
        _txtFps.text = TXT_FPS + _currentFPS;
        
        // memory
        if (_memUpdateCounter == 0)
        {
            _memUpdateCounter = UPDATE_INTERVAL;
            
            _currMemory = as3hx.Compat.parseFloat((System.totalMemory * 0.000000954).toFixed(3));
            
            if (_currMemory > _maxMemory)
            {
                _maxMemory = _currMemory;
            }
            
            _txtMem.text = TXT_MEM + _currMemory + "/" + _maxMemory;
        }
        else
        {
            _memUpdateCounter--;
        }
        
        // drw
        _txtDrw.text = TXT_DRW + _painter.drawCount;
        
        // updateData
        _tmpValue = _perfValues.pop();
        _tmpValue.fps = _currentFPS;
        _tmpValue.time = (_delta > _avgTime) ? _delta - _avgTime : 0;
        _tmpValue.mem = _currMemory;
        _perfValues.unshift(_tmpValue);
        
        // draw graphs
        _bData.lock();
        
        _bData.fillRect(_drawArea, COLOR_BG);
        
        _pointer = Std.int(_perfValues.length - 1);
        
        while (_pointer >= 0)
        {
            _tmpValue = _perfValues[_pointer];
            
            // TIME
            _timeOffset = Std.int(Math.ceil(_tmpValue.time * 100) * TIME_SCALE);
            
            _timeOffset = Std.int(_h - _timeOffset);
            
            while (_timeOffset <= _h)
            {
                _bData.setPixel(_w - _pointer, _timeOffset, COLOR_MS);
                _timeOffset++;
            }
            
            // FPS
            _bData.setPixel(_w - _pointer, (1 - (_tmpValue.fps / _maxFPS)) * _h ^ 0, COLOR_FPS);
            
            // MEMORY
            _bData.setPixel(_w - _pointer, _hMem + (1 - _tmpValue.mem / _maxMemory) * _hMem ^ 0, COLOR_MEM);
            
            _pointer--;
        }
        
        _bData.unlock();
    }
    
    override private function get_width() : Float
    {
        return _w * scaleX;
    }
    
    override private function get_height() : Float
    {
        return _h * scaleY;
    }
}
