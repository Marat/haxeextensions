/**
 * Created by Marat on 02.07.2016.
 */
package com.moxisgames.framework.animation;

import starling.animation.IAnimatable;
import starling.animation.Juggler;
import haxe.Constraints.Function;
import starling.animation.Transitions;

class Animation {

    static private var _juggler:Juggler;

    public function new() {
    }

    static public function setJuggler(juggler:Juggler):Void {
        _juggler = juggler;
    }

    // callbacks

    public static function delay(delaySec:Float, method:Function, args:Array<Dynamic> = null):IAnimatable {
        return _juggler.delayCall(method, delaySec, args);
    }

    public static function repeat(delaySec:Float, method:Function, count:Int = 0, args:Array<Dynamic> = null):IAnimatable {
        return _juggler.repeatCall(method, delaySec, count, args);
    }

//    public static function cancelCallback(callback:Function):Void {
//        _juggler.removeDelayedCalls(callback);
//    }

//    public static function isCallbackActive(callback:Function):Bool {
//        return _juggler.containsDelayedCalls(callback);
//    }

    public static function callStack(functions:Array<Dynamic>, delay:Float):Void {
        function callNext():Void {
            if (functions.length > 0) {
                Reflect.callMethod(null, functions.shift(), null);
            }

            if (functions.length > 0) {
                _juggler.delayCall(callNext, delay);
            }
        }

        callNext();
    }

    // tween

    public static function to(target:Dynamic, time:Float, props:Dynamic, ease:String = "easeOut"):Void {
        props.transition = ease;

        _juggler.tween(target, time, props);
    }

    public static function from(target:Dynamic, time:Float, props:Dynamic, ease:String = "easeOut"):Void {
        for (prop in Reflect.fields(props)) {
            if (!target.exists(prop)) {
                continue;
            }

            var tempVal:Dynamic = Reflect.field(target, prop);
            Reflect.setField(target, prop, Reflect.field(props, prop));
            Reflect.setField(props, prop, tempVal);
        }

        if (!props.exists("transition")) {
            props.transition = ease;
        }

        _juggler.tween(target, time, props);
    }

    public static function tweenNone(target:Dynamic, time:Float, props:Dynamic):Void {
        props.transition = Transitions.LINEAR;

        _juggler.tween(target, time, props);
    }

//    public static function poolFrom(delayBetweenAnims:Float, pool:Array<Dynamic>, globalDelay:Float = 0):Void {
//        for (i in 0...pool.length) {
//            var params:Array<Dynamic> = pool[i];
//            params[2].delay = globalDelay + delayBetweenAnims * i;
//            from.apply(null, params);
//        }
//    }

    public static function removeTween(target:Dynamic):Void {
        _juggler.removeTweens(target);
    }
}

