package com.moxisgames.framework.ui.layouts;

import starling.display.DisplayObject;

/**
	 * ...
	 * @author mismagilov
	 */
class VerticalLayout extends BaseLayout
{
    
    public static inline var DIRECTION_UP : String = "up";
    public static inline var DIRECTION_DOWN : String = "down";
    
    public function new(gapValue : Int = 0, alignValue : String = LEFT_TOP, fixedWidth : Int = 0)
    {
        super(gapValue, alignValue);
        
        _maxBound = fixedWidth;
        
        _sizeType = (_maxBound == 0) ? SIZE_DYNAMIC : SIZE_FIXED;
        
        _direction = DIRECTION_DOWN;
    }
    
    override private function alignChildren() : Void
    {
        var offset : Float = 0;
        
        var i : Int = 0;
        var l : Int = numChildren;
        while (i < l)
        {
            var currentChild : DisplayObject = getChildAt(i);
            
            offset += Std.int(_spaces[i]);
            
            if (direction == DIRECTION_DOWN)
            {
                currentChild.y = offset;
            }
            else
            {
                currentChild.y = -Std.int(offset + currentChild.height);
            }
            
            offset += Std.int(currentChild.height + gap);
            
            switch (align)
            {
                case LEFT_TOP:
                    currentChild.x = 0;
                
                case MIDDLE:
                    currentChild.x = (_maxBound - currentChild.width) >> 1;
                
                case RIGHT_BOTTOM:
                    currentChild.x = Std.int(_maxBound - currentChild.width);
            }
            i++;
        }
    }
    
    override private function checkMaxBound() : Void
    {
        if (_sizeType == SIZE_FIXED)
        {
            return;
        }
        
        if (align != LEFT_TOP)
        {
            _maxBound = 0;
            
            var i : Int = 0;
            var l : Int = numChildren;
            while (i < l)
            {
                _maxBound = max(_maxBound, getChildAt(i).width);
                i++;
            }
        }
    }
}
