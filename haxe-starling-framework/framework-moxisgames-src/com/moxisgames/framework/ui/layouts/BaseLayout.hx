package com.moxisgames.framework.ui.layouts;

import starling.display.DisplayObject;
import starling.display.Sprite;

/**
	 * ...
	 * @author mismagilov
	 */
class BaseLayout extends Sprite
{
    public var direction(get, set) : String;
    public var align(get, set) : String;
    public var gap(get, set) : Int;

    private var _spaces : Array<Dynamic>;
    
    private var _gap : Int;
    private var _direction : String;
    private var _align : String = LEFT_TOP;
    
    private var _sizeType : String;
    private var _maxBound : Int = 0;
    
    public static inline var LEFT_TOP : String = "lt";
    public static inline var MIDDLE : String = "m";
    public static inline var RIGHT_BOTTOM : String = "rb";
    
    public static inline var SIZE_FIXED : String = "fx";
    public static inline var SIZE_DYNAMIC : String = "dn";
    
    public function new(gapValue : Int, alignValue : String)
    {
        super();
        
        _gap = gapValue;
        _align = alignValue;
        
        _spaces = [];
    }
    
    public function addSpace(space : Int) : Void
    {
        _spaces[numChildren] = space;
    }
    
    public function addChilds(objects : Array<Dynamic>) : Void
    {
        var len : Int = objects.length;
        for (i in 0...len)
        {
            if (Std.is(objects[i], DisplayObject))
            {
                super.addChild(try cast(objects[i], DisplayObject) catch(e:Dynamic) null);
            }
            else
            {
                addSpace(objects[i]);
            }
        }
        
        checkMaxBound();
        
        alignChildren();
    }
    
    private function alignChildren() : Void
    {  // function for override, place your code here  
        
    }
    
    override public function addChild(child : DisplayObject) : DisplayObject
    {
        var addedChild : DisplayObject = super.addChild(child);
        
        checkMaxBound();
        
        alignChildren();
        
        return addedChild;
    }
    
    private function checkMaxBound() : Void
    {  //  
        
    }
    
    override public function removeChild(child : DisplayObject, dispose : Bool = false) : DisplayObject
    {
        var removedChild : DisplayObject = super.removeChild(child, dispose);
        
        checkMaxBound();
        
        alignChildren();
        
        return removedChild;
    }
    
    public function update() : Void
    {
        alignChildren();
    }
    
    private function get_direction() : String
    {
        return _direction;
    }
    
    private function set_direction(value : String) : String
    {
        _direction = value;
        
        alignChildren();
        return value;
    }
    
    private function get_align() : String
    {
        return _align;
    }
    
    private function set_align(value : String) : String
    {
        _align = value;
        
        alignChildren();
        return value;
    }
    
    private function get_gap() : Int
    {
        return _gap;
    }
    
    private function set_gap(value : Int) : Int
    {
        _gap = value;
        
        alignChildren();
        return value;
    }
    
    private static function max(a : Float, b : Float) : Float
    {
        return (a > b) ? a : b;
    }
}
