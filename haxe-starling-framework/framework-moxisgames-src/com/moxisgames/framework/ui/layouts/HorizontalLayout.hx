package com.moxisgames.framework.ui.layouts;

import starling.display.DisplayObject;

/**
	 * ...
	 * @author mismagilov
	 */
class HorizontalLayout extends BaseLayout
{
    
    public static inline var DIRECTION_LEFT : String = "left";
    public static inline var DIRECTION_RIGHT : String = "right";
    
    public function new(gapValue : Int = 0, alignValue : String = LEFT_TOP, fixedHeight : Int = 0)
    {
        super(gapValue, alignValue);
        
        _maxBound = fixedHeight;
        
        _sizeType = (_maxBound == 0) ? SIZE_DYNAMIC : SIZE_FIXED;
        
        _direction = DIRECTION_RIGHT;
    }
    
    override private function alignChildren() : Void
    {
        var offset : Int = 0;
        
        var i : Int = 0;
        var l : Int = numChildren;
        while (i < l)
        {
            var currentChild : DisplayObject = getChildAt(i);
            
            offset += Std.int(_spaces[i]);
            
            if (direction == DIRECTION_RIGHT)
            {
                currentChild.x = offset;
            }
            else
            {
                currentChild.x = -Std.int(offset + currentChild.width);
            }
            
            offset += Std.int(currentChild.width + gap);
            
            switch (align)
            {
                case LEFT_TOP:
                    currentChild.y = 0;
                
                case MIDDLE:
                    currentChild.y = (_maxBound - currentChild.height) >> 1;
                
                case RIGHT_BOTTOM:
                    currentChild.y = Std.int(_maxBound - currentChild.height);
            }
            i++;
        }
    }
    
    override private function checkMaxBound() : Void
    {
        if (_sizeType == SIZE_FIXED)
        {
            return;
        }
        
        if (align != LEFT_TOP)
        {
            _maxBound = 0;
            
            var i : Int = 0;
            var l : Int = numChildren;
            while (i < l)
            {
                _maxBound = max(_maxBound, getChildAt(i).height);
                i++;
            }
        }
    }
}
