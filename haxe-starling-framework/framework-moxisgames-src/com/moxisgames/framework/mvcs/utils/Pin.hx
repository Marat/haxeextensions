package com.moxisgames.framework.mvcs.utils;

class Pin {

    private static var _instances:Array<Dynamic> = new Array<Dynamic>();

    public function new() {
    }

    public static function detain(instance:Dynamic) {
        if (_instances.indexOf(instance) == -1) {
            _instances.push(instance);
        }
    }

    public static function release(instance:Dynamic) {
        if (_instances.indexOf(instance) != -1) {
            _instances.remove(instance);
        }
    }

    public static function releaseAll() {
        for (instance in _instances) {
            release(instance);
        }
    }
}

