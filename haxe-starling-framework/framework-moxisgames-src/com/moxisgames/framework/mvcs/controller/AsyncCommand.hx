package com.moxisgames.framework.mvcs.controller;

import Reflect;
import haxe.Constraints.Function;
import com.moxisgames.framework.mvcs.utils.Pin;

class AsyncCommand {

    private var _callback:Function;

    public function new() {
        Pin.detain(this);
    }

    public function execute(callback:Function) {
        _callback = callback;
    }

    public function dispatchComplete(result:Bool, callbackArgs:Array<Dynamic> = null) {
        if (_callback != null) {
            Reflect.callMethod(this, _callback, callbackArgs);
            _callback = null;
        }

        Pin.release(this);
    }
}
