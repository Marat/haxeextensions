package com.moxisgames.extensions;

import openfl.Lib;

#if android
import openfl.utils.JNI;
#end

class HaxeCamera {
	
	static private var _takePhoto:Dynamic;
	
	static private inline var ANDROID_CLASS_PATH:String = "com.moxisgames.extensions.HaxeCamera";
	
	static public function takePhoto(listener:Dynamic) 
	{
		#if android
		if (_takePhoto == null)
			_takePhoto = JNI.createStaticMethod(ANDROID_CLASS_PATH, "takePhoto", "(Lorg/haxe/lime/HaxeObject;)V");
		
		Lib.postUICallback(
			function() {
				_takePhoto(listener);
			}
		);
		#end
	}
	
}