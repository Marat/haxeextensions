package com.moxisgames.stats;

import googleAnalytics.Stats;

/**
 * ...
 * @author Ismagilov Marat
 */
class Analytics
{
	
	static private var _inited:Bool = false;

	public function new() 
	{
	}
	
	static public function init(id:String, domain:String) 
	{
		if (_inited) return;
		
		Stats.init(id, domain);
		
		_inited = true;
	}
	
	static public function event(category:String, event:String, label:String, value:Int = 0) 
	{
		if (!_inited) return;
		
		Stats.trackEvent(category, event, label, value);
	}
	
}