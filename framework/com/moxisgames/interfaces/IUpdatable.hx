package com.moxisgames.interfaces;

/**
 * @author Ismagilov Marat
 */

interface IUpdatable 
{
	function update(delta:Int):Void;
}