package com.moxisgames.interfaces;

/**
 * @author Ismagilov Marat
 */

interface IAnimation extends IUpdatable extends IDestroyable
{
	function playOnce():Void;
	function play():Void;
	function stop():Void;
	function gotoAndPlay(frame:Int):Void;
	function gotoAndStop(frame:Int):Void;
}