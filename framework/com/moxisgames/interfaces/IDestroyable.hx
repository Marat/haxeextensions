package com.moxisgames.interfaces;

/**
 * @author Ismagilov Marat
 */

interface IDestroyable 
{
	function destroy():Void;
}