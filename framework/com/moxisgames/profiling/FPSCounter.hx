package com.moxisgames.profiling;

import com.moxisgames.app.App;
import com.moxisgames.utils.PerformanceUtils;
import haxe.Timer;
import openfl.Lib;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.FPS;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.geom.Rectangle;
import openfl.system.System;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Ismagilov Marat
 */
class FPSCounter extends Sprite
{
	
	var _fps:FPS;
	var _bData:BitmapData;
	var _bitmap:Bitmap;
	
	var _perfValues:Array<PerformanceData>;
	
	var _w:Int;
	var _h:Int;
	var _hMem:Int;
	var _currFPS:Int;
	var _maxFPS:Int;
	
	var _tmpValue:PerformanceData;
	
	var _prevTime:Float;
	var _now:Float;
	var _delta:Float;
	var _avgTime:Float;
	
	var _txtMemory:TextField;
	var _currMemory:Float;
	var _maxMemory:Float = 0;
	var _memUpdateCounter:Int = 0;
	var _drawArea:Rectangle;
	var _pointer:Int;
	var _timeOffset:Int;
	var _dataToDraw:PerformanceData;
	
	static private inline var TIME_SCALE:Int = 3;
	static private inline var MAX_COUNTER:Int = 30;
	
	static private inline var COLOR_FPS:Int = 0xFFAA4048;
	static private inline var COLOR_MEMORY:Int = 0xFF1E276A;
	static private inline var COLOR_TIME:Int = 0x508A4A;
	static private inline var COLOR_BG:Int = 0xFFAACAA6;
	
	public function new(fontName:String, w:Int = 120, h:Int = 60, maxFPS:Int = 60) 
	{
		super();
		
		_w = w;
		_h = h;
		_hMem = Std.int(_h / 2);
		_maxFPS = maxFPS;
		
		_drawArea = new Rectangle(0, 0, _w, _h);
		
		_avgTime = Math.ceil((1 / _maxFPS) * 1000) / 1000;
		
		_bData = new BitmapData(_w, _h, false);
		
		_bitmap = new Bitmap(_bData, NEVER, false);
		addChild(_bitmap);
		
		_fps = new FPS(10, 5);
		_fps.defaultTextFormat = new TextFormat (fontName, 12, COLOR_FPS);
		_fps.setTextFormat(_fps.defaultTextFormat);
		_fps.autoSize = LEFT;
		_fps.textColor = COLOR_FPS;
		addChild(_fps);
		
		_prevTime = Timer.stamp ();
		
		_txtMemory = new TextField();
		_txtMemory.selectable = false;
		_txtMemory.mouseEnabled = false;
		_txtMemory.defaultTextFormat = new TextFormat(fontName, 12, COLOR_MEMORY);
		_txtMemory.text = "memory";
		_txtMemory.autoSize = LEFT;
		_txtMemory.x = _fps.x;
		_txtMemory.y = _fps.y + _fps.height - 5;
		addChild(_txtMemory);
		
		_perfValues = new Array<PerformanceData>();
		
		for (i in 0..._w) 
			_perfValues[i] = { fps:0, time:0, mem:0 };
		
		mouseChildren = false;
		
		buttonMode = true;
		
		addEventListener(MouseEvent.CLICK, click);
		
		if (App.isAppActive())
			capture(true);
		
		App.onAppActivated.add(onAppActivateChanged);
	}
	
	function onAppActivateChanged(active:Bool) 
	{
		if (active)
			capture(true);
		else
			capture(false);
	}
	
	public function capture(value:Bool) 
	{
		if (value)
			Lib.current.stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		else
			Lib.current.stage.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
	}
	
	private function onEnterFrame(e:Event):Void 
	{
		// calculate
		_now = Timer.stamp();
		_delta = Math.ceil((_now - _prevTime) * 1000) / 1000;
		_prevTime = _now;
		
		_currFPS = _fps.currentFPS > _maxFPS ? _maxFPS : _fps.currentFPS;
		
		if (_memUpdateCounter == 0)
		{
			_memUpdateCounter = MAX_COUNTER;
			
			_currMemory = getMb();
			_maxMemory = Math.max(_currMemory, _maxMemory);
			
			_txtMemory.text = "m:" + _currMemory + "/" + _maxMemory;
		}
		else
		{
			_memUpdateCounter--;
		}
		
		// updateData
		_tmpValue = _perfValues.pop();
		_tmpValue.fps = _currFPS;
		_tmpValue.time = _delta > _avgTime ? _delta - _avgTime : 0;
		_tmpValue.mem = _currMemory;
		_perfValues.unshift(_tmpValue);
		
		// draw
		//_txtDelta.text = Std.string(delta);
		
		_bData.lock();
		
		_bData.fillRect(_drawArea, COLOR_BG);
		
		_pointer = _perfValues.length - 1;
		while (_pointer >= 0) 
		{
			_dataToDraw = _perfValues[_pointer];
			
			// TIME
			_timeOffset = Math.ceil(_dataToDraw.time * 100) * TIME_SCALE;
			
			_timeOffset = _h - _timeOffset;
			
			if (_timeOffset < 0) _timeOffset = 0;
			
			while (_timeOffset <= _h)
			{
				_bData.setPixel(_w - _pointer, _timeOffset, COLOR_TIME);
				_timeOffset++;
			}
			
			// FPS
			_bData.setPixel(_w - _pointer, Math.floor((1 - (_dataToDraw.fps / _maxFPS)) * _h), COLOR_FPS);
			
			// MEMORY
			_bData.setPixel(_w - _pointer, Math.floor(_hMem + (1 - _dataToDraw.mem / _maxMemory) * _hMem), COLOR_MEMORY);
			
			_pointer--;
		}
		
		_bData.unlock();
	}
	
	private function click(e:MouseEvent):Void 
	{
		trace("GC runGC = " + getMb());
		
		PerformanceUtils.gc();
		
		_memUpdateCounter = Math.ceil(MAX_COUNTER / 2);
		
		_txtMemory.text = "Clean...";
	}
	
	public static inline function getMb() : Float {
        return Math.ceil(((System.totalMemory / 1024) / 1000) * 100) / 100;
    }
	
	public function setScale(scale:Float) 
	{
		scaleX = scaleY = scale;
	}
	
}

typedef PerformanceData = { time:Float, fps:Float, mem:Float };