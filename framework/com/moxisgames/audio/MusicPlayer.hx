package com.moxisgames.audio;

import com.moxisgames.log.Logger;
import motion.Actuate;
import openfl.Assets;
import openfl.media.Sound;
import openfl.media.SoundChannel;
import openfl.media.SoundTransform;

/**
 * ...
 * @author Ismagilov Marat
 */
class MusicPlayer
{

	public var volume:Float = 0.5;
	
	private var _currentSoundId:String;
	private var _currentSound:Sound;
	
	private var _activeChannel:SoundChannel;
	private var _position:Float = 0;
	
	private var _transformMute:SoundTransform;
	
	@:isVar public var enabled(get, set):Bool = true;
	
	static public inline var GAME:String = "theme_game";
	static public inline var MENU:String = "theme_menu";
	
	#if web
	static public inline var FILE_EXT:String = ".mp3";
	#else
	static public inline var FILE_EXT:String = ".ogg";
	#end
	
	static private var _instance:MusicPlayer;
	
	public function new() 
	{
		_transformMute = new SoundTransform(0);
	}
	
	public function play():Void
	{
		if (!enabled) return;
		
		if (_activeChannel == null && _currentSound != null)
		{
			_activeChannel = _currentSound.play(_position, 99999, _transformMute);
			
			if (_activeChannel != null)
				Actuate.transform(_activeChannel, 1).sound(volume);
		}
	}
	
	public function stop():Void 
	{
		if (_activeChannel != null)
		{
			_position = _activeChannel.position;
			_activeChannel.stop();
			_activeChannel = null;
		}
	}
	
	public function switchSound(newSoundId:String) 
	{
		newSoundId += FILE_EXT;
		
		if (_currentSoundId == newSoundId) return;
		
		_currentSoundId = newSoundId;
		
		if (_activeChannel != null)
		{
			Actuate.transform(_activeChannel, 1).sound(0).onComplete(
				function()
				{
					Assets.loadMusic(_currentSoundId, false).onComplete(onMusicLoaded);
				}
			);
		}
		else
		{
			Assets.loadMusic(_currentSoundId, false).onComplete(onMusicLoaded);
		}
	}
	
	function onMusicLoaded(sound:Sound) 
	{
		if (sound == null) return;
		
		_currentSound = sound;
		
		_position = 0;
		_activeChannel = null;
		
		play();
	}
	
	static public function getInstance():MusicPlayer 
	{
		if (_instance == null)
			_instance = new MusicPlayer();
		
		return _instance;
	}
	
	function get_enabled():Bool 
	{
		return enabled;
	}
	
	function set_enabled(value:Bool):Bool 
	{
		if (enabled == value) return enabled;
		
		enabled = value;
		
		if (enabled)
			play();
		else
			stop();
		
		return enabled;
	}
	
}