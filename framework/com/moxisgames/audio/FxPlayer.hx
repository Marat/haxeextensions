package com.moxisgames.audio;

import flash.media.Sound;
import openfl.Assets;
import openfl.media.SoundChannel;
import openfl.media.SoundTransform;
import openfl.utils.IAssetCache;

/**
 * ...
 * @author Ismagilov Marat
 */
class FxPlayer
{
	public var volume(default, set):Float = 0.5;
	public var enabled:Bool = true;
	
	var _fxChannel:SoundChannel;
	var _transform:SoundTransform;
	var _cache:IAssetCache;
	
	static private var _instance:FxPlayer;
	
	#if web
	static public inline var FILE_EXT:String = ".mp3";
	#else
	static public inline var FILE_EXT:String = ".ogg";
	#end
	
	public function new() 
	{
		_transform = new SoundTransform(0.5);
		_cache = Assets.cache;
	}
	
	function set_volume(value:Float):Float 
	{
		_transform.volume = value;
		return volume = value;
	}
	
	public function playFx(fxID:String, loops:Int = 0):Void 
	{
		if (!enabled) return;
		
		fxID += FILE_EXT;
		
		if (_cache.hasSound(fxID))
		{
			Assets.getSound(fxID).play(0, 0, _transform);
		}
		else
		{
			Assets.loadSound(fxID).onComplete(
				function onSoundLoaded(sound:Sound) 
				{
					_cache.setSound(fxID, sound);
					_fxChannel = sound.play(0, loops, _transform);
				}
			);
		}
	}
	
	static public function getInstance():FxPlayer 
	{
		if (_instance == null)
			_instance = new FxPlayer();
		
		return _instance;
	}
	
	static public function play(fxID:String, loops:Int = 0) 
	{
		_instance.playFx(fxID, loops);
	}
	
}