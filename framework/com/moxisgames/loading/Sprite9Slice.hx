package com.moxisgames.loading;

import com.moxisgames.interfaces.IDestroyable;
import flash.display.BitmapData;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.geom.Matrix;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Marat Ismagilov
 */
class Sprite9Slice extends Sprite implements IDestroyable
{

	var _parts:Array<Bitmap>;
	
	public function new(bitmapData:BitmapData, rect:Rectangle) 
	{
		super();
		
		var rows:Array<Int> = [
			0, 
			Math.floor(rect.top), 
			Math.floor(rect.bottom), 
			Math.floor(bitmapData.height)
		];
		var cols:Array<Int> = [
			0, 
			Math.floor(rect.left), 
			Math.floor(rect.right), 
			Math.floor(bitmapData.width)
		];
		
		var mat:Matrix = new Matrix();
		var counter = 0;
		var bData:BitmapData;
		var bitmap:Bitmap;
		
		_parts = [];
		
		for (cx in 0...3)
		{
			for (cy in 0...3)
			{
				mat.identity();
				mat.translate( -cols[cx], -rows[cy]);
				
				bData = new BitmapData(Std.int(cols[cx + 1] - cols[cx]), Std.int(rows[cy + 1] - cols[cy]));
				bData.draw(bitmapData, mat, null, null, null, true);
				
				bitmap = new Bitmap(bData);
				addChild(bitmap);
				
				_parts[counter] = bitmap;
				counter++;
			}
		}
		
		setSize(200, 200);
	}
	
	// 0 3 6
	// 1 4 7
	// 2 5 8
	public function setSize(w:Int, h:Int) 
	{
		setCoord(_parts[0], 0, 0);
		setCoord(_parts[2], 0, h - _parts[2].height);
		
		setCoord(_parts[6], w - _parts[6].width, 0);
		setCoord(_parts[8], w - _parts[8].width, h - _parts[8].height);
		
		setCoord(_parts[1], 0, _parts[0].height - 1, -1, h - _parts[0].height - _parts[2].height + 2);
		setCoord(_parts[7], w - _parts[7].width, _parts[6].height - 1, -1, h - _parts[6].height - _parts[8].height + 2);
		
		setCoord(_parts[3], _parts[0].width - 1, 0, w - _parts[0].width - _parts[6].width + 2);
		setCoord(_parts[5], _parts[0].width - 1, h - _parts[5].height, w - _parts[2].width - _parts[8].width + 2);
		
		setCoord(_parts[4], 
			_parts[0].width - 1, 
			_parts[0].height - 1, 
			w - _parts[0].width - _parts[6].width + 2,
			h - _parts[0].height - _parts[2].height + 2);
	}
	
	function setCoord(bitmap:Bitmap, xV:Float, yV:Float, w:Float = -1, h:Float = -1) 
	{
		bitmap.x = Std.int(xV);
		bitmap.y = Std.int(yV);
		
		if (w != -1)
			bitmap.width = Std.int(w);
		
		if (h != -1)
			bitmap.height = Std.int(h);
	}
	
	public function destroy():Void 
	{
		removeChildren();
		
		for (bitmap in _parts)
			bitmap.bitmapData.dispose();
		
		_parts.splice(0, _parts.length);
		_parts = null;
	}
	
}