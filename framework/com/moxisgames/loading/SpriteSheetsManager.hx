package com.moxisgames.loading;

import com.moxisgames.log.Logger;
import com.moxisgames.ui.image.ImageResizer;
import com.moxisgames.utils.PerformanceUtils;
import com.moxisgames.utils.StringUtils;
import haxe.Json;
import haxe.ds.StringMap;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.DisplayObjectContainer;
import openfl.geom.Matrix;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Marat Ismagilov
 */
class SpriteSheetsManager
{

	var _scaleAssetCurrent:Float;
	var _scaleAssetPack:Float;
	var _needResize:Bool;
	
	var _pathToImages:String;
	
	var _textures:StringMap<TextureSheet>;
	
	var _matrix:Matrix = new Matrix();
	
	var _inited:Bool = false;
	var _loadTasks:Array<SpriteSheetsLoadingTask>;
	var _activeTask:SpriteSheetsLoadingTask;
	
	//var _sRect:Rectangle = new Rectangle();
	//var _dPoint:Point = new Point();
	
	//static private var _allowInstatiate:Bool = false;
	//static private var _instance:SpriteSheetsManager;
	
	public function new() 
	{
		//if (!_allowInstatiate) throw "[SpriteSheetsManager] use singletone!";
		
		_textures = new StringMap<TextureSheet>();
	}
	
	public function init(currentScale:Float, supportScales:Array<Float>, pathToImages:String = "img/") 
	{
		_scaleAssetPack = getNearScale(currentScale, supportScales);
		_scaleAssetCurrent = currentScale / _scaleAssetPack;
		_needResize = _scaleAssetCurrent != 1;
		_pathToImages = pathToImages;
		
		_loadTasks = [];
		
		_inited = true;
		
		Logger.log("SpriteSheetsManager init. Game scale x" + currentScale + ". Use pack x" + _scaleAssetPack);
	}
	
	public function loadTextures(names:Array<String>, completeCallback:Void->Void) 
	{
		if (!_inited) throw "[SpriteSheetsManager] you should init() first";
		
		var task = new SpriteSheetsLoadingTask(names, completeCallback);
		
		if (_activeTask == null)
		{
			startNewTask(task);
		}
		else
		{
			_loadTasks.push(task);
		}
	}
	
	function startNewTask(task:SpriteSheetsLoadingTask)
	{
		PerformanceUtils.startMeasure("[SpriteSheetsManager] load textures '" + task.name + "'");
		
		_activeTask = task;
		processTask();
	}
	
	function processTask() 
	{
		if (_activeTask != null)
		{
			if (_activeTask.isCompleted())
			{
				PerformanceUtils.endMeasure("[SpriteSheetsManager] load textures '" + _activeTask.name + "'");
				
				_activeTask.complete();
				
				if (_loadTasks.indexOf(_activeTask) != -1)
					_loadTasks.remove(_activeTask);
				
				_activeTask = null;
				
				if (_loadTasks.length > 0)
					startNewTask(_loadTasks.shift());
			}
			else
			{
				var textureName = _activeTask.getNextPath();
				
				// такая текстуру уже загружена
				if (hasTexture(textureName)) 
				{
					processTask();
					return;
				}
				
				var jsonPath = _pathToImages + "x" + _scaleAssetPack + "/" + textureName + ".json";
				var pngPath  = _pathToImages + "x" + _scaleAssetPack + "/" + textureName + ".png";
				
				Assets.loadBitmapData(pngPath, false)
					.onComplete(
						function(bitmapData:BitmapData) 
						{ 
							Assets.loadText(jsonPath).onComplete(
								function(jsonText:String) 
								{
									onBitmapLoaded(textureName, bitmapData, jsonText); 
								}
							);
						} 
					)
					.onError(onBitmapError);
			}
		}
	}
	
	function onBitmapLoaded(textureName:String, atlas:BitmapData, jsonText:String):Void 
	{
		var json = Json.parse(jsonText);
		
		var texture = new TextureSheet();
		
		var frames:Array<Dynamic> = json.frames;
		
		for (frame in frames)
		{
			var bitmapData = new BitmapData(frame.frame.w, frame.frame.h, true, 0x00000000);
			
			_matrix.tx = -frame.frame.x;
			_matrix.ty = -frame.frame.y;
			bitmapData.draw(atlas, _matrix);
			
			texture.setBitmapData(frame.filename, 
				_needResize ? ImageResizer.scaleBitmapData(bitmapData, _scaleAssetCurrent) : bitmapData);
			
			if (_needResize)
				bitmapData.dispose();
		}
		
		atlas.dispose();
		
		_textures.set(textureName, texture);
		
		Logger.log("+ Texture '" + textureName + "' ready");
		
		processTask();
	}
	
	public function hasTexture(textureName:String):Bool 
	{
		return _textures.exists(textureName);
	}
	
	public function unloadTextures(names:Array<String>) 
	{
		for (textureName in names)
		{
			if (_textures.exists(textureName))
			{
				_textures.get(textureName).destroy();
				_textures.remove(textureName);
				
				Logger.log("- Texture '" + textureName + "' was disposed");
			}
		}
	}
	
	public function getTexture(textureName:String):TextureSheet
	{
		return _textures.get(textureName);
	}
	
	function onBitmapError(error:Dynamic):Void 
	{
		Logger.error("onBitmapError " + error);
		
		processTask();
	}
	
	//
	function getNearScale(value:Float, supportScales:Array<Float>):Float
	{
		supportScales.sort(
			function(a:Float, b:Float):Int
			{
				if (a > b) return 1;
				else if (a < b) return -1;
				
				return 0;
			}
		);
		
		for (targScale in supportScales) 
		{
			if (targScale >= value)
				return targScale;
		}
		
		return supportScales[supportScales.length - 1];
	}
	
	// SUGAR
	
	public function getBitmapDataFrom(textureName:String, bitmapDataID:String):BitmapData 
	{
		return getTexture(textureName).getBitmapData(bitmapDataID);
	}
	
	public function createBitmap(textureName:String, bitmapDataID:String, ?parent:DisplayObjectContainer, ?smoothing:Bool = false):Bitmap
	{
		if (!hasTexture(textureName)) 
		{
			Logger.warning("Texture '" + textureName + "' not exists. Can't create '" + bitmapDataID + "' bitmap.");
			return new Bitmap();
		}
		
		var bmp = new Bitmap(getBitmapDataFrom(textureName, bitmapDataID), AUTO, smoothing);
		
		if (parent != null)
			parent.addChild(bmp);
		
		return bmp;
	}
	
	public function create9Slice(textureName:String, bitmapDataID:String, rect:Rectangle):Sprite9Slice
	{
		var sprite = new Sprite9Slice(getBitmapDataFrom(textureName, bitmapDataID), rect);
		return sprite;
	}
	
	public function createAnimation(textureName:String, animID:String, frameRate:Int = 30, smooth:Bool = false):TextureAnimationCopyPixels
	{
		var texture:TextureSheet = getTexture(textureName);
		
		var counter:Int = 1;
		var framesBitmaps:Array<BitmapData> = [];
		var animName:String = animID + StringUtils.leadZeros(counter, 4);
		
		while (texture.hasBitmapData(animName)) 
		{
			framesBitmaps.push(texture.getBitmapData(animName));
			
			counter++;
			animName = animID + StringUtils.leadZeros(counter, 4);
		}
		
		var anim = new TextureAnimationCopyPixels(framesBitmaps, frameRate, smooth);
		return anim;
	}
	
	//
	
	/*static public function getInstance():SpriteSheetsManager 
	{
		if (_instance == null)
		{
			_allowInstatiate = true;
			_instance = new SpriteSheetsManager();
			_allowInstatiate = false;
		}
		
		return _instance;
	}*/
	
}