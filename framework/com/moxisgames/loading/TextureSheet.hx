package com.moxisgames.loading;

import com.moxisgames.interfaces.IDestroyable;
import com.moxisgames.log.Logger;
import com.moxisgames.utils.CreateUtils;
import haxe.ds.StringMap;
import openfl.display.BitmapData;

/**
 * ...
 * @author Marat Ismagilov
 */
class TextureSheet implements IDestroyable
{

	var _bitmaps:StringMap<BitmapData>;
	
	public function new() 
	{
		_bitmaps = new StringMap<BitmapData>();
	}
	
	public function setBitmapData(id:String, bData:BitmapData) 
	{
		_bitmaps.set(id, bData);
	}
	
	public function hasBitmapData(id:String):Bool 
	{
		return _bitmaps.exists(id);
	}
	
	public function getBitmapData(id:String):BitmapData 
	{
		if (_bitmaps.exists(id))
			return _bitmaps.get(id);
		else
			Logger.warning("Bitmap with id=" + id + " was not found in sheet");
			
		return null;
	}
	
	/* INTERFACE com.moxisgames.interfaces.IDestroyable */
	
	public function destroy():Void 
	{
		for (bData in _bitmaps.iterator())
			bData.dispose();
		
		CreateUtils.clearMap(_bitmaps);
		_bitmaps = null;
	}
	
}