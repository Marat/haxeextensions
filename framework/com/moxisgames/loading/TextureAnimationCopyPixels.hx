package com.moxisgames.loading;

import com.moxisgames.interfaces.IAnimation;
import com.moxisgames.loop.Juggler;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.PixelSnapping;
import openfl.events.Event;

/**
 * ...
 * @author Marat Ismagilov
 */
class TextureAnimationCopyPixels extends Bitmap implements IAnimation
{
	
	public var data:Dynamic;
	
	var _framesBitmaps:Array<BitmapData>;
	
	var _frameTime:Int;
	var _passTime:Int = 0;
	
	var _playing:Bool = false;
	var _once:Bool = false;
	var _autoDestroy:Bool = false;
	var _currentFrame:Int = 1;
	var _smooth:Bool;
	
	@:isVar public var totalFrames(default, null):Int = 1;
	
	public function new(framesBitmaps:Array<BitmapData>, frameRate:Int, smooth:Bool = false) 
	{
		super();
		
		_smooth = smooth;
		
		_framesBitmaps = framesBitmaps;
		_frameTime = Std.int((1 / frameRate) * 1000);
		
		totalFrames = _framesBitmaps.length;
		
		changeFrame();
	}
	
	private function changeFrame():Void 
	{
		this.bitmapData = _framesBitmaps[_currentFrame - 1];
		
		if (_smooth) smoothing = true;
		
		if (_currentFrame < totalFrames)
		{
			_currentFrame++;
		}
		else
		{
			if (_once)
			{
				stop();
				
				_once = false;
				
				if (_autoDestroy) destroy();
			}
			else
			{
				_currentFrame = 1;
			}
			
			dispatchEvent(new Event(Event.COMPLETE));
		}
	}
	
	public function playOnceWithDestroy():Void 
	{
		_autoDestroy = true;
		
		playOnce();
	}
	
	/* INTERFACE com.moxisgames.interfaces.IAnimation */
	
	public function playOnce():Void 
	{
		_once = true;
		
		gotoAndPlay(1);
	}
	
	public function play():Void 
	{
		if (!_playing)
		{
			_playing = true;
			Juggler.addToLoop(this);
		}
	}
	
	public function stop():Void 
	{
		if (_playing)
		{
			Juggler.removeFromLoop(this);
			_playing = false;
		}
	}
	
	public function gotoAndPlay(frame:Int):Void 
	{
		_currentFrame = checkMaxFrame(frame, totalFrames);
		
		changeFrame();
		
		play();
	}
	
	public function gotoAndStop(frame:Int):Void 
	{
		stop();
		
		_currentFrame = checkMaxFrame(frame, totalFrames);
		
		changeFrame();
	}
	
	public function update(delta:Int):Void
	{
		_passTime += delta;
		
		if (_passTime >= _frameTime)
			changeFrame();
			
		while (_passTime >= _frameTime)
			_passTime -= _frameTime;
	}
	
	static inline function checkMaxFrame(frame:Int, total:Int):Int 
	{
		if (frame < 0) frame = 0;
		else if (frame > total) frame = total;
		
		return frame;
	}
	
	public function destroy():Void 
	{
		stop();
		
		this.bitmapData = null;
		
		_framesBitmaps.splice(0, _framesBitmaps.length);
		_framesBitmaps = null;
		
		data = null;
		
		if (parent != null)
			parent.removeChild(this);
	}
	
}