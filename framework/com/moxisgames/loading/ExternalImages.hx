package com.moxisgames.loading;

import haxe.ds.StringMap;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Loader;
import openfl.display.LoaderInfo;
import openfl.events.Event;
import openfl.net.URLRequest;
import openfl.system.ApplicationDomain;
import openfl.system.Capabilities;
import openfl.system.LoaderContext;
import openfl.system.SecurityDomain;

/**
 * ...
 * @author Ismagilov Marat
 */
class ExternalImages
{

	static private var _hash:StringMap<BitmapData> = new StringMap<BitmapData>();
	static private var _callbacks:StringMap<Array<BitmapData->Void>> = new StringMap<Array<BitmapData->Void>>();
	
	public function new() 
	{
	}
	
	static public function getBitmapData(url:String, onBitmapReceived:BitmapData->Void, saveToCache:Bool = true) 
	{
		if (_hash.exists(url))
		{
			onBitmapReceived(_hash.get(url));
		}
		else if (_callbacks.exists(url))
		{
			var arr:Array<BitmapData->Void> = _callbacks.get(url);
			arr.push(onBitmapReceived);
		}
		else
		{
			var arr:Array<BitmapData->Void> = [onBitmapReceived];
			_callbacks.set(url, arr);
			
			function onLoaderComplete(e:Event):Void 
			{
				cast(e.currentTarget, LoaderInfo).removeEventListener(Event.COMPLETE, onLoaderComplete);
				
				if (Std.is(cast(e.currentTarget, LoaderInfo).content, Bitmap))
				{
					var bData = cast((cast(e.currentTarget, LoaderInfo)).content, Bitmap).bitmapData;
					
					if (saveToCache)
						_hash.set(url, bData);
					
					var arr:Array<BitmapData->Void> = _callbacks.get(url);
					
					for (cb in arr)
						cb(bData);
						
					_callbacks.remove(url);
				}
			}
			
			var loadingContext:LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain, SecurityDomain.currentDomain);
			
			#if flash
			if (Capabilities.playerType == "Desktop")
				loadingContext = null;
			#end
			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderComplete);
			loader.load(new URLRequest(url), loadingContext);
		}
	}
	
}