package com.moxisgames.loading;

/**
 * ...
 * @author Marat Ismagilov
 */
class SpriteSheetsLoadingTask
{
	var _pathsToLoad:Array<String>;
	var _completeCallback:Void->Void;
	
	public var name(default, null):String;
	
	public function new(pathsToLoad:Array<String>, completeCallback:Void->Void) 
	{
		_pathsToLoad = pathsToLoad.copy();
		_completeCallback = completeCallback;
		
		name = pathsToLoad.join(",");
	}
	
	public function isCompleted():Bool 
	{
		return _pathsToLoad.length == 0;
	}
	
	public function getNextPath():String 
	{
		if (_pathsToLoad.length > 0)
			return _pathsToLoad.shift();
			
		return "";
	}
	
	public function complete() 
	{
		_completeCallback();
		_completeCallback = null;
		_pathsToLoad = null;
		name = null;
	}
	
}