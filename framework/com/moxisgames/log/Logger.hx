package com.moxisgames.log;

import openfl.Lib;

/**
 * ...
 * @author Ismagilov Marat
 */
class Logger
{
	
	#if html5
	static private var _startTime:Float;
	#end
	
	static private var _appPrefix:String = "";
	
	static public var debug:Bool = false;
	
	public function new() 
	{
	}
	
	static public function setAppPrefix(appPrefix:String) 
	{
		_appPrefix = appPrefix;
		
		#if html5
		_startTime = Lib.getTimer();
		#end
	}
	
	static public function log(str:String):Void
	{
		if (debug)
		{
			Lib.trace(_appPrefix + " " + getTime() + ": " + str);
		}
	}
	
	static public function warning(str:String) 
	{
		if (debug)
		{	
			Lib.trace("Warning: " + _appPrefix + " " + getTime() + ": " + str);
		}
	}
	
	static public function error(str:String) 
	{
		if (debug)
		{	
			Lib.trace("Error #LOG " + _appPrefix + " " + getTime() + ": " + str);
		}
	}
	
	static private inline function getTime():Float 
	{
		#if html5
		return Math.floor((Lib.getTimer() - _startTime) / 10) / 100;
		#else
		return Math.floor(Lib.getTimer() / 10) / 100;
		#end
	}
	
}