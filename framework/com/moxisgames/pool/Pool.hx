package com.moxisgames.pool;
import com.moxisgames.interfaces.IDestroyable;

/**
 * A generic container that facilitates pooling and recycling of objects.
 * WARNING: Pooled objects must have parameterless constructors: function new()
 */
@:generic
class Pool<T>
{
	private var _pool:Array<T>;
	private var _class:Class<T>;
	private var _args:Array<Dynamic>;
	
	public var length(get, never):Int;
	
	public function new(classObj:Class<T>, preAllocateCount:Int = 0, args:Array<Dynamic> = null) 
	{
		_pool = [];
		_class = classObj;
		_args = (args == null ? [] : args);
		
		if (preAllocateCount > 0)
			preAllocate(preAllocateCount);
	}
	
	public function get():T
	{
		//trace("get ");
		
		var obj:T = _pool.pop();
		if (obj == null) 
		{
			obj = Type.createInstance(_class, _args);
			//trace("Pool create new INST");
		}
		else
		{
			//trace("Pool USE existing INST");
		}
		
		return obj;
	}
	
	public function put(obj:T):Void
	{
		//trace("put");
		// we don't want to have the same object in pool twice
		if (obj != null && _pool.indexOf(obj) < 0)
		{
			_pool.push(obj);
		}
	}
	
	public function preAllocate(numObjects:Int):Void
	{
		for (i in 0...numObjects)
		{
			_pool.push(Type.createInstance(_class, _args));
		}
	}
	
	public function clear(destroy:Bool = true):Void
	{
		if (destroy 
			&& _pool.length > 0 
			&& Std.is(_pool[0], IDestroyable))
		{
			for (item in _pool)
				cast(item, IDestroyable).destroy();
		}
		
		_pool.splice(0, _pool.length);
	}
	
	private inline function get_length():Int
	{
		return _pool.length;
	}
}