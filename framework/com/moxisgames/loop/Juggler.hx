package com.moxisgames.loop;

import com.moxisgames.app.App;
import com.moxisgames.interfaces.IUpdatable;
import openfl.Lib;
import openfl.events.Event;

/**
 * ...
 * @author Ismagilov Marat
 */
class Juggler
{

	static private var _active:Bool = false;
	static private var _objects:Array<IUpdatable> = [];
	static private var _time:Int;
	
	public function new() 
	{
	}
	
	static public function addToLoop(anim:IUpdatable) 
	{
		if (_objects.indexOf(anim) == -1)
		{
			_objects.push(anim);
			
			if (!_active)
			{
				App.onAppActivated.add(onAppActivateChanged);
				
				_active = true;
				_time = Lib.getTimer();
				
				if (App.isAppActive())
					Lib.current.stage.addEventListener(Event.ENTER_FRAME, update);
			}
		}
	}
	
	static private function onAppActivateChanged(active:Bool) 
	{
		if (active)
			Lib.current.stage.addEventListener(Event.ENTER_FRAME, update);
		else
			Lib.current.stage.removeEventListener(Event.ENTER_FRAME, update);
	}
	
	static public function removeFromLoop(anim:IUpdatable) 
	{
		if (_objects.indexOf(anim) != -1)
		{
			_objects.remove(anim);
		
			if (_objects.length == 0)
			{
				App.onAppActivated.remove(onAppActivateChanged);
				
				Lib.current.stage.removeEventListener(Event.ENTER_FRAME, update);
				_active = false;
			}
		}
	}
	
	static function update(_) 
	{
		var now:Int = Lib.getTimer();
		var delta:Int = now - _time;
		_time = now;
		
		for (anim in _objects) 
		{
			anim.update(delta);
		}
	}
	
}