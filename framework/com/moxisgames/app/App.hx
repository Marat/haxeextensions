package com.moxisgames.app;

import motion.Actuate;
import msignal.Signal.Signal1;
import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.display.StageAlign;
import openfl.display.StageScaleMode;
import openfl.events.Event;
import openfl.Lib;
import openfl.events.MouseEvent;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Ismagilov Marat
 */
class App extends Sprite
{
	
	private var _inited:Bool = false;
	
	static private var _appIsActive:Bool = true;
	
	static private var _orientation:String = AUTO;
	
	@:isVar static public var scaleContentX(default, null):Float = 1;
	@:isVar static public var scaleContentY(default, null):Float = 1;
	@:isVar static public var scale(default, null):Float = 1;
	
	@:isVar static public var currentStageWidth(default, null):Int;
	@:isVar static public var currentStageHeight(default, null):Int;
	
	@:isVar static public var isPortraitOrientation(default, null):Bool = true;

	@:isVar static public var onOrientationChanged(default, never):Signal1<Bool> = new Signal1<Bool>();
	@:isVar static public var onAppActivated(default, never):Signal1<Bool> = new Signal1<Bool>();
	
	@:isVar static public var defaultScreenSizeMax(default, null):Int = 640;
	@:isVar static public var defaultScreenSizeMin(default, null):Int = 480;
	
	static public inline var DEFAULT_FPS:Float = 60;
	
	static public inline var HORIZONTAL:String = "horizontal";
	static public inline var VERTICAL:String = "vertical";
	static public inline var AUTO:String = "auto";
	
	public function new(screenW:Int = 640, screenH:Int = 480, orientation:String = AUTO) 
	{
		super();
		
		_orientation = orientation;
		
		Lib.current.stage.align = StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
		
		defaultScreenSizeMax = screenW > screenH ? screenW : screenH;
		defaultScreenSizeMin = screenW < screenH ? screenW : screenH;
		
		addEventListener(Event.ADDED_TO_STAGE, added);
	}
	
	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		
		stage.addEventListener(Event.RESIZE, resize);
		stage.addEventListener(Event.ACTIVATE, onActivate);
		stage.addEventListener(Event.DEACTIVATE, onDeactivate);
		
		resize();
	}
	
	/* ENTRY POINT */
	
	function resize(e = null):Void 
	{
		trace("[App] resize " + Lib.current.stage.stageWidth + "x" + Lib.current.stage.stageHeight);
		
		if (!_inited && !isStageSizeCorrect()) return;
		
		if (!_inited) init();
		
		if (isPortraitOrientation && Lib.current.stage.stageWidth >= Lib.current.stage.stageHeight)
		{
			isPortraitOrientation = false;
			
			currentStageWidth = Lib.current.stage.stageWidth;
			currentStageHeight = Lib.current.stage.stageHeight;
			
			orientationChanged(isPortraitOrientation);
			
			onOrientationChanged.dispatch(isPortraitOrientation);
		}
		else if (!isPortraitOrientation && Lib.current.stage.stageWidth <= Lib.current.stage.stageHeight)
		{
			isPortraitOrientation = true;
			
			currentStageWidth = Lib.current.stage.stageWidth;
			currentStageHeight = Lib.current.stage.stageHeight;
			
			orientationChanged(isPortraitOrientation);
			
			onOrientationChanged.dispatch(isPortraitOrientation);
		}
	}
	
	function isStageSizeCorrect():Bool 
	{
		if (_orientation == VERTICAL)
			return Lib.current.stage.stageWidth <= Lib.current.stage.stageHeight;
		else if (_orientation == HORIZONTAL)
			return Lib.current.stage.stageWidth >= Lib.current.stage.stageHeight;
			
		return true;
	}
	
	function onActivate(e:Event):Void 
	{
		trace("[App] onActivate _appIsActive=" + _appIsActive);
		
		if (!_appIsActive)
		{
			_appIsActive = true;
			
			Lib.current.stage.frameRate = DEFAULT_FPS;
			
			activeChanged(true);
			
			onAppActivated.dispatch(true);
			
			Actuate.resumeAll();
			
			Lib.current.stage.removeEventListener(MouseEvent.MOUSE_DOWN, onClickWhenDeactivated);
		}
	}
	
	function onDeactivate(e:Event):Void 
	{
		trace("[App] onDeactivate _appIsActive=" + _appIsActive);
		
		if (_appIsActive)
		{
			_appIsActive = false;
			
			#if !flash
			Lib.current.stage.frameRate = 1;
			#end
			
			activeChanged(false);
			
			onAppActivated.dispatch(false);
			
			Actuate.pauseAll();
			
			Lib.current.stage.addEventListener(MouseEvent.MOUSE_DOWN, onClickWhenDeactivated);
		}
	}
	
	private function onClickWhenDeactivated(e:MouseEvent):Void 
	{
		onActivate(null);
	}
	
	function init() 
	{
		trace("[App] init");
		
		isPortraitOrientation = Lib.current.stage.stageWidth < Lib.current.stage.stageHeight;
		
		currentStageWidth = Lib.current.stage.stageWidth;
		currentStageHeight = Lib.current.stage.stageHeight;
		
		scaleContentX = currentStageWidth / (isPortraitOrientation ? defaultScreenSizeMin : defaultScreenSizeMax);
		scaleContentY = currentStageHeight / (isPortraitOrientation ? defaultScreenSizeMax : defaultScreenSizeMin);
		
		scale = Math.min(scaleContentX, scaleContentY);
		scale = Math.round(scale * 10) / 10;
		
		_inited = true;
	}
	
	function activeChanged(active:Bool) 
	{
		// for override
	}
	
	function orientationChanged(portrait:Bool) 
	{
		// for override
	}
	
	static public function isAppActive():Bool
	{
		return _appIsActive;
	}
	
	/* SUGAR */
	
	static public function scaleInt(value:Float):Int 
	{
		return Math.ceil(value * App.scale);
	}
	
	static public function scaleFloat(value:Float):Float 
	{
		return value * App.scale;
	}
	
	static public function scaleRect(rectangle:Rectangle):Rectangle 
	{
		rectangle.x = scaleInt(rectangle.x);
		rectangle.y = scaleInt(rectangle.y);
		rectangle.width = scaleInt(rectangle.width);
		rectangle.height = scaleInt(rectangle.height);
		return rectangle;
	}
	
	static public function centerOnStageByX(target:DisplayObject) 
	{
		target.x = (currentStageWidth - target.width) / 2;
	}
	
	static public function centerOnStageByY(target:DisplayObject) 
	{
		target.y = (currentStageHeight - target.height) / 2;
	}
	
	static public function centerOnStage(target:DisplayObject, roundCoords:Bool = false) 
	{
		centerOnStageByX(target);
		centerOnStageByY(target);
		
		if (roundCoords)
		{
			target.x = Std.int(target.x);
			target.y = Std.int(target.y);
		}
	}
	
}