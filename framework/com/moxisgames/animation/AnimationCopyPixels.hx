package com.moxisgames.animation;

import com.moxisgames.interfaces.IAnimation;
import com.moxisgames.loop.Juggler;
import haxe.ds.StringMap;
import haxe.Json;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.PixelSnapping;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Ismagilov Marat
 */
class AnimationCopyPixels extends Bitmap implements IAnimation
{
	var _once:Bool = false;
	
	var _currentFrame:Int = 1;
	var _totalFrames:Int = 1;
	
	var _playing:Bool = false;
	
	var _source:BitmapData;
	var _rects:Array<Rectangle>;
	
	static private var _destPoint:Point = new Point();
	
	static private var _animsSourceBank:StringMap<AnimationCopyPixelsSrc> = new StringMap<AnimationCopyPixelsSrc>();
	
	static public function prepareAnimation(id:String, pathToJSON:String, pathToImage:String):Void 
	{
		var sourceBitmapData:BitmapData = Assets.getBitmapData(pathToImage);
		var framesData:Array<Dynamic> = Json.parse(Assets.getText(pathToJSON)).frames;
		
		var rects:Array<Rectangle> = [];
		
		for (i in 0...framesData.length) 
		{
			var frameData:Dynamic = framesData[i].frame;
			rects.push(new Rectangle(frameData.x, frameData.y, frameData.w, frameData.h));
		}
		
		_animsSourceBank.set(id, { source:sourceBitmapData, rects:rects } );
	}
	
	public function new(id:String, pathToJSON:String, pathToImage:String, smooth:Bool = true) 
	{
		if (!_animsSourceBank.exists(id))
			prepareAnimation(id, pathToJSON, pathToImage);
		
		var src:AnimationCopyPixelsSrc = _animsSourceBank.get(id);
		
		_source = src.source;
		_rects = src.rects;
		
		super(new BitmapData(Math.ceil(_rects[0].width), Math.ceil(_rects[0].height), true, 0), PixelSnapping.AUTO, smooth);
		
		_totalFrames = _rects.length;
		
		changeFrame();
	}
	
	/* INTERFACE IAnimation */
	
	public function update(_):Void 
	{
		if (_playing)
			changeFrame();
	}
	
	private function changeFrame():Void 
	{
		var rect:Rectangle = _rects[_currentFrame - 1];
		
		bitmapData.lock();
		bitmapData.copyPixels(_source, rect, _destPoint);
		bitmapData.unlock();
		
		//trace(_currentFrame + " = " + width + "_" + height);
		
		if (_currentFrame < _totalFrames)
		{
			_currentFrame++;
		}
		else
		{
			if (_once)
			{
				stop();
				
				_once = false;
			}
			else
			{
				_currentFrame = 1;
			}
		}
	}
	
	public function playOnce():Void 
	{
		_once = true;
		
		play();
	}
	
	public function play() 
	{
		if (!_playing)
		{
			_playing = true;
			Juggler.addToLoop(this);
		}
	}
	
	public function stop() 
	{
		if (_playing)
		{
			_playing = false;
			Juggler.removeFromLoop(this);
		}
	}
	
	public function gotoAndPlay(frame:Int):Void 
	{
		_currentFrame = checkMaxFrame(frame, _totalFrames);
		
		changeFrame();
		
		play();
	}
	
	public function gotoAndStop(frame:Int):Void 
	{
		stop();
		
		_currentFrame = checkMaxFrame(frame, _totalFrames);
		
		changeFrame();
	}
	
	public function destroy():Void 
	{
		stop();
		
		bitmapData.dispose();
		
		_source = null;
		_rects = null;
	}
	
	static inline function checkMaxFrame(frame:Int, total:Int):Int 
	{
		if (frame < 0) frame = 0;
		else if (frame > total) frame = total;
		
		return frame;
	}
	
}

typedef AnimationCopyPixelsSrc = {
	source:BitmapData,
	rects:Array<Rectangle>
};