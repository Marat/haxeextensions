package com.moxisgames.animation;

#if cpp
typedef AnimationOptimum = com.moxisgames.animation.AnimationTilesheet;
#else
typedef AnimationOptimum = com.moxisgames.animation.AnimationCopyPixels;
#end