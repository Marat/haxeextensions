package com.moxisgames.animation;

import com.moxisgames.interfaces.IAnimation;
import com.moxisgames.loop.Juggler;
import haxe.ds.StringMap;
import haxe.Json;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Ismagilov Marat
 */
class AnimationBitmaps extends Sprite implements IAnimation
{

	var _once:Bool = false;
	
	var _frames:Array<Bitmap>;
	
	var _frameView:Bitmap;
	
	var _currentFrame:Int = 1;
	var _totalFrames:Int = 1;
	
	var _playing:Bool = false;
	
	static private var _animsSourceBank:StringMap<Array<BitmapData>> = new StringMap<Array<BitmapData>>();
	
	static private function prepareAnimation(id:String, pathToJSON:String, pathToImage:String):Void 
	{
		var sourceBitmapData:BitmapData = Assets.getBitmapData(pathToImage);
		var framesData:Array<Dynamic> = Json.parse(Assets.getText(pathToJSON)).frames;
		
		var arr:Array<BitmapData> = [];
		
		for (i in 0...framesData.length) 
		{
			var frameData:Dynamic = framesData[i].frame;
			
			var bData:BitmapData = new BitmapData(frameData.w, frameData.h, true, 0);
			bData.copyPixels(sourceBitmapData, new Rectangle(frameData.x, frameData.y, frameData.w, frameData.h), new Point());
			
			arr.push(bData);
		}
		
		//Assets.cache.removeBitmapData(pathToImage);
		//sourceBitmapData.dispose();
		
		_animsSourceBank.set(id, arr);
	}
	
	public function new(id:String, pathToJSON:String, pathToImage:String, smooth:Bool = true) 
	{
		if (!_animsSourceBank.exists(id))
			prepareAnimation(id, pathToJSON, pathToImage);
		
		super();
		
		_frames = [];
		
		var arr:Array<BitmapData> = _animsSourceBank.get(id);
		_totalFrames = arr.length;
		
		for (i in 0..._totalFrames) 
		{
			var fr:Bitmap = new Bitmap(arr[i], AUTO, smooth);
			_frames.push(fr);
		}
		
		_frameView = _frames[_currentFrame];
		addChild(_frameView);
	}
	
	/* INTERFACE IAnimation */
	
	public function update(_):Void 
	{
		if (_playing)
			changeFrame();
	}
	
	private function changeFrame():Void 
	{
		removeChild(_frameView);
		
		_frameView = _frames[_currentFrame - 1];
		addChild(_frameView);
		
		if (_currentFrame < _totalFrames)
		{
			_currentFrame++;
		}
		else
		{
			if (_once)
			{
				stop();
				
				_once = false;
			}
			else
			{
				_currentFrame = 1;
			}
		}
	}
	
	public function play() 
	{
		if (!_playing)
		{
			_playing = true;
			Juggler.addToLoop(this);
		}
	}
	
	public function playOnce() 
	{
		_once = true;
		
		play();
	}
	
	public function stop() 
	{
		if (_playing)
		{
			_playing = false;
			Juggler.removeFromLoop(this);
		}
	}
	
	public function gotoAndPlay(frame:Int) 
	{
		_currentFrame = checkMaxFrame(frame, _totalFrames);
		
		changeFrame();
		
		play();
	}
	
	public function gotoAndStop(frame:Int) 
	{
		stop();
		
		_currentFrame = checkMaxFrame(frame, _totalFrames);
		
		changeFrame();
	}
	
	public function destroy():Void 
	{
		stop();
		
		removeChild(_frameView);
		_frameView = null;
		
		_frames = null;
	}
	
	static inline function checkMaxFrame(frame:Int, total:Int):Int 
	{
		if (frame < 1) frame = 1;
		else if (frame > total) frame = total;
		
		return frame;
	}
	
}