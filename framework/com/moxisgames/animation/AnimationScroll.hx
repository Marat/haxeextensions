package com.moxisgames.animation;

import com.moxisgames.interfaces.IAnimation;
import com.moxisgames.loop.Juggler;
import haxe.ds.StringMap;
import haxe.Json;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Ismagilov Marat
 */
class AnimationScroll extends Bitmap implements IAnimation
{
	var _once:Bool = false;
	
	var _currentFrame:Int = 1;
	var _totalFrames:Int = 1;
	
	var _playing:Bool = false;
	
	var _rects:Array<Rectangle>;
	
	static private var _animsSourceBank:StringMap<AnimationScrollSrc> = new StringMap<AnimationScrollSrc>();
	
	static public function prepareAnimation(id:String, pathToJSON:String, pathToImage:String):Void 
	{
		var sourceBitmapData:BitmapData = Assets.getBitmapData(pathToImage);
		var framesData:Array<Dynamic> = Json.parse(Assets.getText(pathToJSON)).frames;
		
		var rects:Array<Rectangle> = [];
		
		for (i in 0...framesData.length) 
		{
			var frameData:Dynamic = framesData[i].frame;
			rects.push(new Rectangle(frameData.x, frameData.y, frameData.w, frameData.h));
		}
		
		_animsSourceBank.set(id, { source:sourceBitmapData, rects:rects } );
	}
	
	public function new(id:String, pathToJSON:String, pathToImage:String, smooth:Bool = true) 
	{
		if (!_animsSourceBank.exists(id))
			prepareAnimation(id, pathToJSON, pathToImage);
		
		var src:AnimationScrollSrc = _animsSourceBank.get(id);
		
		_rects = src.rects;
		
		super(src.source, AUTO, smooth);
		
		_totalFrames = _rects.length;
		
		changeFrame();
		
		play();
	}
	
	/* INTERFACE IAnimation */
	
	public function update(_):Void 
	{
		if (_playing)
			changeFrame();
	}
	
	private function changeFrame():Void 
	{
		scrollRect = _rects[_currentFrame - 1];
		
		if (_currentFrame < _totalFrames)
		{
			_currentFrame++;
		}
		else
		{
			if (_once)
			{
				stop();
				
				_once = false;
			}
			else
			{
				_currentFrame = 1;
			}
		}
	}
	
	public function play() 
	{
		if (!_playing)
		{
			_playing = true;
			Juggler.addToLoop(this);
		}
	}
	
	public function stop() 
	{
		if (_playing)
		{
			_playing = false;
			Juggler.removeFromLoop(this);
		}
	}
	
	public function playOnce():Void 
	{
		_once = true;
		
		play();
	}
	
	public function gotoAndPlay(frame:Int):Void 
	{
		_currentFrame = checkMaxFrame(frame, _totalFrames);
		
		changeFrame();
		
		play();
	}
	
	public function gotoAndStop(frame:Int):Void 
	{
		stop();
		
		_currentFrame = checkMaxFrame(frame, _totalFrames);
		
		changeFrame();
	}
	
	public function destroy():Void 
	{
		stop();
		
		_rects = null;
	}
	
	static inline function checkMaxFrame(frame:Int, total:Int):Int 
	{
		if (frame < 0) frame = 0;
		else if (frame > total) frame = total;
		
		return frame;
	}
	
}

typedef AnimationScrollSrc = {
	source:BitmapData,
	rects:Array<Rectangle>
};