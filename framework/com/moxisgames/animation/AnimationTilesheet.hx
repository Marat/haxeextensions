package com.moxisgames.animation;

import com.moxisgames.interfaces.IAnimation;
import com.moxisgames.loop.Juggler;
import haxe.ds.StringMap;
import haxe.Json;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.Shape;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Ismagilov Marat
 */
class AnimationTilesheet extends Shape implements IAnimation
{
	var _once:Bool = false;
	
	var _currentFrame:Int = 1;
	
	var _playing:Bool = false;
	
	var _drawParams:Array<Float>;
	
	var _tilesheet:AnimTilesheet;
	
	static private var _animsSourceBank:StringMap<AnimTilesheet> = new StringMap<AnimTilesheet>();
	
	static public function prepareAnimation(id:String, pathToJSON:String, pathToImage:String):Void 
	{
		var sourceBitmapData:BitmapData = Assets.getBitmapData(pathToImage);
		var framesData:Array<Dynamic> = Json.parse(Assets.getText(pathToJSON)).frames;
		
		var rects:Array<Rectangle> = [];
		
		var tileSheet:AnimTilesheet = new AnimTilesheet(sourceBitmapData);
		
		var frames:Int = 0;
		
		for (i in 0...framesData.length) 
		{
			var frameData:Dynamic = framesData[i].frame;
			tileSheet.addTileRect(new Rectangle(frameData.x, frameData.y, frameData.w, frameData.h));
			frames++;
		}
		
		_animsSourceBank.set(id, tileSheet);
	}
	
	public function new(id:String, pathToJSON:String, pathToImage:String, smooth:Bool = true) 
	{
		super();
		
		if (!_animsSourceBank.exists(id))
			prepareAnimation(id, pathToJSON, pathToImage);
		
		super();
		
		_tilesheet = _animsSourceBank.get(id);
		
		_drawParams = [0, 0, _currentFrame];
		
		changeFrame();
	}
	
	private function changeFrame():Void 
	{
		this.graphics.clear();
		
		_drawParams[2] = _currentFrame - 1;
		
		_tilesheet.drawTiles(this.graphics, _drawParams);
		
		//trace(_currentFrame + " = " + width + "_" + height);
		
		if (_currentFrame < _tilesheet.totalFrames)
		{
			_currentFrame++;
		}
		else
		{
			if (_once)
			{
				stop();
				
				_once = false;
			}
			else
			{
				_currentFrame = 1;
			}
		}
	}
	
	/* INTERFACE com.moxisgames.interfaces.IAnimation */
	
	public function update(_):Void 
	{
		if (_playing)
			changeFrame();
	}
	
	public function playOnce():Void 
	{
		_once = true;
		
		play();
	}
	
	public function play():Void 
	{
		if (!_playing)
		{
			_playing = true;
			Juggler.addToLoop(this);
		}
	}
	
	public function stop():Void 
	{
		if (_playing)
		{
			_playing = false;
			Juggler.removeFromLoop(this);
		}
	}
	
	public function gotoAndPlay(frame:Int):Void 
	{
		_currentFrame = checkMaxFrame(frame, _tilesheet.totalFrames);
		
		changeFrame();
		
		play();
	}
	
	public function gotoAndStop(frame:Int):Void 
	{
		stop();
		
		_currentFrame = checkMaxFrame(frame, _tilesheet.totalFrames);
		
		changeFrame();
	}
	
	public function destroy():Void 
	{
		stop();
		
		_drawParams = null;
		
		_tilesheet = null;
	}
	
	static inline function checkMaxFrame(frame:Int, total:Int):Int 
	{
		if (frame < 1) frame = 1;
		else if (frame > total) frame = total;
		
		return frame;
	}
	
}