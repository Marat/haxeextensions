package com.moxisgames.animation;

import openfl.display.BitmapData;
import openfl.display.Tilesheet;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Ismagilov Marat
 */
class AnimTilesheet extends Tilesheet
{
	
	public var totalFrames:Int = 0;

	public function new(image:BitmapData) 
	{
		super(image);
	}
	
	override public function addTileRect(rectangle:Rectangle, centerPoint:Point = null):Int 
	{
		totalFrames++;
		return super.addTileRect(rectangle, centerPoint);
	}
	
}