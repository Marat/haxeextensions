package com.moxisgames.listeners;

import openfl.events.Event;
import openfl.events.EventDispatcher;

/**
 * ...
 * @author Ismagilov Marat
 */
class ActiveListener 
{
	public var dispatcher:EventDispatcher;
	public var event:String;
	public var listener:Dynamic->Void;
	public var useCapture:Bool;
	
	public function new(dispatcher:EventDispatcher, event:String, listener:Dynamic->Void, useCapture:Bool = false) 
	{
		this.dispatcher = dispatcher;
		this.event = event;
		this.listener = listener;
		this.useCapture = useCapture;
		
		dispatcher.addEventListener(event, listener, useCapture);
	}
	
	public function compare(disp:EventDispatcher, evt:String, list:Dynamic->Void, useCapt:Bool):Bool 
	{
		if (disp != dispatcher)
			return false;
		
		if (evt != event)
			return false;
		
		if (!Reflect.compareMethods(list, listener))
			return false;
		
		if (useCapt != useCapture)
			return false;
		
		return true;
	}
	
	public function remove():Void 
	{
		dispatcher.removeEventListener(event, listener, useCapture);
		
		dispatcher = null;
		listener = null;
	}
	
}
