package com.moxisgames.listeners;

import openfl.events.Event;
import openfl.events.EventDispatcher;
/**
 * ...
 * @author Ismagilov Marat
 */
class ListenersManager 
{
	
	private var _hash:Array<ActiveListener>;
	
	public function new() 
	{
		_hash = new Array<ActiveListener>();
	}
	
	public function addListener(dispatcher:EventDispatcher, event:String, listener:Dynamic->Void, useCapture:Bool = false):Void 
	{
		if (dispatcher == null) return;
		
		if (getListener(dispatcher, event, listener, useCapture) == null)
			_hash.push(new ActiveListener(dispatcher, event, listener, useCapture));
	}
	
	private function getListener(dispatcher:EventDispatcher, event:String, listener:Dynamic->Void, useCapture:Bool):ActiveListener 
	{
		for (list in _hash) 
		{
			if (list.compare(dispatcher, event, listener, useCapture))
				return list;
		}
		
		return null;
	}
	
	public function removeListener(dispatcher:EventDispatcher, event:String, listener:Dynamic->Void, useCapture:Bool = false):Void 
	{
		if (dispatcher == null) return;
		
		var list:ActiveListener = getListener(dispatcher, event, listener, useCapture);
		
		if (list != null)
		{
			list.remove();
			_hash.splice(_hash.indexOf(list), 1);
		}
	}
	
	public function clear():Void 
	{
		var i:Int = _hash.length - 1;
		
		while (i >= 0) 
		{
			var list:ActiveListener = _hash[i];
			list.remove();
			_hash.splice(i, 1);
			
			i--;
		}
	}
	
	public function destroy():Void 
	{
		clear();
		
		_hash = null;
	}
	
}