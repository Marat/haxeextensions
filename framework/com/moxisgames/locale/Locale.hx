package com.moxisgames.locale;

import haxe.ds.StringMap;
import haxe.xml.Fast;
import haxe.xml.Parser;
import msignal.Signal.Signal0;
import openfl.Assets;

/**
 * ...
 * @author Marat Ismagilov
 */
class Locale
{
	
	var _supportedLangs:Array<String>;
	var _langCode:String;
	var _hash:StringMap<String>;
	var _callback:Bool->Void;
	var _busy:Bool = false;
	
	public var onLanguageChanged(default, null):Signal0 = new Signal0();
	
	static private var _allowInstatiate:Bool = false;
	static private var _instance:Locale;
	@:isVar static public var change(get, null):Signal0;
	
	public function new() 
	{
		if (!_allowInstatiate) throw "Use singleton!";
		
		_hash = new StringMap<String>();
	}
	
	public function getCurrentLang():String 
	{
		return _langCode;
	}
	
	public function getString(stringID:String):String 
	{
		if (_hash.exists(stringID))
			return _hash.get(stringID);
		
		return stringID;
	}
	
	public function init(supportedLangs:Array<String>, currentLang:String, callback:Bool->Void = null):String 
	{
		_supportedLangs = supportedLangs;
		
		var langFound:Bool = false;
		
		for (i in 0...supportedLangs.length)
		{
			if (currentLang.toLowerCase().indexOf(supportedLangs[i].toLowerCase()) != -1)
			{
				currentLang = supportedLangs[i];
				langFound = true;
				break;
			}
		}
		
		if (!langFound) currentLang = supportedLangs[0];
		
		loadLang(currentLang, callback);
		
		return currentLang;
	}
	
	public function loadLang(langCode:String, callback:Bool->Void = null) 
	{
		if (_langCode == langCode || _busy) return;
		
		_busy = true;
		
		_langCode = langCode;
		
		_callback = callback;
		
		//PerformanceUtils.startMeasure("Lang");
		
		Assets.loadText("values/strings_" + langCode + ".xml")
			.onComplete(onLangXMLLoadComplete)
			.onError(onLangXMLLoadError);
	}
	
	function onLangXMLLoadComplete(text:String) 
	{
		var xml:Xml = Parser.parse(text);
		var lib:Xml = xml.firstElement();
		
		var fast = new Fast(lib);
		
		for (node in fast.nodes.string) 
			_hash.set(node.att.name, node.innerData);
		
		onLanguageChanged.dispatch();
		
		callback(true);
		
		_busy = false;
		
		//PerformanceUtils.endMeasure("Lang");
	}
	
	function onLangXMLLoadError(error:Dynamic) 
	{
		trace("Error [Locale] onLangXMLLoadError " + error);
		
		callback(false);
	}
	
	function callback(value:Bool) 
	{
		if (_callback != null)
		{
			_callback(value);
			_callback = null;
		}
	}
	
	static public function _(stringID:String):String 
	{
		return _instance.getString(stringID);
	}
	
	static private function get_change():Signal0
	{
		return _instance.onLanguageChanged;
	}
	
	static public function getInstance():Locale 
	{
		if (_instance == null)
		{
			_allowInstatiate = true;
			_instance = new Locale();
			_allowInstatiate = false;
		}
		
		return _instance;
	}
	
}