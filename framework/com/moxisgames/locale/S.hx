package com.moxisgames.locale;

import haxe.ds.StringMap;
import haxe.xml.Fast;
import haxe.xml.Parser;
import openfl.Assets;

/**
 * ...
 * @author Ismagilov Marat
 */
class S
{

	static private var _currentLocaleID:String;
	
	static private var _langs:Array<String> = [EN, RU];
	static private var _hash:StringMap<String>;
	
	static private var _callbacks:Array<Listener> = [];
	
	static public inline var RU:String = "ru";
	static public inline var EN:String = "en";
	
	public function new() 
	{
	}
	
	static public function init(defaultLocale:String) 
	{
		defaultLocale = defaultLocale.toLowerCase();
		
		for (i in _langs) 
		{
			if (defaultLocale.indexOf(i) != -1)
			{
				changeLocale(i);
				return;
			}
		}
		
		changeLocale(_langs[0]);
	}
	
	static public function changeLocale(newLocaleID:String):Void 
	{
		if (_currentLocaleID == newLocaleID) return;
		
		_currentLocaleID = newLocaleID;
		
		//Logger.log("Change locale " + newLocaleID);
		
		var xmlData = Assets.getText("values/strings_" + _currentLocaleID + ".xml");
		var xml:Xml = Parser.parse(xmlData);
		var lib:Xml = xml.firstElement();
		
		var fast = new Fast(lib);
		
		_hash = new StringMap<String>();
		
		for (node in fast.nodes.string) 
			_hash.set(node.att.name, node.innerData);
		
		var len:Int = _callbacks.length;
		for (i in 0...len) 
		{
			_callbacks[i].callback();
		}
	}
	
	static public function addChangeListener(listener:Void->Void, priority:Int = 0) 
	{
		removeChangeListener(listener);
		
		_callbacks.push( { callback:listener, priority:priority } );
		
		_callbacks.sort(compareMethod);
	}
	
	static private function compareMethod(list1:Listener, list2:Listener):Int 
	{
		if (list1.priority > list2.priority) return -1;
		else if (list1.priority < list2.priority) return 1;
		return 0;
	}
	
	static public function removeChangeListener(listener:Void->Void) 
	{
		for (list in _callbacks) 
		{
			if (Reflect.compareMethods(listener, list.callback))
			{
				_callbacks.remove(list);
				list.callback = null;
				return;
			}
		}
	}
	
	static public function _(stringID:String):String 
	{
		if (_hash.exists(stringID))
			return _hash.get(stringID);
		
		return stringID;
	}
	
	static public function getCurrentLocale():String 
	{
		return _currentLocaleID;
	}
	
}

typedef Listener = { 
	callback:Void->Void, 
	priority:Int 
}