package com.moxisgames.data;
import com.moxisgames.interfaces.IDestroyable;

/**
 * ...
 * @author Marat Ismagilov
 */
class FunctionWithArgs implements IDestroyable
{
	var _scope:Dynamic;
	var _func:haxe.Constraints.Function;
	var _args:Array<Dynamic>;

	public function new(scope:Dynamic, func:haxe.Constraints.Function, args:Array<Dynamic>) 
	{
		_scope = scope;
		_args = args;
		_func = func;
	}
	
	public function call():Dynamic
	{
		return Reflect.callMethod(_scope, _func, _args);
	}
	
	public function destroy() 
	{
		_scope = null;
		_func = null;
		_args.splice(0, _args.length);
		_args = null;
	}
	
}