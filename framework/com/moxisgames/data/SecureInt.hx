package com.moxisgames.data;

import com.moxisgames.utils.Random;
import haxe.crypto.Md5;

/**
 * ...
 * @author Ismagilov Marat
 */
class SecureInt
{
	private var _rand:Int;
	private var _int:Int;

	private var _hash(null, null):String;

	private var S1(null, never):String = Random.getUID(5);
	private static inline var S2:String = "1b964Il7";
	
	public function new(val:Int = 0) 
	{
		setValue(val);
	}
	
	public function getValue():Int 
	{
		if (getCurrentHash() != _hash)
		{
			throw "Error: SecureInt validate failed";
			
			return -1;
		}
		
		setValue(_int - _rand);
		
		return _int - _rand;
	}
	
	public function setValue(val:Int):Void 
	{
		generateSeed();
		
		_int = val + _rand;
		
		_hash = getCurrentHash();
	}
	
	// SYNTAX SUGAR
	
	public function addValue(val:Int):Void 
	{
		setValue(getValue() + val);
	}
	
	public function removeValue(val:Int):Void 
	{
		setValue(getValue() - val);
	}
	
	// PRIVATE UTILS
	
	private function generateSeed():Void 
	{
		_rand = Random.intRanged(1, 999);
	}
	
	private function getCurrentHash():String 
	{
		return Md5.encode(S1 + _int + S2 + _rand);
	}
	
}