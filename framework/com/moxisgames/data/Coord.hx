package com.moxisgames.data;

/**
 * ...
 * @author Marat Ismagilov
 */
class Coord
{

	public var x:Int;
	public var y:Int;
	
	public function new(xVal:Int = 0, yVal:Int = 0) 
	{
		setTo(xVal, yVal);
	}
	
	public function setTo(xVal:Int = 0, yVal:Int = 0) 
	{
		x = xVal;
		y = yVal;
	}
	
	public function equals(other:Coord):Bool 
	{
		return x == other.x && y == other.y;
	}
	
	public function clone():Coord 
	{
		return new Coord(x, y);
	}
	
	public function copyFrom(from:Coord) 
	{
		x = from.x;
		y = from.y;
	}
	
	public function toString():String 
	{
		return x + ":" + y;
	}
	
}