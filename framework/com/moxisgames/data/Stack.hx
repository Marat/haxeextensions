package com.moxisgames.data;
import com.moxisgames.interfaces.IDestroyable;

/**
 * ...
 * @author Marat Ismagilov
 */
class Stack<T> implements IDestroyable
{

	var _arr:Array<T>;
	
	public function new() 
	{
		_arr = [];
	}
	
	public function add(value:T) 
	{
		_arr.push(value);
	}
	
	public function getNext():T 
	{
		if (isEmpty()) return null;
		
		return _arr.shift();
	}
	
	public function size():Int 
	{
		return _arr.length;
	}
	
	public function isEmpty():Bool
	{
		return _arr.length == 0;
	}
	
	/* INTERFACE com.moxisgames.interfaces.IDestroyable */
	
	public function destroy():Void 
	{
		_arr.splice(0, _arr.length);
		_arr = null;
	}
	
}