package com.moxisgames.ui.layouts;

import openfl.display.DisplayObject;
import openfl.display.Sprite;

/**
 * ...
 * @author Ismagilov Marat
 */
class BaseLayout extends Sprite 
{
	
	// расстояние между объектами в пикс.
	@:isVar public var gap(get, set):Float = 0;
	
	// направление расстановки объектов в Layout
	@:isVar public var direction(get, set):String;
	
	// направление расстановки объектов в Layout
	@:isVar public var align(get, set):LayoutAlign = LEFT_TOP;
	
	private var _sizeType:LayoutSize;
	
	private var _maxBound:Float = 0;
	
	public function new(gapValue:Float, alignValue:LayoutAlign) 
	{
		super();
		
		if (alignValue == null)
			alignValue = LEFT_TOP;
		
		gap = gapValue;
		align = alignValue;
	}
	
	public function addChilds(childs:Array<DisplayObject>):Void 
	{
		var len:Int = childs.length;
		for (i in 0...len) 
			super.addChild(childs[i]);
		
		checkMaxBound();
		
		alignChildren();
	}
	
	private function alignChildren():Void 
	{
		// function for override, place your code here
	}
	
	override public function addChild(child:DisplayObject):DisplayObject 
	{
		var addedChild:DisplayObject = super.addChild(child);
		
		alignChildren();
		
		return addedChild;
	}
	
	function checkMaxBound() 
	{
		//
	}
	
	override public function removeChild(child:DisplayObject):DisplayObject 
	{
		var removedChild:DisplayObject = super.removeChild(child);
		
		checkMaxBound();
		
		alignChildren();
		
		return removedChild;
	}
	
	public function update():Void 
	{
		alignChildren();
	}
	
	public function removeAllChildren():Void 
	{
		while (numChildren > 0) 
			removeChildAt(0);
	}
	
	private function get_direction():String 
	{
		return direction;
	}
	
	private function set_direction(value:String):String 
	{
		direction = value;
		
		alignChildren();
		
		return direction;
	}
	
	private function get_align():LayoutAlign 
	{
		return align;
	}
	
	private function set_align(value:LayoutAlign):LayoutAlign 
	{
		align = value;
		
		alignChildren();
		
		return align;
	}
	
	private function get_gap():Float 
	{
		return gap;
	}
	
	private function set_gap(value:Float):Float 
	{
		gap = value;
		
		alignChildren();
		
		return gap;
	}
	
	public function getMaxWidth():Float 
	{
		#if flash
		return width;
		#else
		var minX:Float = 0;
		var maxX:Float = 0;
		
		for (index in 0...numChildren) 
		{
			var child:DisplayObject = getChildAt(index);
			minX = Math.min(minX, child.x);
			maxX = Math.max(maxX, child.x + child.width);
		}
		return maxX - minX;
		#end
	}
	
	public function getMaxHeight():Float 
	{
		#if flash
		return height;
		#else
		var minY:Float = 0;
		var maxY:Float = 0;
		
		for (index in 0...numChildren) 
		{
			var child:DisplayObject = getChildAt(index);
			minY = Math.min(minY, child.y);
			maxY = Math.max(maxY, child.y + child.height);
		}
		return maxY - minY;
		#end
	}
	
	#if (html5)
	override function get_width():Float 
	{
		return getMaxWidth();
	}
	override function get_height():Float 
	{
		return getMaxHeight();
	}
	#end
	
}

enum LayoutAlign
{
	LEFT_TOP;
	MIDDLE;
	RIGHT_BOTTOM;
}

enum LayoutSize
{
	FIXED;
	DYNAMIC;
}
