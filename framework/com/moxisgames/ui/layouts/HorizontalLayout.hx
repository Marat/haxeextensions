package com.moxisgames.ui.layouts;

import com.moxisgames.ui.layouts.BaseLayout.LayoutAlign;
import openfl.display.DisplayObject;

/**
 * ...
 * @author Ismagilov Marat
 */
class HorizontalLayout extends BaseLayout 
{
	
	public static inline var LEFT:String = "left";
	public static inline var RIGHT:String = "right";
	
	public function new(gapValue:Float = 0, alignValue:LayoutAlign = null, fixedHeight:Int = 0) 
	{
		super(gapValue, alignValue);
		
		_maxBound = fixedHeight;
		
		_sizeType = _maxBound == 0 ? DYNAMIC : FIXED;
		
		direction = RIGHT;
	}
	
	override private function alignChildren():Void 
	{
		checkMaxBound();
		
		var offset:Float = 0;
		
		for (i in 0...numChildren) 
		{
			var currentChild:DisplayObject = getChildAt(i);
			
			if (direction == RIGHT)
				currentChild.x = offset;
			else
				currentChild.x = -offset - currentChild.width;
				
			offset += currentChild.width + gap;
			
			switch (align) 
			{
				case LayoutAlign.LEFT_TOP:
					currentChild.y = 0;
					
				case LayoutAlign.MIDDLE:
					currentChild.y = (_maxBound - currentChild.height) / 2;
					
				case LayoutAlign.RIGHT_BOTTOM:
					currentChild.y = _maxBound - currentChild.height;
			}
		}
	}
	
	override function checkMaxBound() 
	{
		if (_sizeType == FIXED) return;
		
		_maxBound = 0;
		
		if (align != LEFT_TOP)
		{
			for (i in 0...numChildren) 
			{
				_maxBound = Math.max(_maxBound, getChildAt(i).height);
			}
		}
	}
	
}
