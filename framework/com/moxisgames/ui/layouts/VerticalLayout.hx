package com.moxisgames.ui.layouts;

import com.moxisgames.ui.layouts.BaseLayout.LayoutAlign;
import openfl.display.DisplayObject;

/**
 * ...
 * @author Ismagilov Marat
 */
class VerticalLayout extends BaseLayout
{
	
	public static inline var UP:String = "up";
	public static inline var DOWN:String = "down";
	
	public function new(gapValue:Float = 0, alignValue:LayoutAlign = null, fixedWidth:Int = 0) 
	{
		super(gapValue, alignValue);
		
		_maxBound = fixedWidth;
		
		_sizeType = _maxBound == 0 ? DYNAMIC : FIXED;
		
		direction = DOWN;
	}
	
	override private function alignChildren():Void 
	{
		checkMaxBound();
		
		var offset:Float = 0;
		
		for (i in 0...numChildren) 
		{
			var currentChild:DisplayObject = getChildAt(i);
			
			if (direction == DOWN)
				currentChild.y = offset;
			else
				currentChild.y = -offset - currentChild.height;
				
			offset += currentChild.height + gap;
			
			switch (align) 
			{
				case LayoutAlign.LEFT_TOP:
					currentChild.x = 0;
					
				case LayoutAlign.MIDDLE:
					currentChild.x = (_maxBound - currentChild.width) / 2;
					
				case LayoutAlign.RIGHT_BOTTOM:
					currentChild.x = _maxBound - currentChild.width;
			}
		}
	}
	
	override function checkMaxBound() 
	{
		if (_sizeType == FIXED) return;
		
		if (align != LEFT_TOP)
		{
			_maxBound = 0;
			
			for (i in 0...numChildren) 
			{
				_maxBound = Math.max(_maxBound, getChildAt(i).width);
			}
		}
	}
	
}
