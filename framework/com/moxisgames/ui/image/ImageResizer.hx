package com.moxisgames.ui.image;

import openfl.display.BitmapData;
import openfl.geom.Matrix;
import openfl.geom.Point;

/**
 * ...
 * @author Ismagilov Marat
 */
class ImageResizer
{
	
	public static inline var METHOD_PAN_AND_SCAN:String = "panAndScan";
	public static inline var METHOD_LETTERBOX:String = "letterbox";
	public static inline var METHOD_RAW:String = "raw";

	public function new() 
	{
	}
	
	static public function scaleBitmapData(source:BitmapData, scale:Float):BitmapData 
	{
		return bilinearIterative(
			source, 
			Math.ceil(source.width * scale), 
			Math.ceil(source.height * scale), 
			METHOD_RAW);
	}
	
	static public function resizeBitmapData(source:BitmapData, width:Int = 0, height:Int = 0):BitmapData 
	{
		if (width == 0 && height == 0)
			return source.clone();
		
		if (height == 0)
			height = Math.round(source.height * (width / source.width));
		else if (width == 0)
			width = Math.round(source.width * (height / source.height));
		
		return bilinearIterative(
			source,
			width,
			height,
			METHOD_RAW);
	}
	
	public static function bilinear(source:BitmapData, width:Int, height:Int, method:String, allowEnlarging:Bool=true):BitmapData
	{
		var scale:Point = scalePoint(
			new Point(source.width, source.height), 
			new Point(width, height), method, allowEnlarging);
		var result:BitmapData = new BitmapData(width, height, true, 0x0);
		var matrix:Matrix = new Matrix();
		matrix.scale(scale.x, scale.y);
		matrix.tx = (width - source.width * scale.x) / 2;
		matrix.ty = (height - source.height * scale.y) / 2;
		result.draw(source, matrix, null, null, null, true);
		return result;
	}
	
	public static function bilinearIterative(source:BitmapData, width:Int, height:Int, method:String, allowEnlarging:Bool = true,
		iterationMultiplier:Float = 2):BitmapData
	{
		var w:Int = source.width;
		var h:Int = source.height;
		var result:BitmapData = null;
		while(result == null || w != width || h != height)
		{
			w = Std.int(source.width > width 
				? Math.max(w / iterationMultiplier, width) 
				: Math.min(w * iterationMultiplier, width));
				
			h = Std.int(source.height > height 
				? Math.max(h / iterationMultiplier, height) 
				: Math.min(h * iterationMultiplier, height));
			
			var temp = bilinear(result != null ? result : source, w, h, method, 
				allowEnlarging);
			
			// уничтожаем промежуточные битмап даты
			if (result != null)
				result.dispose();
			
			result = temp;
		}
		return result;
	}
	
	public static function fixMethod(method:String):String
	{
		return (method == METHOD_PAN_AND_SCAN || method == METHOD_LETTERBOX)
			? method : METHOD_RAW;
	}
	
	public static function newDimensions(origDimensions:Point, containerDimensions:Point, method:String, allowEnlarging:Bool = true):Point
	{
		var s:Point = scalePoint(origDimensions, containerDimensions, method, allowEnlarging);
		return new Point(s.x * origDimensions.x, s.y * origDimensions.y);
	}
	
	private static function scalePoint(origDimensions:Point, containerDimensions:Point, method:String, allowEnlarging:Bool = true):Point
	{
		var sw:Float = containerDimensions.x / origDimensions.x;
		var sh:Float = containerDimensions.y / origDimensions.y;
		var sx:Float = sw; 
		var sy:Float = sh;
		
		if (method == METHOD_PAN_AND_SCAN)
			sx = sy = Math.max(sw, sh);
		else if(method == METHOD_LETTERBOX)
			sx = sy = Math.min(sw, sh);
		return new Point((sx > 1 && !allowEnlarging) ? 1 : sx, 
			(sy > 1 && !allowEnlarging) ? 1 : sy);
	}
	
}