package com.moxisgames.ui.buttons;

import com.moxisgames.ui.buttons.BaseButton;
import com.moxisgames.utils.CreateUtils;
import openfl.display.Bitmap;
import openfl.display.Sprite;

/**
 * ...
 * @author Ismagilov Marat
 */
class SwitchButton extends Sprite
{

	public var selected(get, set):Bool;
	private var _selected:Bool = false;
	
	private var _btnOn:BaseButton;
	private var _btnOff:BaseButton;
	
	public function new(onUp:Bitmap, offUp:Bitmap, ?onDown:Bitmap, ?offDown:Bitmap) 
	{
		super();
		
		_btnOn = CreateUtils.createButtonWithBitmaps(null, onUp, onDown);
		_btnOff = CreateUtils.createButtonWithBitmaps(null, offUp, offDown);
		
		selected = true;
	}
	
	function get_selected():Bool 
	{
		return _selected;
	}
	
	function set_selected(value:Bool):Bool 
	{
		if (value)
		{
			addChild(_btnOn);
			
			if (contains(_btnOff)) removeChild(_btnOff);
		}
		else
		{
			addChild(_btnOff);
			
			if (contains(_btnOn)) removeChild(_btnOn);
		}
		
		return _selected = value;
	}
	
	public function destroy():Void 
	{
		_btnOn.destroy();
		_btnOn = null;
		
		_btnOff.destroy();
		_btnOff = null;
	}
	
}