package com.moxisgames.ui.buttons;

import com.moxisgames.ui.buttons.BaseButton;
import com.moxisgames.ui.buttons.IconButton;
import com.moxisgames.utils.CreateUtils;
import com.moxisgames.utils.ImageOutline;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Ismagilov Marat
 */
class LabelButton extends IconButton
{

	private var _txtBitmap:Bitmap;
	private var _txtBitmapData:BitmapData;
	
	private var _txtLabel:TextField;
	private var _tFormat:TextFormat;
	
	private var _label:String;
	private var _outline:OutlineParams;
	
	public function new(clickCallback:BaseButton->Void, upBitmap:Bitmap, ?downBitmap:Bitmap, label:String, tFormat:TextFormat, ?outline:OutlineParams, multiline:Bool = false) 
	{
		super(clickCallback, upBitmap, downBitmap);
		
		_destroyIcon = true;
		
		_tFormat = tFormat;
		
		if (_tFormat == null) 
			_tFormat = new TextFormat();
		
		_txtLabel = CreateUtils.createTextField(label, _tFormat, multiline);
		
		_txtLabel.width = _upBitmap.width;
		_txtLabel.height = _upBitmap.height;
		
		_outline = outline;
		
		_txtBitmap = new Bitmap();
		addIcon(_txtBitmap);
		
		//addChild(_txtLabel);
		
		setLabel(label);
	}
	
	public function setLabel(label:String):Void
	{
		if (_label == label) return;
		
		_label = label;
		
		_txtLabel.text = _label;
		_txtLabel.setTextFormat(_tFormat);
		
		var oldBitmapData:BitmapData = _txtBitmapData;
		
		_txtBitmapData = redrawTxt();
		
		if (oldBitmapData != null)
			oldBitmapData.dispose();
			
		_txtBitmap.bitmapData = _txtBitmapData;
		
		if (_smoothing)
			setSmoothing(_smoothing);
		
		alignIcon();
	}
	
	function redrawTxt():BitmapData 
	{
		var newBitmapData:BitmapData = null;
		
		var temp = new BitmapData(
			Math.ceil(_txtLabel.width), 
			Math.ceil(_txtLabel.height + CreateUtils.ADDITIONAL_FILTER_SIZE), true, 0);
		
		temp.draw(_txtLabel);
		
		if (_outline == null)
		{
			newBitmapData = temp;
		}
		else
		{
			newBitmapData = ImageOutline.outline(temp, _outline.weight, _outline.color, _outline.alpha, _outline.antialias, _outline.threshold);
			temp.dispose();
		}
		
		return newBitmapData;
	}
	
	override public function destroy():Void 
	{
		_tFormat = null;
		_txtBitmap = null;
		_txtBitmapData = null;
		_txtLabel = null;
		_outline = null;
		
		super.destroy();
	}
	
}