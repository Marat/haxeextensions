package com.moxisgames.ui.buttons;

import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.Lib;
import openfl.geom.ColorTransform;

/**
 * ...
 * @author Ismagilov Marat
 */
class BaseButton extends Sprite
{
	var _container:Sprite;
	var _smoothing:Bool = false;
	
	var _upBitmap:Bitmap;
	var _downBitmap:Bitmap;
	
	var _clickCallback:BaseButton->Void;
	
	public var data:Dynamic;
	
	@:isVar public var enabled(default, set):Bool = true;
	
	static public var customEnabledBehavior:Sprite->Bool->Void;
	
	private static var ENABLE_TRANSFORM:ColorTransform = new ColorTransform();
	private static var DISABLE_TRANSFORM:ColorTransform = new ColorTransform(0.6, 0.6, 0.6, 1, -50, -50, -50);
	
	public function new(clickCallback:BaseButton->Void, upBitmap:Bitmap, downBitmap:Bitmap = null) 
	{
		super();
		
		_container = new Sprite();
		addChild(_container);
		
		_clickCallback = clickCallback;
		
		_upBitmap = upBitmap;
		_downBitmap = downBitmap;
		
		_container.addChild(_upBitmap);
		
		alignChildren();
		
		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.CLICK, onClick);
		
		mouseChildren = false;
	}
	
	function alignChildren() 
	{
		_upBitmap.x = -_upBitmap.width / 2;
		_upBitmap.y = -_upBitmap.height / 2;
		
		if (_downBitmap != null)
		{
			_downBitmap.x = -_downBitmap.width / 2;
			_downBitmap.y = -_downBitmap.height / 2;
		}
		
		_container.x = _container.width / 2;
		_container.y = _container.height / 2;
	}
	
	public function setClickCallback(clickCallback:BaseButton->Void) 
	{
		_clickCallback = clickCallback;
	}
	
	public function getButtonBounds():DisplayObject 
	{
		return _container;
	}
	
	private function onClick(e:MouseEvent):Void 
	{
		if (_clickCallback != null)
			_clickCallback(this);
	}
	
	private function onMouseDown(e:MouseEvent):Void 
	{
		removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		
		onPress();
		
		addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
	}
	
	private function onMouseUp(e:MouseEvent):Void 
	{
		removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		
		onRelease();
		
		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
	}
	
	function set_enabled(value:Bool):Bool 
	{
		if (enabled == value) return enabled;
		
		enabled = value;
		
		mouseEnabled = value;
		
		setEnabledView(value);
		
		return value;
	}
	
	function setEnabledView(value:Bool) 
	{
		if (customEnabledBehavior != null)
			customEnabledBehavior(this, value);
		else
			_container.transform.colorTransform = value ? ENABLE_TRANSFORM : DISABLE_TRANSFORM;
	}
	
	public function setSmoothing(value:Bool) 
	{
		_smoothing = value;
		
		_upBitmap.smoothing = _smoothing;
		
		if (_downBitmap != null) _downBitmap.smoothing = _smoothing;
	}
	
	function onPress() 
	{
		if (_downBitmap == null) return;
		
		if (_container.contains(_upBitmap))
			_container.removeChild(_upBitmap);
		
		_container.addChildAt(_downBitmap, 0);
	}
	
	function onRelease() 
	{
		if (_downBitmap == null) return;
		
		if (_container.contains(_downBitmap))
			_container.removeChild(_downBitmap);
		
		_container.addChildAt(_upBitmap, 0);
	}
	
	public function destroy() 
	{
		removeEventListener(MouseEvent.CLICK, onClick);
		removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		
		_container.removeChildren();
		
		removeChildren();
		
		_container = null;
		_clickCallback = null;
		_upBitmap = null;
		_downBitmap = null;
		data = null;
		
		if (parent != null && parent.contains(this))
			parent.removeChild(this);
	}
	
}