package com.moxisgames.ui.buttons;

import openfl.display.Bitmap;
import openfl.display.DisplayObject;

/**
 * ...
 * @author Ismagilov Marat
 */
class IconButton extends BaseButton
{
	
	var _icon:DisplayObject;
	var _destroyIcon:Bool;

	public var iconOffsetY(default, set):Int = 0;
	
	static public var pressOffset:Int = 2;
	
	public function new(clickCallback:BaseButton->Void, upBitmap:Bitmap, downBitmap:Bitmap = null) 
	{
		super(clickCallback, upBitmap, downBitmap);
	}
	
	public function addIcon(icon:DisplayObject, destroyIcon:Bool = true) 
	{
		_icon = icon;
		_destroyIcon = destroyIcon;
		
		if (_smoothing)
			setSmoothing(_smoothing);
		
		_container.addChild(_icon);
		
		alignIcon();
	}
	
	override function onPress() 
	{
		super.onPress();
		
		_icon.y += pressOffset;
	}
	
	override function onRelease() 
	{
		super.onRelease();
		
		alignIcon();
	}
	
	function set_iconOffsetY(value:Int):Int 
	{
		iconOffsetY = value;
		alignIcon();
		return iconOffsetY;
	}
	
	function alignIcon() 
	{
		_icon.x = Math.round(_upBitmap.x + (_upBitmap.width - _icon.width) / 2);
		_icon.y = Math.round(_upBitmap.y + (_upBitmap.height - _icon.height) / 2) + iconOffsetY;
	}
	
	override public function setSmoothing(value:Bool) 
	{
		super.setSmoothing(value);
		
		if (Std.is(_icon, Bitmap))
			cast(_icon, Bitmap).smoothing = value;
	}
	
	override public function destroy() 
	{
		if (_destroyIcon && Std.is(_icon, Bitmap))
			cast(_icon, Bitmap).bitmapData.dispose();
		
		_icon = null;
		
		super.destroy();
	}
	
}