package com.moxisgames.ui.text.render;

import com.moxisgames.locale.S;
import com.moxisgames.utils.CreateUtils;
import openfl.filters.BitmapFilter;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Ismagilov Marat
 */
class LocaleBitmapRenderText extends BitmapRenderText
{
	
	var _isLocale:Bool;
	
	public function new(text:String, tf:TextFormat=null, txtFilters:Array<BitmapFilter>=null, listen:Bool=false) 
	{
		_isLocale = listen;
		
		super(text, tf, txtFilters);
		
		if (_isLocale)
			S.addChangeListener(onLocaleChanged, -1);
	}
	
	override function createTextField(text:String, txtFilters:Array<BitmapFilter>) 
	{
		txtSource = CreateUtils.createLocaleTextField(text, _textFormat, txtFilters, _isLocale);
	}
	
	private function onLocaleChanged():Void 
	{
		redraw();
	}
	
	override public function setText(value:String):Void 
	{
		getLocaleTxtSource().setText(value);
		
		redraw();
	}
	
	public function setLocaleID(localeID:String):Void
	{
		getLocaleTxtSource().localeID = localeID;
		
		redraw();
	}
	
	public function getLocaleTxtSource():LocaleTextField
	{
		return cast(txtSource, LocaleTextField);
	}
	
	override public function dispose() 
	{
		if (_isLocale)
			S.removeChangeListener(onLocaleChanged);
		
		getLocaleTxtSource().destroy();
		
		super.dispose();
	}
	
}