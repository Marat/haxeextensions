package com.moxisgames.ui.text.render;

import com.moxisgames.utils.CreateUtils;
import com.moxisgames.utils.ImageOutline;
import com.moxisgames.utils.ImageOutline.OutlineParams;
import com.moxisgames.utils.PerformanceUtils;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.events.Event;
import openfl.filters.BitmapFilter;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Ismagilov Marat
 */
class BitmapRenderText extends Bitmap
{
	
	var _text:String;
	var _textFormat:TextFormat;
	var _multiline:Bool;
	var _outline:OutlineParams;
	
	public var multiline(get, set):Bool;
	public var wordWrap(get, set):Bool;
	
	public var txtSource(default, null):TextField;
	
	public var autoDestroy:Bool = true;
	
	public function new(text:String, tf:TextFormat = null, ?outline:OutlineParams, multilineV:Bool = false) 
	{
		super();
		
		_textFormat = tf;
		
		_outline = outline;
		
		_multiline = multilineV;
		
		txtSource = CreateUtils.createTextField("", _textFormat, _multiline);
		
		setText(text);
		
		addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
	}
	
	private function onAddedToStage(e:Event):Void 
	{
		removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		
		if (autoDestroy)
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
	}
	
	private function onRemovedFromStage(e:Event):Void 
	{
		dispose();
	}
	
	//
	public function setText(value:String):Void
	{
		_text = value;
		
		redraw();
	}
	
	public function redraw(force:Bool = false) 
	{
		if (txtSource.text == _text) return;
		
		//PerformanceUtils.startMeasure("[BitmapRenderText text = " + _text + "]");
		
		txtSource.text = _text;
		
		if (_textFormat != null)
			txtSource.setTextFormat(_textFormat);
		
		var newW:Int = Math.ceil(txtSource.width + CreateUtils.ADDITIONAL_FILTER_SIZE);
		var newH:Int = Math.ceil(txtSource.height);
		var bData:BitmapData;
		var oldBitmapData:BitmapData = null;
		var createNew:Bool = false;
		
		//trace("txtSource.width = " + txtSource.width);
		//trace("txtSource.textWidth = " + txtSource.getRect(txtSource).right);
		//trace("newW = " + newW);
		
		if (force || bitmapData == null || newW > bitmapData.width || newH > bitmapData.height)
		{
			bData = new BitmapData(newW, newH, true, 0);
			
			oldBitmapData = bitmapData;
			
			createNew = true;
		}
		else
		{
			bData = bitmapData;
			
			CreateUtils.clearBitmapData(bData);
		}
		
		bData.draw(txtSource);
		
		if (_outline != null)
		{
			var temp = ImageOutline.outline(bData, _outline.weight, _outline.color, _outline.alpha, _outline.antialias, _outline.threshold);
			bData.dispose();
			
			bData = temp;
			
			createNew = true;
		}
		
		if (createNew)
			bitmapData = bData;
		
		if (oldBitmapData != null)
			oldBitmapData.dispose();
			
		//PerformanceUtils.endMeasure("[BitmapRenderText text = " + _text + "]");
	}
	
	// width
	@:getter(width) #if !flash override #end function get_width():Float 
	{
		return txtSource != null ? txtSource.width + CreateUtils.ADDITIONAL_FILTER_SIZE : 0;
	}
	
	@:setter(width) #if !flash override #end function set_width(value:Float) 
	{
		txtSource.width = value;
		#if !flash return return txtSource.width; #end
	}
	
	// height
	@:getter(height) #if !flash override #end function get_height():Float 
	{
		return txtSource != null ? txtSource.height : 0;
	}
	
	@:setter(height) #if !flash override #end function set_height(value:Float) 
	{
		txtSource.height = value;
		#if !flash return return txtSource.height; #end
	}
	
	// multiline
	function get_wordWrap():Bool 
	{
		return txtSource.wordWrap;
	}
	
	function set_wordWrap(value:Bool):Bool 
	{
		txtSource.wordWrap = value;
		return txtSource.wordWrap;
	}
	
	// wordWrap
	function get_multiline():Bool 
	{
		return txtSource.multiline;
	}
	
	function set_multiline(value:Bool):Bool 
	{
		txtSource.multiline = value;
		return txtSource.multiline;
	}
	
	public function dispose() 
	{
		removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		
		txtSource = null;
		_textFormat = null;
		_outline = null;
		
		bitmapData.dispose();
	}
	
}