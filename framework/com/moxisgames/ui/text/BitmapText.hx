package com.moxisgames.ui.text;

import bitmapFont.BitmapFont;
import bitmapFont.BitmapTextField;
import openfl.display.PixelSnapping;
import openfl.events.Event;

/**
 * ...
 * @author Ismagilov Marat
 */
class BitmapText extends BitmapTextField
{

	public function new(?font:BitmapFont, text:String="", pixelSnapping:PixelSnapping=null, smoothing:Bool=false) 
	{
		super(font, text, pixelSnapping, smoothing);
		
		setAutoDestroy(true);
	}
	
	public function setAutoDestroy(enable:Bool) 
	{
		if (enable && !this.hasEventListener(Event.REMOVED_FROM_STAGE))
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		else
			removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
	}
	
	function onRemovedFromStage(e:Event):Void 
	{
		dispose();
	}
	
	override public function dispose() 
	{
		setAutoDestroy(false);
		
		super.dispose();
	}
	
}