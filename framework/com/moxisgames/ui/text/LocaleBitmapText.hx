package com.moxisgames.ui.text;

import bitmapFont.BitmapFont;
import com.moxisgames.locale.S;
import openfl.display.PixelSnapping;

/**
 * ...
 * @author Ismagilov Marat
 */
class LocaleBitmapText extends BitmapText
{
	
	var _listen:Bool;

	@:isVar public var localeID(get, set):String;
	
	public function new(?font:BitmapFont, pixelSnapping:PixelSnapping=null, smoothing:Bool=false, listen:Bool = true) 
	{
		super(font, "", pixelSnapping, smoothing);
		
		_listen = listen;
		
		if (_listen)
			S.addChangeListener(onLocaleChanged);
	}
	
	private function onLocaleChanged():Void 
	{
		localeID = localeID;
	}
	
	function get_localeID():String 
	{
		return localeID;
	}
	
	function set_localeID(value:String):String 
	{
		text = S._(value);
		
		return localeID = value;
	}
	
	override public function dispose() 
	{
		if (_listen)
			S.removeChangeListener(onLocaleChanged);
		
		super.dispose();
	}
	
}