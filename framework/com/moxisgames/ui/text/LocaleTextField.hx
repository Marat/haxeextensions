package com.moxisgames.ui.text;

import com.moxisgames.locale.Locale;
import openfl.events.Event;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Ismagilov Marat
 */
class LocaleTextField extends TextField
{

	@:isVar public var localeID(get, set):String;
	
	private var _textFormat:TextFormat;
	
	public function new(listen:Bool = true) 
	{
		super();
		
		if (listen)
			Locale.change.add(onLocaleChanged);
		
		addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
	}
	
	override public function setTextFormat(format:TextFormat, beginIndex:Int = 0, endIndex:Int = 0):Void 
	{
		_textFormat = format;
	}
	
	private function onLocaleChanged():Void 
	{
		localeID = localeID;
	}
	
	function get_localeID():String 
	{
		return localeID;
	}
	
	function set_localeID(value:String):String 
	{
		setText(Locale._(value));
		
		return localeID = value;
	}
	
	public function setText(value:String) 
	{
		text = value;
		
		if (_textFormat != null)
			super.setTextFormat(_textFormat);
	}
	
	function onRemovedFromStage(e:Event):Void 
	{
		destroy();
	}
	
	public function destroy() 
	{
		Locale.change.remove(onLocaleChanged);
		
		removeEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		
		_textFormat = null;
	}
	
}