package com.moxisgames.cookie;

import com.moxisgames.log.Logger;
import haxe.crypto.Md5;
import lime.system.BackgroundWorker;
import openfl.net.SharedObject;

/**
 * ...
 * @author Marat Ismagilov
 */
class Cookie
{
	
	var _soData:SharedObject;
	
	var _busy:Bool= false;
	var _changed:Bool = false;
	
	var _flushWorker:BackgroundWorker;
	
	// secure params
	var _secured:Bool = true;
	var _s:String;
	var _sortedParams:Array<String>;
	
	private static inline var P_KEY:String = "k";
	
	private static inline var USE_SAVE_WORKER:Bool = false;
	
	public function new(soData:SharedObject, name:String = "defaultCookieName", secured:Bool) 
	{
		_soData = soData;
		_secured = secured;
		
		if (USE_SAVE_WORKER)
		{
			_flushWorker = new BackgroundWorker();
			_flushWorker.doWork.add(workerSaveData);
			_flushWorker.onComplete.add(workerSaveDataComplete);
		}
		
		_s = "M" + name.charAt(0) + name.charAt(name.length - 1) + "mg_com";
		
		if (_secured) 
		{
			_sortedParams = Reflect.fields(_soData.data).copy();
			_sortedParams.remove(P_KEY);
			_sortedParams.sort(sortAlphabetical);
		}
		
		// validate data
		if (_secured && getData(P_KEY) != gk())
		{
			Logger.error("[Cookie] data is invalid! Clear cookies data!");
			Logger.log("[Cookie] = " + toString());
			Logger.warning("key generated = " + gk());
			
			//clear();
		}
		else
		{
			Logger.log("[Cookie] data is valid");
		}
	}
	
	public function setData(varName:String, value:Dynamic, flushData:Bool = false):Void
	{
		if (getData(varName) != value)
		{
			Reflect.setField(_soData.data, varName, value);
			
			if (_secured)
			{
				if (_sortedParams.indexOf(varName) == -1)
				{
					_sortedParams.push(varName);
					_sortedParams.sort(sortAlphabetical);
				}
				
				updateProtectKey();
			}
			
			_changed = true;
		}
		
		#if html5
		flush();
		#else
		if (flushData)
			flush();
		#end
	}
	
	public function removeData(varName:String):Void
	{
		if (Reflect.hasField(_soData.data, varName))
		{
			Reflect.deleteField(_soData.data, varName);
			
			if (_secured) 
			{
				if (_sortedParams.indexOf(varName) != -1)
				{
					_sortedParams.remove(varName);
				}
				
				updateProtectKey();
			}
			
			_changed = true;
		}
	}
	
	function updateProtectKey() 
	{
		Reflect.setField(_soData.data, P_KEY, gk());
	}
	
	public function flush():Void
	{
		if (!_changed || _busy) 
		{
			//Logger.warning("[Cookie] can't flush _hasChanges=" + _changed + ", _busy=" + _busy);
			return;
		}
		
		try 
		{
			if (_secured) updateProtectKey();
			
			if (USE_SAVE_WORKER)
			{
				_busy = true;
				_flushWorker.run("save");
			}
			else
			{
				_changed = false;
				_soData.flush();
			}
		}
		catch (msg:String)
		{
			trace("[Cookie] flush error = " + msg);
		}
	}
	
	function workerSaveData(arg:Dynamic) 
	{
		Logger.log("[Cookie] flush start");
		
		_changed = false;
		
		_soData.flush();
		
		_flushWorker.sendComplete();
	}

	function workerSaveDataComplete(arg:Dynamic) 
	{
		_busy = false;
		
		Logger.log("[Cookie] flush complete");
	}
	
	function gk():String
	{
		if (_sortedParams.length == 0) return "";
		
		var s:String = _s;
		
		for (i in 0..._sortedParams.length)
			s += _sortedParams[i] + Reflect.field(_soData.data, _sortedParams[i]);
		
		return Md5.encode(s);
	}
	
	public function toString():String 
	{
		var s:String = "[Cookie]";
		
		var keys = Reflect.fields(_soData.data); 
		
		for (key in keys)
			s += " " + key + "=" + Reflect.field(_soData.data, key);
		
		return s;
	}

	public function getData(varName:String, defaultValue:String = "", createIfNotExists:Bool = false):Dynamic
	{
		if (!Reflect.hasField(_soData.data, varName))
		{
			if (createIfNotExists) 
				setData(varName, defaultValue);
			
			return defaultValue;
		}
		
		return Reflect.field(_soData.data, varName);
	}
	
	public function getBool(varName:String, defaultValue:Bool = false, createIfNotExists:Bool = false):Bool 
	{
		if (!Reflect.hasField(_soData.data, varName))
		{
			if (createIfNotExists) 
				setData(varName, defaultValue);
			
			return defaultValue;
		}
		
		return Reflect.field(_soData.data, varName);
	}
	
	public function getFloat(varName:String, defaultValue:Float = 0, createIfNotExists:Bool = false):Float 
	{
		return Std.parseFloat(getData(varName, Std.string(defaultValue), createIfNotExists));
	}
	
	public function getInt(varName:String, defaultValue:Int = 0):Int 
	{
		#if html5
		return Std.int(Std.parseFloat(getData(varName, Std.string(defaultValue))));
		#else
		return Std.parseInt(getData(varName, Std.string(defaultValue)));
		#end
	}
	
	public function getIntInBounds(varName:String, defaultValue:Int = 0, min:Int = 0, max:Int = 1):Int
	{
		var value = getInt(varName, defaultValue);
		
		if (value < min)
			value = min;
		else if (value > max)
			value = max;
		
		return value;
	}
	
	public function hasData(varName:String):Bool 
	{
		return Reflect.hasField(_soData.data, varName);
	}
	
	public function clear() : Void
	{
		_soData.clear();
		flush();
	}
	
	public function destroy() 
	{
		if (USE_SAVE_WORKER)
		{
			_flushWorker.doWork.remove(workerSaveData);
			_flushWorker.onComplete.remove(workerSaveDataComplete);
			_flushWorker = null;
		}
		
		_soData = null;
		
		_sortedParams = null;
	}
	
	static private inline function sortAlphabetical(a:String, b:String):Int
	{
		a = a.toLowerCase();
		b = b.toLowerCase();
		if (a < b) return -1;
		if (a > b) return 1;
		return 0;
	}
	
}