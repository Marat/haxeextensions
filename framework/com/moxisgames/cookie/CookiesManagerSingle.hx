package com.moxisgames.cookie;

import openfl.Lib;
import openfl.net.SharedObject;

/**
 * Utility class for saving data to local machine
 * 
 * @author Ismagilov Marat
 */
class CookiesManagerSingle
{
	
	private static var _sharedData:SharedObject;
	private static var _enabled:Bool = true;
	static private var _hasChanges:Bool = false;
	
	public function new() 
	{
	}
	
	public static function init(soName:String):Void
	{
		_sharedData = SharedObject.getLocal(soName, "/");
	}

	public static function setData(varName:String, value:Dynamic, flushData:Bool = false):Void
	{
		//Logger.log("setData varName=" + varName + ", flushData=" + flushData);
		
		if (getData(varName) != Std.string(value))
		{
			if (_sharedData != null && _sharedData.data != null)
			{
				Reflect.setField(_sharedData.data, varName, value);
				_hasChanges = true;
			}
		}
		
		if (flushData)
			flush();
	}

	public static function removeData(varName:String):Void
	{
		if (Reflect.hasField(_sharedData.data, varName))
		{
			Reflect.deleteField(_sharedData.data, varName);
			_hasChanges = true;
		}
	}
	
	public static function flush():Void
	{
		if (!_enabled || !_hasChanges) return;
		
		try 
		{
			#if android
				#if (legacy || hybrid) openfl.Lib #else openfl.utils.JNI #end
				.postUICallback(
					function() {
						_sharedData.flush();
						_hasChanges = false;
					}
				);
			#else
			_sharedData.flush();
			_hasChanges = false;
			#end
		}
		catch (msg:String)
		{
			trace("[SOManager flush error = " + msg + "]");
		}
	}

	public static function getData(varName:String, defaultValue:String = ""):String
	{
		if (_sharedData == null || _sharedData.data == null || !Reflect.hasField(_sharedData.data, varName))
			return defaultValue;
		
		return Reflect.field(_sharedData.data, varName);
	}
	
	public static function getInt(varName:String, defaultValue:Int = 0):Int 
	{
		#if html5
		return Std.int(Std.parseFloat(getData(varName, Std.string(defaultValue))));
		#else
		return Std.parseInt(getData(varName, Std.string(defaultValue)));
		#end
	}
	
	public static function hasData(varName:String):Bool 
	{
		return _sharedData != null && _sharedData.data != null && Reflect.hasField(_sharedData.data, varName);
	}
	
	public static function clear() : Void
	{
		_sharedData.clear();
	}

}