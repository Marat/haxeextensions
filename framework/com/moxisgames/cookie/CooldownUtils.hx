package com.moxisgames.cookie;

import openfl.Lib;

/**
 * ...
 * @author Ismagilov Marat
 */
class CooldownUtils
{

	private var _times:Map<String, Int>;
	
	static private var _instance:CooldownUtils;
	
	public function new() 
	{
		_times = new Map<String, Int>();
	}
	
	public function setDuration(id:String, durationInSec:Int) 
	{
		if (durationInSec > 0)
		{
			var time:Int = getNowTimeInSeconds() + durationInSec;
			_times.set(id, time);
			
			CookiesManager.setData(id, time, true);
		}
	}
	
	public function setEndTime(id:String, endTimeInSec:Int) 
	{
		var now:Int = getNowTimeInSeconds();
		var durationInSec:Int = endTimeInSec - now;
		
		if (durationInSec > 0)
		{
			_times.set(id, endTimeInSec);
		}
		else
		{
			CookiesManager.removeData(id);
		}
	}
	
	public function removeCooldown(id:String)
	{
		if (_times.exists(id))
		{
			_times.remove(id);
			
			CookiesManager.removeData(id);
		}
	}
	
	public function getCooldownTime(id:String):Int
	{
		if (_times.exists(id))
		{
			var lastTime:Int = _times.get(id) - getNowTimeInSeconds();
			
			if (lastTime <= 0)
				removeCooldown(id);
			
			return lastTime < 0 ? 0 : lastTime;
		}
		
		return 0;
	}
	
	public function isOnCooldown(id:String):Bool 
	{
		return getCooldownTime(id) > 0;
	}
	
	static public function getInstance():CooldownUtils 
	{
		if (_instance == null) _instance = new CooldownUtils();
		
		return _instance;
	}
	
	static public function getNowTimeInSeconds():Int
	{
		return Math.round(Date.now().getTime() / 1000);
	}
	
}