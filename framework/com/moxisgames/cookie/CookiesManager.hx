package com.moxisgames.cookie;
import haxe.ds.StringMap;
import openfl.net.SharedObject;

/**
 * ...
 * @author Marat Ismagilov
 */
class CookiesManager
{

	var _cookies:StringMap<Cookie>;
	
	static private var _instance:CookiesManager;
	
	static private var _allowInstatiate:Bool = false;
	
	public function new() 
	{
		if (!_allowInstatiate) throw "Use getInstance() for access to singletone instance";
		
		_cookies = new StringMap<Cookie>();
	}
	
	public function getCookie(soName:String, secured:Bool = false):Cookie 
	{
		if (_cookies.exists(soName))
			return _cookies.get(soName);
		
		var soData = SharedObject.getLocal(soName, "/");
		var cookie = new Cookie(soData, soName, secured);
		_cookies.set(soName, cookie);
		return cookie;
	}
	
	public function flushAll() 
	{
		for (cookie in _cookies)
			cookie.flush();
	}
	
	static public function getInstance():CookiesManager 
	{
		if (_instance == null)
		{
			_allowInstatiate = true;
			_instance = new CookiesManager();
			_allowInstatiate = false;
		}
		
		return _instance;
	}
	
}