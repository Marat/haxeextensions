package com.moxisgames.utils;

/**
 * ...
 * @author Ismagilov Marat
 */
class StringUtils
{

	public static inline var SEC_IN_DAY:Int = 86400;
	public static inline var SEC_IN_HOUR:Int = 3600;
	public static inline var SEC_IN_MIN:Int = 60;
	
	public function new() 
	{
	}
	
	/**
	 * @param	timeInSeconds
	 * @return time HH:MM:SS
	 */
	public static function getTimeString(timeInSeconds:Int):String
	{
		if (timeInSeconds < 0)
			timeInSeconds = 0;
		
		var hours:Int = Math.floor(timeInSeconds / SEC_IN_HOUR);
		
		var mins:Int = Math.floor((timeInSeconds - (hours * SEC_IN_HOUR)) / SEC_IN_MIN);
		
		var seconds:Int = timeInSeconds % SEC_IN_MIN;
		
		var str:String = "";
		
		if (hours > 0)
			str += leadZero(hours) + ":";
		
		if (mins >= 0)
			str += leadZero(mins) + ":";
		
		str += leadZero(seconds);
		
		return str;
	}
	
	public static function removeWhitespacesInString(str:String):String 
	{
		//удаление пробелов в начале строки
		while((str.charAt(0) == ' ') && str.length > 0)
			str = str.substring(1, str.length);
			
		//удаление пробелов в конце строки
		while((str.charAt(str.length - 1) == ' ') && str.length > 0)
			str = str.substring(0, str.length - 1);
			
		return str;
	}
	
	public static function replaceParams(str:String, params:Array<Dynamic>):String 
	{
		if (str == "" || str == null)
			return "";
		
		for (i in 0...params.length) 
			str = insertParam(str, i, Std.string(params[i]));
		
		return str;
	}

	private static function insertParam(str:String, paramIndex:Int, paramValue:String):String 
	{
		var searchStr:String = "{" + paramIndex + "}";
		var i:Int;
		
		i = str.lastIndexOf(searchStr);
		if (i == -1)
			return str;

		while (i != -1) {
			str = str.substring(0, i) + paramValue + str.substring(i + searchStr.length, str.length);
			i = str.indexOf(searchStr);
		}

		return str;
	}
	
	/*public static function translit(str:String):String
	{
		var arrRUSSIAN:Array = ["А","Б","В","Г","Д","Е","Ё","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Э","Ю","Я","а","б","в","г","д","е","ё","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","ь","ы","ъ","э","ю","я"," "];
		var arrTRANSLATE:Array = ["A","B","V","G","D","E","E","Zh","Z","I","I","K","L","M","N","O","P","R","S","T","U","F","H","C","Ch","Sh","Sh'","E","Yu","Ya","a","b","v","g","d","e","e","zh","z","i","i","k","l","m","n","o","p","r","s","t","u","f","h","c","ch","sh","sh'","'","i","'","e","yu","ya","_"];
		var i:Int;
		while (i < arrRUSSIAN.length)
		{
			str = (str.split(arrRUSSIAN[i])).join(arrTRANSLATE[i]);
			i++;
		}
		return str;
	}*/
	
	public static function cutString(string:String, length:Int):String
	{
		if (string.length > length)
			string = string.substr(0, length) + "...";
		
		return string;
	}
	
	public static function addSeparatorToPrice(num:Int, separator:String = " "):String 
	{
		if (num < 1000) return Std.string(num);
		
		var pos:Int = Std.string(num).length - Math.floor((Std.string(num).length - 1) / 3) * 3;
		var str:String = Std.string(num).substr(0, pos);
		
		while (pos < Std.string(num).length) 
		{
			str += separator + Std.string(num).substr(pos, 3);
			pos += 3;
		}
		
		return str;
	}
	
	/**
	 * Метод для правильной работы с числительными
	 * 
	 * @param	count - число
	 * @param	form1 - форма обозначения числа в кол-ве 1
	 * @param	form2 - форма обозначения числа в кол-ве 2
	 * @param	form5 - форма обозначения числа в кол-ве 5
	 * @return - возращает строку в правильной форме 
	 */
	public static function pluralForm(count:Int, form1:String, form2:String, form5:String, addCountToString:Bool = true):String
	{
		var defaultCount:Int = count;
		count = Std.int(Math.abs(count) % 100);

		var n1:Int = count % 10;

		var form:String = "";
		
		if ( count > 10 && count < 20 )
			form = form5;
		else if ( n1 > 1 && n1 < 5 )
			form = form2;
		else if ( n1 == 1 )
			form = form1;
		else
			form = form5;
		
		if (addCountToString)
			return defaultCount + " " + form;
		
		return form;
	}
	
	static public inline function leadZero(value:Int):String 
	{
		if (value < 10) return "0" + value;
		return Std.string(value);
	}
	
	static public inline function leadZeros(value:Int, length:Int = 4):String 
	{
		var retval:String = Std.string(value);
		
		while (retval.length < length)
			retval = "0" + retval;
			
		return retval;
	}
	
}