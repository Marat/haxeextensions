package com.moxisgames.utils;

import com.moxisgames.ui.buttons.BaseButton;
import com.moxisgames.ui.text.LocaleTextField;
import motion.Actuate;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;
import openfl.display.PixelSnapping;
import openfl.geom.Matrix;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormat;
import openfl.utils.Object;

/**
 * ...
 * @author Ismagilov Marat
 */
class CreateUtils
{

	static public var ADDITIONAL_FILTER_SIZE:Float = 3;
	
	static public function drawToBitmap(target:DisplayObject, parent:DisplayObjectContainer = null):Bitmap 
	{
		var bData:BitmapData = new BitmapData(Math.ceil(target.width), Math.ceil(target.height), true, 0);
		
		if (parent == null)
			parent = target.parent;
		
		var bounds = target.getBounds(parent);
		bData.draw(target, new Matrix(1, 0, 0, 1, -bounds.left, -bounds.top));
		var bmp:Bitmap = new Bitmap(bData);
		bmp.x = bounds.x;
		bmp.y = bounds.y;
		return bmp;
	}
	
	static public function createBitmap(id:String, parent:DisplayObjectContainer = null, smooth:Bool = false, pixelSnapping:PixelSnapping = null, params:Object = null):Bitmap 
	{
		var bmp:Bitmap = new Bitmap(Assets.getBitmapData(id), pixelSnapping == null ? PixelSnapping.AUTO : pixelSnapping, smooth);
		
		if (parent != null)
			parent.addChild(bmp);
			
		if (params != null)
		{
			for (key in params)
			{
				Reflect.setProperty(bmp, key, params[key]);
			}
		}
		
		return bmp;
	}
	
	static public function createTextField(text:String, tf:TextFormat, multiline:Bool = false):TextField
	{
		var txt:TextField = new TextField();
		txt.autoSize = TextFieldAutoSize.LEFT;
		txt.selectable = false;
		txt.embedFonts = true;
		txt.mouseEnabled = false;
		//txt.border = true;
		
		if (multiline)
		{
			txt.multiline = true;
			txt.wordWrap = true;
		}
		
		txt.setTextFormat(tf);
		txt.defaultTextFormat = tf;
		
		txt.text = text;
		
		return txt;
	}
	
	static public function createLocaleTextField(localeID:String, tf:TextFormat, multiline:Bool = false, listen:Bool = true):LocaleTextField
	{
		var txt:LocaleTextField = new LocaleTextField(listen);
		txt.autoSize = TextFieldAutoSize.LEFT;
		txt.selectable = false;
		txt.embedFonts = true;
		txt.mouseEnabled = false;
		//txt.border = true;
		
		if (multiline)
		{
			txt.multiline = true;
			txt.wordWrap = true;
		}
		
		txt.setTextFormat(tf);
		txt.defaultTextFormat = tf;
		
		txt.localeID = localeID;
		
		return txt;
	}
	
	static public function createButtonWithBitmaps(callback:BaseButton->Void, upState:Bitmap, ?downState:Bitmap):BaseButton 
	{
		return new BaseButton(callback, upState, downState);
	}
	
	static public function clearBitmapData(bData:BitmapData) 
	{
		bData.fillRect(bData.rect, 0);
	}
	
	static public function clearArray(arr:Array<Dynamic>) 
	{
		arr.splice(0, arr.length);
	}
	
	static public function clearMap(map:Map<Dynamic, Dynamic>) 
	{
		for (key in map.keys())
			map.remove(key);
	}
	
	static public function stopActuate(args:Array<Dynamic>) 
	{
		for (obj in args)
			Actuate.stop(obj, null, false, false);
	}
	
}