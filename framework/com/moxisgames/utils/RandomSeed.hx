package com.moxisgames.utils;

/**
 * ...
 * @author Marat Ismagilov
 */
class RandomSeed
{

	public static var globalSeed(default, set):Int = 1;
	
	private static var _internalSeed:Int = 1;
	
	private static inline var MULTIPLIER:Int = 48271;
	private static inline var MODULUS:Int = 2147483647;
	
	/**
	 * Internal helper variables.
	 */
	private static var _floatHelper:Float = 0;
	private static var _intHelper:Int = 0;
	private static var _intHelper2:Int = 0;
	private static var _intHelper3:Int = 0;
	
	public function new() { }
	
	public static inline function resetGlobalSeed():Int
	{
		return globalSeed = Std.int(Math.random() * MODULUS);
	}
	
	/**
	 * Internal method to quickly generate a pseudorandom number. Used only by other functions of this class.
	 * Also updates the internal seed, which will then be used to generate the next pseudorandom number.
	 * 
	 * @return	A new pseudorandom number.
	 */
	private static inline function generateSeed():Int
	{
		return _internalSeed = ((_internalSeed * MULTIPLIER) % MODULUS) & MODULUS;
	}
	
	private static function set_globalSeed(NewSeed:Int):Int
	{
		if (NewSeed < 1)
		{
			NewSeed = 1;
		}
		
		if (NewSeed > MODULUS)
		{
			NewSeed = MODULUS;
		}
		
		_internalSeed = NewSeed;
		globalSeed = NewSeed;
		
		return globalSeed;
	}
	
	public static inline function bool():Bool
	{
		return chanceRollSeed();
	}
	
	public static inline function sign(Chance:Float = 50):Int
	{
		return chanceRollSeed(Chance) ? 1 : -1;
	}
	
	/**
	 * Returns a pseudorandom number between 0 and 1, inclusive.
	 */
	public static inline function float():Float
	{
		return generateSeed() / MODULUS;
	}
	
	/**
	 * Returns a pseudorandom integer between Min and Max, inclusive. Will not return a number in the Excludes array, if provided.
	 * Please note that large Excludes arrays can slow calculations.
	 * 
	 * @param	Min			The minimum value that should be returned. 0 by default.
	 * @param	Max			The maximum value that should be returned. 2,147,483,647 by default.
	 * @param	?Excludes	An optional array of values that should not be returned.
	 */
	public static function intRanged(Min:Int = 0, Max:Int = MODULUS, ?Excludes:Array<Int>):Int
	{
		if (Min == Max)
		{
			_intHelper = Min;
		}
		else
		{
			// Swap values if reversed
			
			if (Min > Max)
			{
				Min = Min + Max;
				Max = Min - Max;
				Min = Min - Max;
			}
			
			if (Excludes == null)
			{
				_intHelper = Math.floor(Min + float() * (Max - Min + 1));
			}
			else
			{
				do
				{
					_intHelper = Math.floor(Min + float() * (Max - Min + 1));
				}
				while (Excludes.indexOf(_intHelper) >= 0);
			}
		}
		
		return _intHelper;
	}
	
	/**
	 * Returns a pseudorandom float value between Min and Max, inclusive. Will not return a number in the Excludes array, if provided.
	 * Please note that large Excludes arrays can slow calculations.
	 * 
	 * @param	Min			The minimum value that should be returned. 0 by default.
	 * @param	Max			The maximum value that should be returned. 33,554,429 by default.
	 * @param	?Excludes	An optional array of values that should not be returned.
	 */
	public static function floatRanged(Min:Float = 0, Max:Float = 1, ?Excludes:Array<Float>):Float
	{
		if (Min == Max)
		{
			_floatHelper = Min;
		}
		else
		{
			// Swap values if reversed.
			
			if (Min > Max)
			{
				Min = Min + Max;
				Max = Min - Max;
				Min = Min - Max;
			}
			
			if (Excludes == null)
			{
				_floatHelper = Min + float() * (Max - Min);
			}
			else
			{
				do
				{
					_floatHelper = Min + float() * (Max - Min);
				}
				while (Excludes.indexOf(_floatHelper) >= 0);
			}
		}
		
		return _floatHelper;
	}
	
	/**
	 * Returns true or false based on the chance value (default 50%). 
	 * For example if you wanted a player to have a 30% chance of getting a bonus, call chanceRoll(30) - true means the chance passed, false means it failed.
	 * 
	 * @param 	Chance 	The chance of receiving the value. Should be given as a number between 0 and 100 (effectively 0% to 100%)
	 * @return 	Whether the roll passed or not.
	 */
	public static inline function chanceRoll(Chance:Float = 50):Bool
	{
		return floatRangedSeed(0, 100) < Chance;
	}
	
	@:generic
	public static function shuffleArray<T>(array:Array<T>):Array<T> 
	{
		var temp:Dynamic;
		var index:Int;
		
		for (i in 0...array.length) 
		{
			index = Std.int(float() * (array.length - i)) + i;
			temp = array[i];
			array[i] = array[index];
			array[index] = temp;
		}
		
		return array;
	}
	
	@:generic public static function getRandomElement<T>(array:Array<T>):T 
	{
		return array[Math.floor(float() * array.length)];
	}
	
	@:generic public static function getMapRandomElement<T>(map:Iterable<T>):T 
	{
		var arr = Lambda.array(map);
		return arr[intRanged(0, arr.length)];
	}
	
}