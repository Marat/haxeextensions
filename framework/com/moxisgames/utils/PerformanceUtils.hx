package com.moxisgames.utils;
import com.moxisgames.log.Logger;
import haxe.ds.StringMap;
import openfl.Lib;
import openfl.system.System;

/**
 * ...
 * @author Ismagilov Marat
 */
class PerformanceUtils
{

	static private var _measuresHash:StringMap<Int>;
	
	static private inline var DEFAULT_MES_NAME:String = "default";
	
	public function new() 
	{
	}
	
	static public function startMeasure(id:String = DEFAULT_MES_NAME) 
	{
		if (_measuresHash == null)
			_measuresHash = new StringMap<Int>();
		
		_measuresHash.set(id, Lib.getTimer());
		
		Logger.log(">> MEASURE " + id + " started");
	}
	
	static public function endMeasure(id:String = DEFAULT_MES_NAME) 
	{
		if (_measuresHash.exists(id))
		{
			var startTime:Int = _measuresHash.get(id);
			_measuresHash.remove(id);
			
			Logger.log(">> MEASURE " + id + " finished in " + ((Lib.getTimer() - startTime) / 1000) + " sec.");
		}
	}
	
	static public inline function gc():Void 
	{
		#if flash
		System.gc();
		#end
		
        System.gc();
	}
	
}