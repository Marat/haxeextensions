package com.moxisgames.utils;

/**
 * ...
 * @author Ismagilov Marat
 */
class Random
{
	
	public function new() {}
	
	/** 
	 * Return a random boolean value (true or false) 
	 **/
	public static inline function bool():Bool
	{
		return Math.random() < 0.5;
	}
	
	public static function chance(chance:Int):Bool 
	{
		return intRanged(0, 100) <= chance;
	}
	
	/**
	 * Returns either a 1 or -1. 
	 * 
	 * @param	Chance	The chance of receiving a positive value. Should be given as a number between 0 and 100 (effectively 0% to 100%)
	 * @return	1 or -1
	 */
	public static inline function sign():Int
	{
		return bool() ? 1 : -1;
	}
	
	/** Return a random integer between 'from' and 'to', inclusive. */
	public static inline function intRanged(from:Int, to:Int):Int
	{
		return from + Math.floor(((to - from + 1) * Math.random()));
	}
	
	public static inline function floatRanged(from:Float, to:Float):Float
	{
		return from + (to - from) * Math.random();
	}
	
	@:generic public static function shuffleArray<T>(array:Array<T>):Array<T> 
	{
		var temp:Dynamic;
		var index:Int;
		
		for (i in 0...array.length) 
		{
			index = Std.int(Math.random() * (array.length - i)) + i;
			temp = array[i];
			array[i] = array[index];
			array[index] = temp;
		}
		
		return array;
	}
	
	public static function shuffleDynamicArray(array:Array<Dynamic>):Array<Dynamic> 
	{
		var temp:Dynamic;
		var index:Int;
		
		for (i in 0...array.length) 
		{
			index = Std.int(Math.random() * (array.length - i)) + i;
			temp = array[i];
			array[i] = array[index];
			array[index] = temp;
		}
		
		return array;
	}
	
	@:generic public static function getArrayRandomElement<T>(array:Array<T>):T 
	{
		return array[Math.floor(Math.random() * array.length)];
	}
	
	@:generic public static function getMapRandomElement<T>(map:Iterable<T>):T 
	{
		var arr = Lambda.array(map);
		return arr[Std.random(arr.length)];
	}
	
	public static inline function getUID(numChars:Int = 10):String
	{
		var CHARS:String = "abcdefghijklnrstxyzABDEFGHIJKLMNOPQRSTXYZ1234567890";
		var CHARS_LEN:Int = CHARS.length;
		var retval:String = "";
		
		for (i in 0...numChars)
			retval += CHARS.charAt(intRanged(0, CHARS_LEN - 1));
		
		return retval;
	}
	
}