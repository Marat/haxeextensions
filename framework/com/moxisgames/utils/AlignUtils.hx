package com.moxisgames.utils;

import openfl.display.DisplayObject;
import openfl.Lib;

/**
 * ...
 * @author Ismagilov Marat
 */
class AlignUtils
{

	public function new() 
	{
	}
	
	static public function alignBy(target:DisplayObject, alignBy:DisplayObject, byX:Bool = true, byY:Bool = true):Void 
	{
		if (byX)
			target.x = Std.int(alignBy.x + (alignBy.width - target.width) / 2);
			
		if (byY)
			target.y = Std.int(alignBy.y + (alignBy.height - target.height) / 2);
	}
	
	static public function alignOnStage(target:DisplayObject, byX:Bool = true, byY:Bool = true):Void 
	{
		if (byX)
			target.x = (Lib.current.stage.stageWidth - target.width) / 2;
			
		if (byY)
			target.y = (Lib.current.stage.stageHeight - target.height) / 2;
	}
	
}