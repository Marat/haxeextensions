package com.moxisgames.mvc.model;
import openfl.events.Event;

/**
 * ...
 * @author Ismagilov Marat
 */
class AbstractActor
{

	public function new() 
	{
	}
	
	public function dispatch(event:Event) 
	{
		MVC.dispatch(event);
	}
	
}