package com.moxisgames.mvc.view;
import com.moxisgames.listeners.ListenersManager;
import haxe.ds.StringMap;
import openfl.display.DisplayObject;
import openfl.events.Event;

/**
 * ...
 * @author Ismagilov Marat
 */
@:rtti
class AbstractMediator
{
	
	var _view:DisplayObject;
	
	var _contextListeners:StringMap<Dynamic->Void>;
	var _viewListeners:ListenersManager;

	public function new(view:DisplayObject) 
	{
		_view = view;
	}
	
	public function complete():Void
	{
		// for override
	}
	
	public function dispatch(event:Event) 
	{
		MVC.dispatch(event);
	}
	
	public function addContextListener(eventType:String, handler:Dynamic->Void) 
	{
		MVC.addListener(eventType, handler);
		
		if (_contextListeners == null)
			_contextListeners = new StringMap<Dynamic->Void>();
			
		_contextListeners.set(eventType, handler);
	}
	
	public function removeContextListener(eventType:String, handler:Dynamic->Void) 
	{
		MVC.removeListener(eventType, handler);
		
		_contextListeners.remove(eventType);
	}
	
	public function addViewListener(eventType:String, handler:Dynamic->Void) 
	{
		if (_viewListeners == null)
			_viewListeners = new ListenersManager();
			
		_viewListeners.addListener(_view, eventType, handler);
	}
	
	public function removeViewListener(eventType:String, handler:Dynamic->Void) 
	{
		_viewListeners.removeListener(_view, eventType, handler);
	}
	
	public function destroy() 
	{
		if (_contextListeners != null)
		{
			var events = _contextListeners.keys();
			
			for (eventType in events)
				MVC.removeListener(eventType, _contextListeners.get(eventType));
			
			_contextListeners = null;
		}
		
		if (_viewListeners != null)
		{
			_viewListeners.destroy();
			_viewListeners = null;
		}
		
		_view = null;
		
		var flds = Type.getInstanceFields(Type.getClass(this));
		
		for (fieldName in flds) 
		{
			if (!Reflect.isFunction(Reflect.getProperty(this, fieldName)))
				Reflect.setField(this, fieldName, null);
		}
	}
	
}