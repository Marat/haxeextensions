package com.moxisgames.mvc;

import com.moxisgames.log.Logger;
import com.moxisgames.mvc.controller.AbstractCommand;
import com.moxisgames.mvc.view.AbstractMediator;
import haxe.ds.ObjectMap;
import haxe.ds.StringMap;
import haxe.EnumTools.EnumValueTools;
import haxe.rtti.Rtti;
import openfl.display.DisplayObject;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.Lib;

/**
 * ...
 * @author Ismagilov Marat
 */
class MVC
{

	static public var debug:Bool = false;
	
	static private var _commands:StringMap<Class<AbstractCommand>> = new StringMap<Class<AbstractCommand>>();
	static private var _models:ObjectMap<Dynamic, Dynamic> = new ObjectMap<Dynamic, Dynamic>();
	static private var _mediators:ObjectMap<DisplayObject, AbstractMediator> = new ObjectMap<DisplayObject, AbstractMediator>();
	
	static private var _detainedCommands:Array<AbstractCommand> = [];
	
	static private var _dispatcher:EventDispatcher = new EventDispatcher();
	
	static private inline var CLASS_ENUM_NAME:String = "CClass";
	
	public function new()
	{
	}
	
	// COMMANDS
	
	static public function registerCommand(eventType:String, eventClass:Class<Event>, commandClass:Class<AbstractCommand>) 
	{
		var commandKey:String = getEventHash(eventType, eventClass);
		
		if (!_commands.exists(commandKey))
		{
			_commands.set(commandKey, commandClass);
		}
	}
	
	static public function unregisterCommand(eventType:String, eventClass:Class<Event>) 
	{
		var commandKey:String = getEventHash(eventType, eventClass);
		
		if (_commands.exists(commandKey))
		{
			_commands.remove(commandKey);
		}
	}
	
	static public function commandDetain(cmd:AbstractCommand) 
	{
		cmd.detained = true;
		
		_detainedCommands.push(cmd);
	}
	
	static public function commandRelease(cmd:AbstractCommand) 
	{
		_detainedCommands.remove(cmd);
		
		cmd.detained = false;
		cmd.destroy();
	}
	
	// MODELS
	
	static public function registerModel<T>(ModelClass:Class<T>):T 
	{
		var instance = Type.createInstance(ModelClass, []);
		_models.set(ModelClass, instance);
		return instance;
	}
	
	static public function unregisterModel<T>(ModelClass:Class<T>):T 
	{
		var instance = _models.get(ModelClass);
		_models.remove(ModelClass);
		return instance;
	}
	
	static public function getModelByClass<T>(ModelClass:Class<T>):T
	{
		return _models.get(ModelClass);
	}
	
	static public function hasModel(ModelClass:Class<Dynamic>):Bool
	{
		return _models.exists(ModelClass);
	}
	
	// VIEW
	
	static public function registerMediator(view:DisplayObject, MediatorClass:Class<AbstractMediator>) 
	{
		var mediator = Type.createInstance(MediatorClass, [view]);
		
		//var time:Int = Lib.getTimer();
		
		var rtti = Rtti.getRtti(MediatorClass);
		
		for (field in rtti.fields.iterator()) 
		{
			if (Reflect.getProperty(mediator, field.name) == null)
			{
				if (EnumValueTools.getName(field.type) == CLASS_ENUM_NAME)
				{
					var FieldClass = Type.resolveClass(field.type.getParameters()[0]);
					
					// set view to mediator
					if (Std.is(view, FieldClass))
					{
						Reflect.setField(mediator, field.name, view);
					}
					// set model to mediator
					else if (hasModel(FieldClass))
					{
						Reflect.setField(mediator, field.name, getModelByClass(FieldClass));
					}
				}
			}
		}
		
		_mediators.set(view, mediator);
		
		//Logger.log("Med time = " + (Lib.getTimer() - time));
		
		mediator.complete();
	}
	
	static public function unregisterMediator(view:DisplayObject) 
	{
		if (!_mediators.exists(view)) return;
		
		var mediator = _mediators.get(view);
		_mediators.remove(view);
		mediator.destroy();
	}
	
	// DISPATCHER
	
	static public function dispatch(event:Event)
	{
		var commandKey:String = getEventHash(event.type, Type.getClass(event));
		
		if (_commands.exists(commandKey))
		{
			var time:Int = Lib.getTimer();
			
			if (debug)
				Logger.log("MVC create instance for " + commandKey);
			
			var CmdClass = _commands.get(commandKey);
			
			var command:AbstractCommand = Type.createInstance(CmdClass, []);
			
			var rtti = Rtti.getRtti(CmdClass);
			
			for (field in rtti.fields.iterator()) 
			{
				if (Reflect.getProperty(command, field.name) == null)
				{
					if (EnumValueTools.getName(field.type) == CLASS_ENUM_NAME)
					{
						var FieldClass = Type.resolveClass(field.type.getParameters()[0]);
						
						// set event to command
						if (Std.is(event, FieldClass))
						{
							Reflect.setField(command, field.name, event);
						}
						// set model to command
						else if (hasModel(FieldClass))
						{
							Reflect.setField(command, field.name, getModelByClass(FieldClass));
						}
					}
				}
			}
			
			//if (Reflect.hasField(command, "event"))
				//Reflect.setField(command, "event", event);
			
			if (debug)
				Logger.log("MVC start execute " + getShortClassName(CmdClass));
			
			command.execute();
			
			if (debug)
				Logger.log("MVC execute complete " + getShortClassName(CmdClass));
				
			//Logger.log("MVC dispatch " + commandKey);
			
			if (!command.detained)
				command.destroy();
			
			Logger.log("MVC total time = " + (Lib.getTimer() - time));
		}
		
		_dispatcher.dispatchEvent(event);
	}
	
	static private function getShortClassName(TargetClass:Class<Dynamic>):String 
	{
		var className = Type.getClassName(TargetClass);
		
		return className.substr(className.lastIndexOf(".") + 1);
	}
	
	static public function addListener(eventType:String, handler:Dynamic->Void) 
	{
		_dispatcher.addEventListener(eventType, handler);
	}
	
	static public function removeListener(eventType:String, handler:Dynamic->Void) 
	{
		_dispatcher.removeEventListener(eventType, handler);
	}
	
	static private function getEventHash(eventType:String, eventClass:Class<Event> = null):String 
	{
		return (eventClass != null ? Type.getClassName(eventClass) + "." : "") + eventType.toUpperCase();
	}
	
}