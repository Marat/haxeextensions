package com.moxisgames.mvc.controller;
import openfl.events.Event;

/**
 * ...
 * @author Ismagilov Marat
 */
@:rtti
class AbstractCommand
{
	
	public var detained:Bool = false;
	
	public function new() 
	{
	}
	
	public function execute() 
	{
		// for override
	}
	
	public function detain() 
	{
		MVC.commandDetain(this);
	}
	
	public function release() 
	{
		MVC.commandRelease(this);
	}
	
	public function dispatch(event:Event) 
	{
		MVC.dispatch(event);
	}
	
	public function destroy() 
	{
		var flds = Type.getInstanceFields(Type.getClass(this));
		
		for (fieldName in flds) 
		{
			if (!Reflect.isFunction(Reflect.getProperty(this, fieldName)))
				Reflect.setField(this, fieldName, null);
		}
	}
	
}