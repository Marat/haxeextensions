package com.moxisgames;

import flash.Lib;

#if android
import openfl.utils.JNI;
#end

class ExtensionTest {
	
	
	public static function sampleMethod (inputValue1:Int, inputValue2:Int):Int {
		
		#if android
		var resultJNI = extension_test_sample_method_jni(inputValue1, inputValue2);
		return resultJNI;
		
		#elseif flash
		return 0;
		#end
	}
	
	#if android
	//()V - ():Void
	//()F - ():Float
	//(Ljava/lang/String;)Z - (String):Bool
	//(Ljava/lang/String;III)V - (String, Int, Int, Int):Void
	
	// import org.haxe.lime.HaxeObject;
	// (Lorg/haxe/lime/HaxeObject;)V - void init(HaxeObject callback) см. Admob.java 
	// callback.call(name, args);
	private static var extension_test_sample_method_jni = JNI.createStaticMethod ("org.haxe.extension.ExtensionTest", "sampleMethod", "(II)I");
	#end
	
}