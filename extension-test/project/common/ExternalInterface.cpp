#ifndef STATIC_LINK
#define IMPLEMENT_API
#endif

#if defined(HX_WINDOWS) || defined(HX_MACOS) || defined(HX_LINUX)
#define NEKO_COMPATIBLE
#endif


#include <hx/CFFI.h>
#include "Utils.h"

#import "Reachability-Swift.h"

using namespace extension_test;



static value extension_test_sample_method (value inputValue) {
	
	BOOL connect = [Reachability isConnectedToNetwork];
	NSLog(@"connect = %@", connect);
	
	int returnValue = SampleMethod(val_int(inputValue));
	return alloc_int(returnValue);
	
}
DEFINE_PRIM (extension_test_sample_method, 1);



extern "C" void extension_test_main () {
	
	val_int(0); // Fix Neko init
	
}
DEFINE_ENTRY_POINT (extension_test_main);



extern "C" int extension_test_register_prims () { return 0; }