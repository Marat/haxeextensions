package com.moxisgames.notifications;

#if android
import lime.system.JNI;
#end

class ScheduledNotifications {

    private static var _schedule:Dynamic;
    private static var _cancel:Dynamic;

    public function new():Void {}

    static public function schedule(slotId:Int, title:String, text:String, ticker:String, delayIsSecs:Int):Void {
        if (_schedule == null) {
            #if android
            _schedule = JNI.createStaticMethod("com/moxisgames/notifications/ScheduledNotifications", "schedule", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V");
            #elseif ios
            // TODO ios ScheduledNotifications
            #end
        }
        _schedule(slotId, title, text, ticker, delayIsSecs * 1000);
    }

    static public function cancel(slotId:Int):Void {
        if (_cancel == null) {
            #if android
            _cancel = JNI.createStaticMethod("com/moxisgames/notifications/ScheduledNotifications", "cancel", "(I)V");
            #elseif ios
            // TODO ios ScheduledNotifications
            #end
        }
        _cancel(slotId);
    }
}