package com.moxisgames.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import org.haxe.extension.Extension;

public class ScheduledNotifications extends Extension {

    public static boolean isActive = false;

    public ScheduledNotifications() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        isActive = true;
    }

    @Override
    public void onStop() {
        isActive = false;

        super.onStop();
    }

    public static void schedule(int id, String title, String text, String ticker, int ms) {
        Intent alarmIntent = new Intent(mainContext, NotifyService.class);
        alarmIntent.putExtra("id", id);
        alarmIntent.putExtra("title", title);
        alarmIntent.putExtra("text", text);
        alarmIntent.putExtra("ticker", ticker);

        PendingIntent pendingIntent = PendingIntent.getService(mainContext, 0, alarmIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) mainContext.getSystemService(Context.ALARM_SERVICE);

        long time = SystemClock.elapsedRealtime() + ms;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, time, pendingIntent);
        else
            alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, time, pendingIntent);

        // Log.w("LocalNotifications", "scheduled in " + (ms / 1000) + " sec.");
    }

    public static void cancel(int id) {
        Intent intent = new Intent(mainContext, NotifyService.class);
        PendingIntent pendingIntent = PendingIntent.getService(mainContext, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) mainContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}