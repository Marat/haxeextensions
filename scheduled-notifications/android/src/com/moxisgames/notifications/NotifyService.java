package com.moxisgames.notifications;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;

public class NotifyService extends IntentService {

    public static final String TAG = "NotifyService";
    public static final String NOTIFICATION_CHANNEL_ID = "com.whee.slotbeatuties.CHANNEL";

    public NotifyService() {
        super("NotifyService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.w(TAG, "Service: onStartCommand startId = " + startId + ", isActive = " + ScheduledNotifications.isActive);

        if (!ScheduledNotifications.isActive && intent != null) {
            onHandleIntent(intent);
        }

        return START_STICKY;
        // START_NOT_STICKY https://developer.android.com/reference/android/app/Service
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.w(TAG, "onHandleIntent " + intent);

        //https://developer.android.com/reference/android/app/IntentService.html#onHandleIntent(android.content.Intent)
        //This may be null if the service is being restarted after its process has gone away
        if (intent == null) {
            return;
        }

        int id = intent.getIntExtra("id", 0);
        String title = intent.getStringExtra("title");
        String text = intent.getStringExtra("text");
        String ticker = intent.getStringExtra("ticker");

        Notification notification = makeNotification(id, title, text, ticker);
        NotificationManager notificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) {
            return;
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) == null) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "SlotBeauties channel", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private NotificationCompat.Builder createBuilder() {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) {
            return new NotificationCompat.Builder(getApplicationContext());
        }

        createNotificationChannel();

        return new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID);
    }

    private Notification makeNotification(int id, String title, String text, String ticker) {
        Intent intent = null;
        try {
            PackageManager pm = this.getPackageManager();
            if (pm != null) {
                String packageName = this.getPackageName();
                intent = pm.getLaunchIntentForPackage(packageName);
                intent.addCategory(Intent.CATEGORY_LAUNCHER); // Should already be set, but just in case
            }
        } catch (Exception e) {
            Log.w(TAG, "Failed to get application launch intent");
        }

        if (intent == null) {
            Log.w(TAG, "Falling back to empty intent");
            intent = new Intent();
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        String GROUP_KEY_APP = this.getPackageName() + ".GROUP";

        NotificationCompat.Builder builder = createBuilder()
            .setAutoCancel(true)
            .setContentTitle(title)
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setTicker(ticker)
            .setSmallIcon(R.drawable.small_icon)
            .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.large_icon))
            .setContentIntent(pendingIntent)
            .setOngoing(false)
            .setChannelId(NOTIFICATION_CHANNEL_ID)
            .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE | NotificationCompat.DEFAULT_LIGHTS)
            .setGroup(GROUP_KEY_APP);

        return builder.build();
    }
}