package com.moxisgames.extensions;

import openfl.errors.Error;
import openfl.Lib;

#if android
import openfl.utils.JNI;
#end

class HaxeShare {
	
	static private var _shareFunc:Dynamic;
	
	static private var _fbStatus:ShareAppStatus = UNKNOWN;
	static private var _twitterStatus:ShareAppStatus = UNKNOWN;
	
	static private inline var ANDROID_CLASS_PATH:String = "com.moxisgames.extensions.HaxeShare";
	
	static public function share(handler:Dynamic, message:String):Void
	{
		#if android
		if (_shareFunc == null)
			_shareFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "share", "(Lorg/haxe/lime/HaxeObject;Ljava/lang/String;)V");

		Lib.postUICallback(
			function() {
				_shareFunc(handler, message);
			}
		);
		
		#elseif ios
		throw new Error("Firstly write ios code!");
		#elseif web
		#end
	}
	
	static public function hasFacebookApp():Bool
	{
		#if android
		if (_fbStatus == UNKNOWN)
		{
			var hasFacebookFunc:Void->Bool = JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasFacebookApp", "()Z");
			_fbStatus = hasFacebookFunc() ? INSTALLED : NONE;
		}
		#elseif ios
		throw new Error("Firstly write ios code!");
		_fbStatus = NONE;
		#elseif web
		_fbStatus = NONE;
		#end
		
		return _fbStatus == INSTALLED;
	}
	
	static public function hasTwitterApp():Bool
	{
		#if android
		if (_twitterStatus == UNKNOWN)
		{
			var hasTwitterFunc:Void->Bool = JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasTwitterApp", "()Z");
			_twitterStatus = hasTwitterFunc() ? INSTALLED : NONE;
		}
		#elseif ios
		throw new Error("Firstly write ios code!");
		_twitterStatus = NONE;
		#elseif web
		_twitterStatus = NONE;
		#end
		
		return _twitterStatus == INSTALLED;
	}
	
}

enum ShareAppStatus
{
	UNKNOWN;
	INSTALLED;
	NONE;
}