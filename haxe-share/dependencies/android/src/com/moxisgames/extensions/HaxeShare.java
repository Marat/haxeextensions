package com.moxisgames.extensions;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import org.haxe.extension.Extension;
import org.haxe.lime.HaxeObject;

import java.lang.String;
import java.lang.System;
import java.util.ArrayList;
import java.util.List;

/* 
	You can use the Android Extension class in order to hook
	into the Android activity lifecycle. This is not required
	for standard Java code, this is designed for when you need
	deeper integration.
	
	You can access additional references from the Extension class,
	depending on your needs:
	
	- Extension.assetManager (android.content.res.AssetManager)
	- Extension.callbackHandler (android.os.Handler)
	- Extension.mainActivity (android.app.Activity)
	- Extension.mainContext (android.content.Context)
	- Extension.mainView (android.view.View)
	
	You can also make references to static or instance methods
	and properties on Java classes. These classes can be included 
	as single files using <java path="to/File.java" /> within your
	project, or use the full Android Library Project format (such
	as this example) in order to include your own AndroidManifest
	data, additional dependencies, etc.
	
	These are also optional, though this example shows a static
	function for performing a single task, like returning a value
	back to Haxe from Java.
*/
public class HaxeShare extends Extension {

	static private HaxeObject _callbackShare;

	private static final int REQUEST_SHARE = 1;

	public static final String FB_PACKAGE_NAME = "com.facebook.orca";
	public static final String TW_PACKAGE_NAME = "com.twitter.android";

	/*public static void share(HaxeObject callbackShare, String message) {

		Log.i("HaxeShare", "share callbackShare=" + String.valueOf(callbackShare) + ", message=" + message);

		_callbackShare = callbackShare;

		Context context = Extension.mainContext;
		Activity activity = Extension.mainActivity;
		PackageManager packageManager = context.getPackageManager();// Create instance of PackageManager

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		if (takePictureIntent.resolveActivity(context.getPackageManager()) != null)
		{
			activity.startActivityForResult(takePictureIntent, REQUEST_SHARE);
		}
	}*/

	public static void share(HaxeObject callbackShare, String message) {

		System.out.println(" >>> HaxeShare: share callbackShare=" + String.valueOf(callbackShare) + ", message=" + message);

		_callbackShare = callbackShare;
		Activity activity = Extension.mainActivity;
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		sendIntent.putExtra(Intent.EXTRA_TEXT, message);
		activity.startActivityForResult(sendIntent, REQUEST_SHARE);
	}

	public static boolean hasFacebookApp() {
		return isAppInstalled(FB_PACKAGE_NAME);
	}

	public static boolean hasTwitterApp() {
		return isAppInstalled(TW_PACKAGE_NAME);
	}

	private static boolean isAppInstalled(String uri) {
		System.out.println("Build.VERSION.SDK_INT = " + Build.VERSION.SDK_INT);

		try {
			PackageManager pm = Extension.mainContext.getPackageManager();
			pm.getPackageInfo(uri, 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}


	/**
	 * Called when an activity you launched exits, giving you the requestCode 
	 * you started it with, the resultCode it returned, and any additional data 
	 * from it.
	 */
	public boolean onActivityResult (int requestCode, int resultCode, Intent data) {

		Log.i("HaxeShare", "onActivityResult");

		return true;
	}
	//if (requestCode == REQUEST_SHARE)
	//{
	//Activity.RESULT_OK
	//if (_callbackShare != null)
	//{
	//_callbackShare.call("onShareCallback", new Object[] { resultCode });
	//_callbackShare = null;
	//}
	//}


	/**
	 * Called when the activity is starting.
	 */
	public void onCreate (Bundle savedInstanceState) {
		
		
		
	}
	
	
	/**
	 * Perform any final cleanup before an activity is destroyed.
	 */
	public void onDestroy () {
		
		
		
	}
	
	
	/**
	 * Called as part of the activity lifecycle when an activity is going into
	 * the background, but has not (yet) been killed.
	 */
	public void onPause () {
		
		
		
	}
	
	
	/**
	 * Called after {@link #onStop} when the current activity is being 
	 * re-displayed to the user (the user has navigated back to it).
	 */
	public void onRestart () {
		
		
		
	}
	
	
	/**
	 * Called after {@link #onRestart}, or {@link #onPause}, for your activity 
	 * to start interacting with the user.
	 */
	public void onResume () {
		
		
		
	}
	
	
	/**
	 * Called after {@link #onCreate} &mdash; or after {@link #onRestart} when  
	 * the activity had been stopped, but is now again being displayed to the 
	 * user.
	 */
	public void onStart () {
		
		
		
	}
	
	
	/**
	 * Called when the activity is no longer visible to the user, because 
	 * another activity has been resumed and is covering this one. 
	 */
	public void onStop () {
		
		
		
	}
	
	
}