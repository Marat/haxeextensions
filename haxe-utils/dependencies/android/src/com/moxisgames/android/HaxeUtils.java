package com.moxisgames.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.android.play.core.tasks.OnSuccessListener;

import org.haxe.extension.Extension;
import org.haxe.lime.HaxeObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.Exception;
import java.lang.String;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/* 
	You can use the Android Extension class in order to hook
	into the Android activity lifecycle. This is not required
	for standard Java code, this is designed for when you need
	deeper integration.
	
	You can access additional references from the Extension class,
	depending on your needs:
	
	- Extension.assetManager (android.content.res.AssetManager)
	- Extension.callbackHandler (android.os.Handler)
	- Extension.mainActivity (android.app.Activity)
	- Extension.mainContext (android.content.Context)
	- Extension.mainView (android.view.View)
	
	You can also make references to static or instance methods
	and properties on Java classes. These classes can be included 
	as single files using <java path="to/File.java" /> within your
	project, or use the full Android Library Project format (such
	as this example) in order to include your own AndroidManifest
	data, additional dependencies, etc.
	
	These are also optional, though this example shows a static
	function for performing a single task, like returning a value
	back to Haxe from Java.
*/
public class HaxeUtils extends Extension {

	static private HaxeObject _takePhotoCallback;
	static private HaxeObject _shareCallback;

	static final int REQUEST_IMAGE_CAPTURE = 1;
	static final int REQUEST_SHARE = 2;

	static final String TW_PACKAGE_NAME = "com.twitter.android";
	static final String TW_ACTIVITY_NAME = "com.twitter.android.composer";
	static final String FB_PACKAGE_NAME = "com.facebook.";
	static final String GP_PACKAGE_NAME = "com.google.android.apps.plus";
	static final String VK_PACKAGE_NAME = "com.vkontakte.android";

	/*public static void deleteCache() {
		try {
			File dir = Extension.mainContext.getCacheDir();
			if (dir != null && dir.isDirectory()) {
				//Log.i("HaxeUtils", "DIR = " + dir.getName());
				deleteDir(dir);
			}
		} catch (Exception e) {}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {

				//Log.i("HaxeUtils", "FILE = " + children[i]);

				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}*/

	public static int getApiVersion() {
		return android.os.Build.VERSION.SDK_INT;
	}
	
	/**
	 * ONLINE
	 */
	public static boolean hasInternetConnection() {
		ConnectivityManager cm =
				(ConnectivityManager) Extension.mainContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}
	
	public static boolean hasTextShareApp() 
	{
		PackageManager packageManager = Extension.mainContext.getPackageManager();
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.setType("text/plain");

		List<ResolveInfo> resolveInfoList = packageManager.queryIntentActivities(sendIntent, 0);
		for (int j = 0; j <resolveInfoList.size(); j++)
		{
			ResolveInfo resInfo = resolveInfoList.get(j);
			String packageName = resInfo.activityInfo.packageName;
			String activityName = resInfo.activityInfo.name;

			//Find apps
			if ((packageName.contains(TW_PACKAGE_NAME) && activityName.contains(TW_ACTIVITY_NAME))
					|| packageName.contains(FB_PACKAGE_NAME)
					|| packageName.contains(VK_PACKAGE_NAME)
					|| packageName.contains(GP_PACKAGE_NAME))
			{
				return true;
			}
		}
		
		return false;
	}

	/**
	 * SHARE
	 */
	public static void shareText(String chooseAppText, String subject, String message, String link, HaxeObject callback) {
		//Log.i("HaxeUtils", "shareText subject=" + subject + ", message=" + message);

		_shareCallback = callback;
		Activity activity = Extension.mainActivity;
		
		// all aplications
		/*Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		sendIntent.putExtra(Intent.EXTRA_TEXT, message);
		sendIntent.putExtra(Intent.EXTRA_TEXT, link);
		activity.startActivityForResult(sendIntent, REQUEST_SHARE);*/
		
		// filtered applications
		List<Intent> targetShareIntents = new ArrayList<Intent>();

		PackageManager packageManager = Extension.mainContext.getPackageManager();
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.setType("text/plain");

		List<ResolveInfo> resolveInfoList = packageManager.queryIntentActivities(sendIntent, 0);
		for (int j = 0; j <resolveInfoList.size(); j++)
		{
			ResolveInfo resInfo = resolveInfoList.get(j);
			String packageName = resInfo.activityInfo.packageName;
			String activityName = resInfo.activityInfo.name;

			//Find apps
			if ((packageName.contains(TW_PACKAGE_NAME) && activityName.contains(TW_ACTIVITY_NAME))
					|| packageName.contains(FB_PACKAGE_NAME)
					|| packageName.contains(VK_PACKAGE_NAME)
					|| packageName.contains(GP_PACKAGE_NAME))
			{
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_SUBJECT, subject);//set subject
				intent.putExtra(Intent.EXTRA_TEXT, message + "\n" + link);
				intent.setComponent(new ComponentName(packageName, activityName));
				intent.setPackage(packageName);

				targetShareIntents.add(intent);
			}
		}

		if (!targetShareIntents.isEmpty()) {
			Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), chooseAppText);
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
			chooserIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			activity.startActivityForResult(chooserIntent, REQUEST_SHARE);
		}
		else
		{
			showNativeMessage("Do not Have apps to share!", true);
		}
	}

	public static String getShareImagePath(String fileName) {
		if (!canSaveToExternalStorage()) {
			return "";
		}

		File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
		imagesFolder.mkdirs();
		File imageFile = new File(imagesFolder, fileName);

		return imageFile.getPath();
	}

	public static void shareImage(String chooseHeader, String subject, String link, String screenshotPath) {
		Log.i("HaxeUtils", "shareImage subject=" + subject + ", screenshotPath=" + screenshotPath);

		if (!canSaveToExternalStorage()) {
			showNativeMessage("External storage unavailable", true);
			return;
		}

		Activity activity = Extension.mainActivity;
		//Environment.getExternalStorageState();
		File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
		imagesFolder.mkdirs();
		
		//File imageFile = new File(imagesFolder, "PuzzleScreenshot.png");
		File imageFile = new File(screenshotPath);

		//String filename = file.toString();

		DisplayMetrics dm = activity.getResources().getDisplayMetrics();

		/*int width = dm.widthPixels;
		int height = dm.heightPixels;
		ByteBuffer buf = ByteBuffer.allocateDirect(width * height * 4);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		GLES20.glReadPixels(0, 0, width, height,
				GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buf);
		GLUtil.checkGlError("glReadPixels");
		buf.rewind();

		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(imageFile));
			Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			bmp.copyPixelsFromBuffer(buf);
			bmp.compress(Bitmap.CompressFormat.PNG, 90, bos);
			bmp.recycle();
		} finally {
			if (bos != null) bos.close();
		}*/

		// create bitmap screen capture
		/*View v = activity.getWindow().getDecorView().getRootView();
		v.setDrawingCacheEnabled(true);
		v.buildDrawingCache();
		Bitmap returnedBitmap = v.getDrawingCache();

		OutputStream fout = null;
		try {
			fout = new FileOutputStream(imageFile);
			returnedBitmap.compress(Bitmap.CompressFormat.PNG, 100 , fout);
			fout.flush();
			fout.close();
			showNativeMessage("Image saved!", true);
			v.destroyDrawingCache();
		} catch ( FileNotFoundException e) {
			// TODO Auto-generated catch block
			showNativeMessage("File not found!", true);
			return;
			// e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			showNativeMessage("IO Exception!", true);
			return;
			// e.printStackTrace();
		}*/

		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		sendIntent.putExtra(Intent.EXTRA_TEXT, link);
		sendIntent.setType("image/png");
		sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));

		activity.startActivityForResult(sendIntent, REQUEST_SHARE);
	}

	public static boolean hasFacebookApp() {
		return isAppInstalled(FB_PACKAGE_NAME);
	}

	public static boolean hasTwitterApp() {
		return isAppInstalled(TW_PACKAGE_NAME);
	}

	private static boolean isAppInstalled(String uri) {
		try {
			PackageManager pm = Extension.mainContext.getPackageManager();
			pm.getPackageInfo(uri, 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	private static boolean canSaveToExternalStorage() {
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		return mExternalStorageAvailable && mExternalStorageWriteable;
	}

	/**
	 * Check if this device has a camera
	 */
	public static boolean hasCamera() {
		Log.i("HaxeUtils", "checkCameraHardware");
		
		PackageManager pm = Extension.mainContext.getPackageManager();
		return pm != null && pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	static private Uri _takedPhotoUri;

	/**
	 * TAKE PHOTO
	 * @param callback
	 */
	public static void takePhoto (HaxeObject callback, String photoName) {
		Log.i("HaxeUtils", "takePhoto");

		if (!canSaveToExternalStorage()) {
			showNativeMessage("External storage unavailable", false);
			return;
		}

		_takePhotoCallback = callback;

		//camera stuff
		Intent imageIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

		//folder stuff
		File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
		imagesFolder.mkdirs();

		File image = new File(imagesFolder, photoName);
		_takedPhotoUri = Uri.fromFile(image);

		imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, _takedPhotoUri);
		Extension.mainActivity.startActivityForResult(imageIntent, REQUEST_IMAGE_CAPTURE);
	}

	/**
	 * Called when an activity you launched exits, giving you the requestCode 
	 * you started it with, the resultCode it returned, and any additional data 
	 * from it.
	 */
	public boolean onActivityResult (int requestCode, int resultCode, Intent data) {

		//Log.i("HaxeUtils", "onActivityResult requestCode=" + requestCode + ", resultCode=" + resultCode + ", data=" + data);

		if (requestCode == REQUEST_IMAGE_CAPTURE)
		{
			if (resultCode == Activity.RESULT_OK)
			{
				int imageRotation = 0;

				if (_takedPhotoUri != null && Extension.mainContext != null)
					imageRotation = getCameraPhotoOrientation(Extension.mainContext, _takedPhotoUri, _takedPhotoUri.getPath());

				if (_takePhotoCallback != null) {
					_takePhotoCallback.call("takePhotoCallback", new Object[]{ _takedPhotoUri != null ? _takedPhotoUri.getPath() : "", imageRotation } );
					_takePhotoCallback = null;
				}
			}
			else
			{
				_takePhotoCallback.call("takePhotoCallback", new Object[]{ "", 0 });
				_takePhotoCallback = null;
			}
		}
		else if (requestCode == REQUEST_SHARE)
		{
			if (_shareCallback != null)
			{
				_shareCallback.call("shareCallback", new Object[] { resultCode == Activity.RESULT_OK ? 1 : 0 } );
				_shareCallback = null;
			}
		}

		return true;
	}

	private int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
		int rotate = 0;
		try {
			context.getContentResolver().notifyChange(imageUri, null);
			File imageFile = new File(imagePath);

			ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_270:
					rotate = 270;
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					rotate = 180;
					break;
				case ExifInterface.ORIENTATION_ROTATE_90:
					rotate = 90;
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rotate;
	}

	static public void showNativeMessage(String text, boolean isLong) {
		Toast.makeText(Extension.mainContext, text, isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
	}

	static public void openRateAppLink() {
		Uri uri = Uri.parse("market://details?id=" + Extension.mainContext.getPackageName());
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		// To count with Play market backstack, After pressing back button,
		// to taken back to our application, we need to add following flags to intent.
		goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
				Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
				Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		try {
			Extension.mainActivity.startActivity(goToMarket);
		} catch (ActivityNotFoundException e) {
			Extension.mainActivity.startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://play.google.com/store/apps/details?id=" + Extension.mainContext.getPackageName())));
		}
	}

	static public void checkAppUpdates() {
		// Log.w("HaxeUtils", "checkAppUpdates");

		if (getApiVersion() < 21) {
			return;
		}

		// Creates instance of the manager.
		final AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(Extension.mainContext);

		// Returns an intent object that you use to check for an update.
		Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

		// Checks that the platform will allow the specified type of update.
		appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
			@Override
			public void onSuccess(AppUpdateInfo appUpdateInfo) {
				// Log.w("HaxeUtils", "callback: " + appUpdateInfo.updateAvailability());

				if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
						// For a flexible update, use AppUpdateType.FLEXIBLE
						&& appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
					// Request the update.
					try {
						appUpdateManager.startUpdateFlowForResult(
								// Pass the intent that is returned by 'getAppUpdateInfo()'.
								appUpdateInfo,
								// Or 'AppUpdateType.FLEXIBLE' for flexible updates.
								AppUpdateType.IMMEDIATE,
								// The current activity making the update request.
								Extension.mainActivity,
								// Include a request code to later monitor this update request.
								0
						);
					} catch(SendIntentException e){
					}
				}
			}
		});
	}

	/**
	 * Called when the activity is starting.
	 */
	public void onCreate (Bundle savedInstanceState) {
	}
	
	/**
	 * Perform any final cleanup before an activity is destroyed.
	 */
	public void onDestroy () {
	}

	/**
	 * Called as part of the activity lifecycle when an activity is going into
	 * the background, but has not (yet) been killed.
	 */
	public void onPause () {
	}

	/**
	 * Called after {@link #onStop} when the current activity is being 
	 * re-displayed to the user (the user has navigated back to it).
	 */
	public void onRestart () {
	}

	/**
	 * Called after {@link #onRestart}, or {@link #onPause}, for your activity 
	 * to start interacting with the user.
	 */
	public void onResume () {
	}

	/**
	 * Called after {@link #onCreate} &mdash; or after {@link #onRestart} when  
	 * the activity had been stopped, but is now again being displayed to the 
	 * user.
	 */
	public void onStart () {
	}

	/**
	 * Called when the activity is no longer visible to the user, because 
	 * another activity has been resumed and is covering this one. 
	 */
	public void onStop () {
	}
}