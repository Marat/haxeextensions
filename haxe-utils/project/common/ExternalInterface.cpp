#ifndef STATIC_LINK
#define IMPLEMENT_API
#endif

#if defined(HX_WINDOWS) || defined(HX_MACOS) || defined(HX_LINUX)
#define NEKO_COMPATIBLE
#endif


#include <hx/CFFI.h>
#include <hxcpp.h>
#include <stddef.h>
#include "Utils.h"

using namespace haxeutils;

//

extern "C" void haxeutils_main () {
	val_int(0); // Fix Neko init
}

extern "C" int haxeutils_register_prims () {
    return 0;
}

DEFINE_ENTRY_POINT (haxeutils_main);
