package com.moxisgames.extensions.utils;

import openfl.display.PNGEncoderOptions;
import openfl.display.BitmapData;
import openfl.geom.Rectangle;
import openfl.Lib;
import openfl.utils.ByteArray;

#if android
import sys.io.File;
import lime.system.JNI;
#elseif ios
import lime.system.CFFI;
#end

class HaxeUtils {
	
	static private inline var ANDROID_CLASS_PATH:String = "com.moxisgames.android.HaxeUtils";
	
	// CAMERA
	static private var _takePhoto:Dynamic->String->Void;
	static private var _cameraStatus:HaxeUtilsFeatureStatus = UNKNOWN;
	
	static private var _apiVersion:Int = -1;
	
	static public function takePhoto(listener:Dynamic, photoName:String) 
	{
		#if android
		if (_takePhoto == null)
			_takePhoto = JNI.createStaticMethod(ANDROID_CLASS_PATH, "takePhoto", "(Lorg/haxe/lime/HaxeObject;Ljava/lang/String;)V");
		
		JNI.postUICallback(
			function() {
				_takePhoto(listener, photoName);
			}
		);
		#elseif ios
		if (_takePhoto == null)
			_takePhoto = CFFI.load("haxeutils", "take_photo", 2);
		
		_takePhoto(listener, photoName);
		#end
	}
	
	static public function hasCamera():Bool
	{
		if (_cameraStatus == UNKNOWN)
		{
			var hasCamera:Dynamic = null;
			
			#if android
			hasCamera = JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasCamera", "()Z");
			#elseif ios
			hasCamera = CFFI.load("haxeutils", "has_camera", 0);
			#end
			
			_cameraStatus = (hasCamera != null && hasCamera()) ? AVAILABLE : UNAVAILABLE;
		}
		
		return _cameraStatus == AVAILABLE;
	}
	
	// CONNECTION
	static private var _hasInternetFunc:Void->Bool;
	
	static public function hasConnection():Bool 
	{
		#if android
		if (_hasInternetFunc == null)
			_hasInternetFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasInternetConnection", "()Z");

		return _hasInternetFunc();
		#elseif ios
		if (_hasInternetFunc == null)
			_hasInternetFunc = CFFI.load("haxeutils", "has_connection", 0);

		return _hasInternetFunc != null && _hasInternetFunc();
		#elseif web
		return true;
		#end
		
		return true;
	}
	
	static public function getApiVersion():Int
	{
		#if android
		if (_apiVersion == -1)
		{
			var getApiVersion:Void->Int = JNI.createStaticMethod(ANDROID_CLASS_PATH, "getApiVersion", "()I");
			_apiVersion = getApiVersion();
			getApiVersion = null;
		}
		#elseif ios
		if (_apiVersion == -1)
		{
			_apiVersion = 100;
		}
		#end
		
		return _apiVersion;
	}
	
	// CACHE
	/*static private var _deleteCache:Void->Void;
	
	static public function deleteCache():Void 
	{
		#if android
		if (_deleteCache == null)
			_deleteCache = JNI.createStaticMethod(ANDROID_CLASS_PATH, "deleteCache", "()V");

		_deleteCache();
		#elseif ios
		if (_deleteCache == null)
			_deleteCache = CFFI.load("haxeutils", "deleteCache", 0);

		_deleteCache();
		#end
	}*/
	
	// MESSAGE
	static private var _showMessageFunc:String->Bool->Void;
	
	static public function showNativeMessage(message:String, isLong:Bool = false):Void
	{
		#if android
		if (_showMessageFunc == null)
			_showMessageFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "showNativeMessage", "(Ljava/lang/String;Z)V");

		JNI.postUICallback(
			function() {
				_showMessageFunc(message, isLong);
			}
		);
		#elseif ios
		if (_showMessageFunc == null)
			_showMessageFunc = CFFI.load("haxeutils", "show_message", 2);

		_showMessageFunc(message, isLong);
		#end
	}
	
	// SHARE
	static private var _shareTextFunc:Dynamic;
	static private var _shareImageFunc:String->String->String->String->Void;
	static private var _getImagePathFunc:String->String;
	
	static private var _fbStatus:HaxeUtilsFeatureStatus = UNKNOWN;
	static private var _twitterStatus:HaxeUtilsFeatureStatus = UNKNOWN;
	static private var _hasTextShareAppsStatus:HaxeUtilsFeatureStatus = UNKNOWN;
	
	static public function shareText(chooseAppText:String, subject:String, message:String, link:String, handler:Dynamic = null):Void
	{
		#if android
		if (_shareTextFunc == null)
			_shareTextFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "shareText", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/haxe/lime/HaxeObject;)V");

		JNI.postUICallback(
			function() {
				_shareTextFunc(chooseAppText, subject, message, link, handler);
			}
		);
		#elseif ios
		if (_shareTextFunc == null)
			_shareTextFunc = CFFI.load("haxeutils", "share_text", 5);

		_shareTextFunc(chooseAppText, subject, message, link, handler);
		#end
	}
	
	static public function getShareImagePath(fileName:String):String
	{
		#if android
		if (_getImagePathFunc == null)
			_getImagePathFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "getShareImagePath", "(Ljava/lang/String;)Ljava/lang/String;");

		return _getImagePathFunc(fileName);
		#elseif ios
		if (_getImagePathFunc == null)
			_getImagePathFunc = CFFI.load("haxeutils", "get_image_path", 1);

		return _getImagePathFunc(fileName);
		#end
		
		return "";
	}
	
	static public function shareImage(chooseHeader:String, subject:String, link:String, bData:BitmapData = null, prepareCallback:Void->Void = null):Void
	{
		#if android
		if (_shareImageFunc == null)
			_shareImageFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "shareImage", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

		JNI.postUICallback(
			function() {
				var screenshotPath:String = getShareImagePath("PuzzleScreenshot.png");
				
				if (screenshotPath != "" && bData != null)
				{
					var bArray:ByteArray = bData.encode(new Rectangle(0, 0, bData.width, bData.height), new PNGEncoderOptions());
					
					File.saveBytes(screenshotPath, bArray);
					
					showNativeMessage("Saved to: " + screenshotPath, true);
					
					_shareImageFunc(chooseHeader, subject, link, screenshotPath);
					
					bData.dispose();
					
					if (prepareCallback != null)
						prepareCallback();
				}
			}
		);
		
		#elseif ios
		if (_shareImageFunc == null)
			_shareImageFunc = CFFI.load("haxeutils", "share_image", 4);
			
		// TODO
		#end
	}
	
	static public function hasTextShareApp():Bool
	{
		if (_hasTextShareAppsStatus == UNKNOWN)
		{
			var hasTextShareAppsFunc:Dynamic = null;
			
			#if android
			hasTextShareAppsFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasTextShareApp", "()Z");
			#elseif ios
			hasTextShareAppsFunc = CFFI.load("haxeutils", "has_text_share_app", 0);
			#end
			
			_hasTextShareAppsStatus = (hasTextShareAppsFunc != null && hasTextShareAppsFunc()) ? AVAILABLE : UNAVAILABLE;
		}
		
		return _hasTextShareAppsStatus == AVAILABLE;
	}
	
	static public function hasFacebookApp():Bool
	{
		#if android
		if (_fbStatus == UNKNOWN)
		{
			var hasFacebookFunc:Dynamic = JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasFacebookApp", "()Z");
			_fbStatus = hasFacebookFunc() ? AVAILABLE : UNAVAILABLE;
		}
		#else
		_fbStatus = UNAVAILABLE;
		#end
		
		return _fbStatus == AVAILABLE;
	}
	
	static public function hasTwitterApp():Bool
	{
		#if android
		if (_twitterStatus == UNKNOWN)
		{
			var hasTwitterFunc:Dynamic = JNI.createStaticMethod(ANDROID_CLASS_PATH, "hasTwitterApp", "()Z");
			_twitterStatus = hasTwitterFunc() ? AVAILABLE : UNAVAILABLE;
		}
		#else
		_twitterStatus = UNAVAILABLE;
		#end
		
		return _twitterStatus == AVAILABLE;
	}

	static public function openRateAppLink():Void {
		#if android
		var openRateAppLink:Dynamic = JNI.createStaticMethod(ANDROID_CLASS_PATH, "openRateAppLink", "()V");
		openRateAppLink();
		#end

        // TODO ios
	}

	static public function checkAppUpdates():Void {
		#if android
		var checkAppUpdates:Dynamic = JNI.createStaticMethod(ANDROID_CLASS_PATH, "checkAppUpdates", "()V");
		checkAppUpdates();
		#end

        // TODO ios
	}
}

enum HaxeUtilsFeatureStatus
{
	UNKNOWN;
	AVAILABLE;
	UNAVAILABLE;
}