package com.moxisgames.extensions;

import haxe.ds.StringMap;
import openfl.Lib;

#if android
import openfl.utils.JNI;
#end

class HaxeFlurry {
	
	static private var _inited:Bool = false;
	
	static private var _eventFunc:String->String->Bool->Void;
	static private var _eventEndFunc:String->Void;
	static private var _errorFunc:String->String->Void;
	
	static private inline var ANDROID_CLASS_PATH:String = "com.moxisgames.extensions.HaxeFlurry";
	
	static public function init(appKey:String) 
	{
		if (_inited) return;
		
		//trace("HaxeFlurry init " + Lib.getTimer());
		
		#if android
		var initFunc:Dynamic = JNI.createStaticMethod(ANDROID_CLASS_PATH, "init", "(Ljava/lang/String;)V");
		
		Lib.postUICallback(
			function() {
				initFunc(appKey);
				_inited = true;
				
				//trace("HaxeFlurry inited. " + Lib.getTimer() + ". appKey = " + appKey);
			}
		);
		#else
		//
		#end
	}
	
	static public function event(eventID:String, params:Array<EventParam> = null, timed:Bool = false) 
	{
		if (!_inited) return;
		
		#if android
		if (_eventFunc == null)
			_eventFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "event", "(Ljava/lang/String;Ljava/lang/String;Z)V");
		
		//trace("HaxeFlurry event " + Lib.getTimer() + " id=" + eventID);
		
		Lib.postUICallback(
			function() {
				var paramsStr:String = "";
				
				if (params != null && params.length > 0)
				{
					for (i in 0...params.length)
					{
						var param:EventParam = params[i];
						
						if (i > 0)
							paramsStr += ",";
						
						paramsStr += param.name + "=" + param.value;
					}
				}
				
				//trace("HaxeFlurry event " + Lib.getTimer() + " id=" + eventID + ", params=" + paramsStr);
				
				_eventFunc(eventID, paramsStr, timed);
			}
		);
		#else
		//
		#end
	}
	
	static public function eventTimeEnd(eventID:String) 
	{
		if (!_inited) return;
		
		#if android
		if (_eventEndFunc == null)
			_eventEndFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "endTimedEvent", "(Ljava/lang/String;)V");
		
		Lib.postUICallback(
			function() {
				_eventEndFunc(eventID);
			}
		);
		#else
		//
		#end
	}
	
	static public function error(errorID:String, errorMessage:String) 
	{
		if (!_inited) return;
		
		//trace("HaxeFlurry error id=" + errorID + " msg=" + errorMessage);
		
		#if android
		if (_errorFunc == null)
			_errorFunc = JNI.createStaticMethod(ANDROID_CLASS_PATH, "error", "(Ljava/lang/String;Ljava/lang/String;)V");
		
		Lib.postUICallback(
			function() {
				_errorFunc(errorID, errorMessage);
			}
		);
		#else
		//
		#end
	}
	
}

typedef EventParam = { name:String, value:String }