package com.moxisgames.extensions;

import openfl.errors.Error;
import openfl.Lib;

#if android
import lime.system.JNI;
#end

class HaxeNetwork {
	
	static private var _isOnline:Dynamic;
	
	static private inline var ANDROID_CLASS_PATH:String = "com.moxisgames.extensions.HaxeNetwork";
	
	static public function isConnected():Bool 
	{
		#if android
		if (_isOnline == null)
			_isOnline = JNI.createStaticMethod(ANDROID_CLASS_PATH, "isOnline", "()Z");

		return _isOnline();
		#elseif ios
		throw new Error("Firstly write ios code!");
		#elseif web
		return true;
		#end
		
		return true;
	}
	
}